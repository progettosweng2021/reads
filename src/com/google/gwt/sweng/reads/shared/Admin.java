package com.google.gwt.sweng.reads.shared;

import java.io.Serializable;

@SuppressWarnings("serial")
public class Admin implements Serializable {
	
	private String username;
	private String password;
	
	/*
	 * COSTRUTTORI
	 */
	
	public Admin() {
		this.username = "admin";
		this.password = "admin";
	}
	
	/*
	 * GETTERS & SETTERS
	 */

	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	
	/*
	 * METODI OVERRIDDEN
	 */

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Admin other = (Admin) obj;
		if (username == null) {
			if (other.username != null)
				return false;
		} else if (!username.equals(other.username))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Amministratore [username=" + username + ", password=" + password + "]";
	}
	
	

}
