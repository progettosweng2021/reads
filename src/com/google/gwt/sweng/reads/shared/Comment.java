package com.google.gwt.sweng.reads.shared;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;

import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.Label;

@SuppressWarnings("serial")
public class Comment implements Serializable {

	private String userId;
	private String articleTitle;
	private Date date;
	private String textComment;
	private Boolean state;
	
	/*
	 * COSTRUTTORI
	 */
	
	public Comment() {}
	
	public Comment(String user, String articleTitle, Date date, String textComment, Boolean state) {
		this.userId = user;		//si intende username
		this.articleTitle = articleTitle;
		this.date = date;
		this.textComment = textComment;
		this.state = state;
	}
	
	/*
	 * GETTERS & SETTERS
	 */
	
	public String getUser() {
		return userId;
	}
	public void setUser(String user) {
		this.userId = user;
	}
	
	public String getArticleTitle() {
		return articleTitle;
	}
	public void setArticleTitle(String articleId) {
		this.articleTitle = articleId;
	}
	
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	
	public String printDate() {
		return DateTimeFormat.getShortDateFormat().format(this.date);
		//return this.date.toLocaleString();
	}
	
	public String getTextComment() {
		return textComment;
	}
	public void setTextComment(String textComment) {
		this.textComment = textComment;
	}
	
	public Boolean isApproved() {
		return state;
	}
	public void setState(Boolean state) {
		this.state = state;
	}
	
	public FlowPanel getPanelComment() {
		
		FlowPanel singleCom = new FlowPanel();
		singleCom.getElement().addClassName("comment-block");
		
		String body = "<h3>" + this.getUser() + "</h3>"
				+ "<div class=\"dateandcat\">"
					+ "<span class=\"badge badge-secondary\">Postato " + this.printDate() + "</span>"
				+ "</div>"
				+ "<p class=\"text-justify\">" + this.getTextComment() + "</p>";

		HTMLPanel comment = new HTMLPanel(body);
		//Label author = new Label(" - " + this.getAuthor());
		
		FlowPanel bottomarticle = new FlowPanel();

		//Button gotoarticle = new Button("Apri");
		//gotoarticle.getElement().addClassName("float-right");
		
		//bottomarticle.add(gotoarticle);
		//bottomarticle.add(stats);
		HTMLPanel hr = new HTMLPanel("<hr>");
		
		//author.getElement().addClassName("font-weight-light font-italic");
		
		singleCom.add(comment);
		singleCom.add(hr);
		
		return singleCom;
	}
	
	/*
	 * METODI OVERRIDDEN
	 */
	
	@Override
	public boolean equals(Object obj) {
		// TODO Auto-generated method stub
		return super.equals(obj);
	}
	@Override
	public String toString() {
		return "textComment [userId=" + userId + ", articleId=" + articleTitle + ", date=" + date
				+ ", textComment=" + textComment + ", state=" + state + "]";
	}
	
	
	
	
	
	
	
	
}
