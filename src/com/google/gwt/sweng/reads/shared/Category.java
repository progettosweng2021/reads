package com.google.gwt.sweng.reads.shared;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.ArrayList;

@SuppressWarnings("serial")
public class Category implements Serializable {
	
	private String name;
	private ArrayList<String> subCat;
	
	/*
	 * COSTRUTTORI
	 */
	
	public Category() {}
	
	public Category(String nameCategoria) {
		this.name = nameCategoria;
		this.subCat = new ArrayList<String>();
	}
	
	/*
	 * GETTERS & SETTERS
	 */

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public ArrayList<String> getSubCat() {
		return subCat;
	}

	public void setSubCat(String subCat) {
		this.subCat.add(subCat);
	}
	
	public boolean removeSubCat(String sub) {
		return this.subCat.remove(sub);
	}

	
}
