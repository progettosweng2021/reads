package com.google.gwt.sweng.reads.shared;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;

import com.google.gwt.core.client.GWT;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.sweng.reads.client.GreetingService;
import com.google.gwt.sweng.reads.client.GreetingServiceAsync;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.Label;

@SuppressWarnings("serial")
public class Article implements Serializable {
	
	private String title;
	private String text;
	private Date date;
	private String category;
	private String subcat;
	private String author;
	private boolean premium;
	private int numTotLike;
	private int numTotComm;
	
	
	/*
	 * COSTRUTTORI
	 */
	
	public Article() {}
	
	public Article(String title, String text, Date date, String category, String author, boolean premium, int com, int like) {
		this.title = title;
		this.text = text;
		this.date = date;
		this.category = category;
		this.author = author;
		this.premium = premium;
		this.subcat = "";
		this.numTotComm = com;
		this.numTotLike = like;
	}
	
	/*
	 * GETTERS & SETTERS
	 */
	
	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}
	
	
	@SuppressWarnings("deprecation")
	public String printDate() {
		return DateTimeFormat.getShortDateFormat().format(this.date);
		//return this.date.toString();
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}
	
	public String getSubCategory() {
		return subcat;
	}
	
	public void setSubCategory(String s) {
		this.subcat = s;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}
	

	public boolean isPremium() {
		return premium;
	}

	public void setPremium(boolean premium) {
		this.premium = premium;
	}
	
	//metodi incremento/decremento numero like
	public int getNumTotLike() {
		return numTotLike;
	}
	
	public void addNumTotLike() {
		this.numTotLike = numTotLike + 1;
	}
	
	public void lessNumTotLike() {
		this.numTotLike = numTotLike - 1;
	}
	
	//metodi incremento/decremento numero commenti
	public int getNumTotComm() {
		return numTotComm;
	}
	
	public void addNumTotComm() {
		this.numTotComm = numTotComm + 1;
	}
	
	public void lessNumTotComm() {
		this.numTotComm = numTotComm - 1;
	}
	
	//stampo TUTTO il testo: stampa che viene mostrata in OpenArticle
	public FlowPanel getPanelArticle() {
		
		FlowPanel singleart = new FlowPanel();
		singleart.getElement().addClassName("article-block");
		
		String body = "<h3>" + getStar() + " " + this.getTitle() +  "</h3><div class=\"dateandcat\"><span class=\"badge badge-secondary\">Postato " + this.printDate() + "</span>"
				+ "<div class=\"float-right\"><span class=\"badge badge-success\">" + this.getCategory() + "</span> "
						+ "<span class=\"badge badge-info\">" + this.getSubCategory() + "</span></div></div>"
							+ "<p class=\"text-justify\">" + this.getText() + "</p>";

		HTMLPanel article = new HTMLPanel(body);
		Label author = new Label(" - " + this.getAuthor());
		
		FlowPanel bottomarticle = new FlowPanel();
		
		author.getElement().addClassName("font-weight-light font-italic");
		
		singleart.add(article);
		singleart.add(author);
		singleart.add(bottomarticle);
		
		return singleart;
	}
	
	public String anticipationText(String text) {
		String result = "";
		
		//stampo solo la prima frase (fino al primo punto)
		String[] parole = text.split("\\. ");
		
		result = parole[0] + ".";
		
		return result;
	}
	
	public String getStar() {
		if (this.isPremium()) {
			return "&#9734;";
		} else {
			return "";
		}
	}
	
	
	//stampo solo i primi n caratteri: stampa che viene usata nella Home
	public FlowPanel getPanelArticleShortVersion() {
		
		FlowPanel singleart = new FlowPanel();
		singleart.getElement().addClassName("article-block");
		
		String body = "<h3>" + getStar() + " " + this.getTitle() + "</h3><div class=\"dateandcat\"><span class=\"badge badge-secondary\">Postato " + this.printDate() + "</span>"
				+ "<div class=\"float-right\"><span class=\"badge badge-success\">" + this.getCategory() + "</span> "
						+ "<span class=\"badge badge-info\">" + this.getSubCategory() + "</span></div></div>"
							+ "<p class=\"text-justify\">" + anticipationText(this.getText())+ "</p>"
							+ "<b class=\"text-justify\"> ... per continuare a leggere aprire l'articolo... </b>"
							+ "<p></p>";

		HTMLPanel article = new HTMLPanel(body);
		Label author = new Label(" - " + this.getAuthor());

		author.getElement().addClassName("font-weight-light font-italic");
		
		singleart.add(article);
		singleart.add(author);
		
		return singleart;
	}
	
	/*
	 * METODI OVERRIDDEN
	 */

	@Override
	public boolean equals(Object arg0) {
		// TODO Auto-generated method stub
		return super.equals(arg0);
	}

	@Override
	public String toString() {
		return "Articolo [title=" + title + ", text=" + text + ", date=" + this.printDate() + ", category="
				+ category + ", author=" + author + ", premium=" + premium + "]";
	}
	
}