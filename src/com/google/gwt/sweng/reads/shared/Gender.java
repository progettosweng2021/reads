package com.google.gwt.sweng.reads.shared;

public enum Gender {
	
	Male,
	Female,
	Other

}
