package com.google.gwt.sweng.reads.shared;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;

@SuppressWarnings("serial")
public class Like implements Serializable {
	
	private String titleArticle;
	private String userName;
	private int ID;			//?????????
	
	/*
	 * COSTRUTTORI
	 */
	
	public Like() {}
	
	public Like(String titleArticle, String user) {
		this.titleArticle = titleArticle;
		this.userName = user;
	}
	
	/*
	 * GETTERS & SETTERS
	 */
	
	public String getTitleArticle() {
		return titleArticle;
	}
	public void setTitleArticle(String titleArticle) {
		this.titleArticle = titleArticle;
	}
	
	public String getUser() {
		return userName;
	}
	public void setUser(String user) {
		this.userName = user;
	}
	
	public int getID() {
		return ID;
	}
	public void setID(int id) {
		this.ID = id;
	}
	
	/*
	 * METODI OVERRIDDEN
	 */
	
	@Override
	public String toString() {
		return "Like [titleArticle=" + titleArticle + ", user=" + userName + ", ID=" + ID + "]";
	}
	
}
