package com.google.gwt.sweng.reads.shared;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;

@SuppressWarnings("serial")
public class User implements Serializable {
	
	private String username;
	private String email;
	private String password;
	private String name;
	private String surname;
	private Date birthDate;
	private String birthCity;
	private Gender genre;
	private String address;
	private Date subDate;
	private Boolean subscribed;
	private ArrayList<String> favCat;
	
	/*	
	 * COSTRUTTORI
	 */
	
	public User() {}
	
	public User(String username, String email, String password, String name, String surname, Date birthDate, String birthCity, Gender genre,
			String address, ArrayList<String> listFavCat) {
		this.username = username;
		this.email = email;
		this.password = password;
		this.name = name;
		this.surname = surname;
		this.birthDate = birthDate;
		this.birthCity = birthCity;
		this.genre = genre;
		this.address = address;
		this.subDate = new Date();
		this.favCat = listFavCat;
		this.subscribed = false;
	}

	
	/*
	 * GETTERS & SETTERS
	 */
		
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}

	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public Date getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}

	public String getBirthCity() {
		return birthCity;
	}

	public void setBirthCity(String birthCity) {
		this.birthCity = birthCity;
	}

	public String getGenre() {
		return genre.name();
	}

	public void setGenre(Gender genre) {
		this.genre = genre;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public Boolean isSubscribed() {
		return subscribed;
	}

	public void setSubscribed(Boolean subscribed) {
		this.subscribed = subscribed;
	}

	public ArrayList<String> getFavCat() {
		return favCat;
	}

	public void setFavCat(ArrayList<String> favCat) {
		this.favCat = favCat;
	}
	
	
/*
 *	METODI OVERRIDDEN 
 */

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		User other = (User) obj;
		return true;
	}
	
	@Override
	public String toString() {
		return "Utente [email=" + email + ", password=" + password + ", name=" + name + ", surname="
				+ surname + ", birthDate=" + birthDate + ", birthCity=" + birthCity + ", genre=" + genre + ", address="
				+ address + ", subscribed=" + subscribed  + ", favCat=" + favCat
				+ "]";
	}
	
}
