package com.google.gwt.sweng.reads.shared;

import java.io.Serializable;
import java.util.Date;

@SuppressWarnings("serial")
public class Subscription implements Serializable {
	
	private String numCard;
	private String name;
	private String surname;
	private String dateExp;		
	private String cvv;			
	private String nation;
	private String cap;
	private String idUser;
	
	/*
	 * COSTRUTTORI
	 */
	
	public Subscription() {}
	
	public Subscription(String numCard, String name, String surname, String dateExp, String cvv, String nation, String cap, String username) {
		this.numCard = numCard;
		this.name = name;
		this.surname = surname;
		this.dateExp = dateExp;
		this.cvv = cvv;
		this.nation = nation;
		this.cap = cap;
		this.idUser = username;
	}
	
	/*
	 * GETTERS & SETTERS
	 */
	public String getNumCard() {
		return numCard;
	}

	public void setNumCard(String numCard) {
		this.numCard = numCard;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public String getSurname() {
		return surname;
	}
	
	public void setSurname(String surname) {
		this.surname = surname;
	}
	
	public String getDate () {
		return dateExp.toString();
	}
	
	public void setDate(String date) {
		this.dateExp= date;
	}

	public String getCvv() {
		return cvv;
	}

	public void setCvv(String cvv) {
		this.cvv = cvv;
	}

	public String getNation() {
		return nation;
	}

	public void setNation(String nation) {
		this.nation = nation;
	}

	public String getCap() {
		return cap;
	}

	public void setCap(String cap) {
		this.cap = cap;
	}

	public String getIdUser() {
		return idUser;
	}

	public void setIdUser(String idUser) {
		this.idUser = idUser;
	}


	
	/*
	 * METODI OVERRIDDEN 
	 */
	
	@Override
	public String toString() {
		return "Subscription [numCard=" + numCard + ", name=" + name + ", surname=" + surname + ", dateExp="
				+ dateExp + ", cvv=" + cvv + ", nation=" + nation + ", cap=" + cap + ", idUser=" + idUser
				+ "]";
	}

	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Subscription other = (Subscription) obj;
	
		if (numCard == null) {
			if (other.numCard != null)
				return false;
		} else if (!numCard.equals(other.numCard))
			return false;
		return true;
	}
	
}
