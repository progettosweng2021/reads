package com.google.gwt.sweng.reads.client;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.VerticalPanel;

public class AdminDashboard {

	//Componenti Admin Dashboard
	public static VerticalPanel vpDashboard;
	public static FlowPanel fpContainer = new FlowPanel();
	public static FlowPanel fpBottoni = new FlowPanel();
	public static FlowPanel fpTesto = new FlowPanel();
	public static FlowPanel fpHeader = new FlowPanel();
	public static HTMLPanel titleHeader = new HTMLPanel("<h4>Admin Dashboard</h4>");


	private static Label testoCentraleDashboard = new Label("Questa e' la dashboard amministratore. Da qui puoi gestire le componenti della piattaforma."
			+ "gestione degli articoli, gestione degli utenti e moderazione dei commenti.Ogni sezioni ha la sua pagina dedicata, selezionare quella scelta premendo il pulsante relativo qui sotto");

	private static Button buttonArticoli = new Button("Gestione articoli");
	private static Button buttonCategorie = new Button("Gestione categorie");
	private static Button buttonCommenti = new Button("Moderazione commenti");
	private static Button logoutBtn = new Button("Log-Out");
	private static String layoutBtn = "m-2 btn btn-primary btn-sm";

	public static void main() {
		
		
		logoutBtn.setStyleName("btn btn-outline-secondary btn-sm float-right");
		fpHeader.add(logoutBtn);
		fpHeader.add(titleHeader);
		titleHeader.getElement().setAttribute("align", "center"); 
		fpHeader.getElement().addClassName("col-12 mx-auto");

		buttonArticoli.getElement().addClassName("col-4");
		buttonCategorie.getElement().addClassName("col-4");
		buttonCommenti.getElement().addClassName("col-4");
		buttonArticoli.setStyleName(layoutBtn);
		buttonCategorie.setStyleName(layoutBtn);
		buttonCommenti.setStyleName(layoutBtn);
		fpBottoni.add(buttonArticoli);
		fpBottoni.add(buttonCategorie);
		fpBottoni.add(buttonCommenti);
		fpBottoni.getElement().setAttribute("align", "center");
		
		fpTesto.add(testoCentraleDashboard);
		fpTesto.getElement().addClassName("col-8 mx-auto");

		fpContainer.add(fpHeader);
		fpContainer.add(fpTesto);
		fpContainer.add(fpBottoni);

		fpContainer.getElement().addClassName("col-8 mx-auto container border rounded-corners");

		if (vpDashboard == null) {

			vpDashboard = new VerticalPanel();

			setClickHandlers();

		}


		vpDashboard.add(fpContainer);
		vpDashboard.getElement().addClassName("w-100");

		RootPanel.get().add(vpDashboard);

		vpDashboard.setVisible(true);
	}
	
	/*
	 * CLICK HANDLERS
	 */

	private static void setClickHandlers() {
		
		logoutBtn.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				vpDashboard.setVisible(false);
				Home.main();			
			}
		});

		buttonArticoli.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				vpDashboard.setVisible(false);
				ArticleManager.main();
			}
		}); 

		buttonCommenti.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				vpDashboard.setVisible(false);
				CommentManager.main();
			}
		});

		buttonCategorie.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				vpDashboard.setVisible(false);
				CategoryManager.main();
			}
		});

	}

}
