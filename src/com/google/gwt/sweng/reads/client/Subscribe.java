package com.google.gwt.sweng.reads.client;

import java.util.Date;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.sweng.reads.shared.User;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Grid;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.datepicker.client.DatePicker;

public class Subscribe {

	//Create a remote service proxy to talk to the server-side Greeting service.   
	private static final GreetingServiceAsync greetingService = GWT.create(GreetingService.class);
	private static final SessionServiceAsync sessionService = GWT.create(SessionService.class); 

	//OGGETTI
	private static HTMLPanel titleAbb = new HTMLPanel("<h4>ABBONAMENTO</h4>");
	private static Label entryLabelSubscribe = new Label("Inserisci i seguenti dati:");
	private static Label cardNumberLabelSubscribe = new Label("Numero di carta:");
	private static TextBox cardNumberTextBoxSubscribe = new TextBox();
	private static Label nameLabelSubscribe = new Label("Nome titolare conto:");
	private static TextBox nameTextBoxSubscribe = new TextBox();
	private static Label surnameLabelSubscribe = new Label("Cognome titolare conto:");
	private static TextBox surnameTextBoxSubscribe = new TextBox();
	private static Label dateLabelSubscribe = new Label("Data scadenza:");
	private static DatePicker dateDatePickerSubscribe = new DatePicker();
	private static Label cvvLabelSubscribe = new Label("cvv:");
	private static TextBox cvvTextBoxSubscribe = new TextBox();
	private static Label nationalityLabelSubscribe = new Label("Nazionalita':");
	private static TextBox nationalityTextBoxSubscribe = new TextBox();
	private static Label capLabelSubscribe = new Label("CAP:");
	private static TextBox capTextBoxSubscribe = new TextBox();

	private static Button abbonarsi = new Button("Abbonati");
	private static Button goBackSubscribe = new Button("Torna indietro");
	public static Label esito = new Label();

	//private static User utenteLoggato = new User();

	//LAYOUT
	private static VerticalPanel vpSubscribe;
	private static Grid grid = new Grid(8,2);
	//container
	public static FlowPanel fpContainer = new FlowPanel();

	public static void main() {

		titleAbb.getElement().addClassName("title"); 
		
		abbonarsi.setStyleName("btn btn-primary btn-sm mt-2");
		goBackSubscribe.setStyleName("btn btn-outline-secondary btn-sm mt-2");
		
		dateDatePickerSubscribe.setYearAndMonthDropdownVisible(true);
		
		grid.setWidget(0, 0, cardNumberLabelSubscribe);
		grid.setWidget(0, 1, cardNumberTextBoxSubscribe);
		grid.setWidget(1, 0, nameLabelSubscribe);
		grid.setWidget(1, 1, nameTextBoxSubscribe);
		grid.setWidget(2, 0, surnameLabelSubscribe);
		grid.setWidget(2, 1, surnameTextBoxSubscribe);
		grid.setWidget(3, 0, dateLabelSubscribe);
		grid.setWidget(3, 1, dateDatePickerSubscribe);
		grid.setWidget(4, 0, cvvLabelSubscribe);
		grid.setWidget(4, 1, cvvTextBoxSubscribe);
		grid.setWidget(5, 0, nationalityLabelSubscribe);
		grid.setWidget(5, 1, nationalityTextBoxSubscribe);
		grid.setWidget(6, 0, capLabelSubscribe);
		grid.setWidget(6, 1, capTextBoxSubscribe);
		grid.setWidget(7, 0, abbonarsi);
		grid.setWidget(7, 1, goBackSubscribe);
		
		fpContainer.add(titleAbb);
		fpContainer.add(grid);

		fpContainer.getElement().addClassName("col-8 mx-auto container border-primary rounded-corners");
		fpContainer.getElement().setAttribute("align", "center");
		//azione dateDatePickerSubscribe
		dateDatePickerSubscribe.addValueChangeHandler(new ValueChangeHandler<Date>() {
			@Override
			public void onValueChange(ValueChangeEvent<Date> valueChangeEvent) {
				Date date = valueChangeEvent.getValue();
				String dateString = DateTimeFormat.getMediumDateFormat().format(date);
			}
		});


		if (vpSubscribe == null) {

			vpSubscribe = new VerticalPanel();

			setClickHandlers();

		}

		vpSubscribe.add(fpContainer);
		vpSubscribe.getElement().addClassName("w-100");

		RootPanel.get().add(vpSubscribe);
		vpSubscribe.setVisible(true);

	}

	private static void setClickHandlers() {
		//azione goBackRegistrazione
		goBackSubscribe.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent clickEvent) {
				vpSubscribe.setVisible(false);
				Home.main();
			}
		});

		abbonarsi.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {

				sessionService.getSession(new AsyncCallback<User>() {

					@Override
					public void onFailure(Throwable caught) {
						Window.alert("Errore nel caricamento della sessione. " + caught.toString());
					}

					@Override
					public void onSuccess(User result) {

						greetingService.subscribe(result.getUsername(), cardNumberTextBoxSubscribe.getText(), nameTextBoxSubscribe.getText(),
								surnameTextBoxSubscribe.getText(), dateDatePickerSubscribe.getValue().toString(), cvvTextBoxSubscribe.getText(),
								nationalityTextBoxSubscribe.getText(), capTextBoxSubscribe.getText(), new AsyncCallback<User>() {

							@Override
							public void onSuccess(User result) {
								
								if (result != null) {
									Window.alert("Grazie per l'abbonamento " + result.getUsername());

									sessionService.setSession(result, new AsyncCallback<Void>() {

										@Override
										public void onFailure(Throwable caught) {
											Window.alert("Errore aggiornamento sessione. " + caught.toString());
										}

										@Override
										public void onSuccess(Void result) {
											// Do Nothing
										}
										
									});

									vpSubscribe.setVisible(false);
									Home.main();

								} else {
									Window.alert("Errore database.  Utente non trovato.");
									Home.main();									
								}
							}

							@Override
							public void onFailure(Throwable caught) {
								Window.alert("Errore richiesta. " + caught.toString());
								Home.main();
							}
						});
					}
				});				
			}
		});
	}

}