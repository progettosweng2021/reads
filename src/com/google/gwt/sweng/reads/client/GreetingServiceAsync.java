package com.google.gwt.sweng.reads.client;

import java.util.ArrayList;
import java.util.Date;

import com.google.gwt.sweng.reads.shared.Admin;
import com.google.gwt.sweng.reads.shared.Article;
import com.google.gwt.sweng.reads.shared.Author;
import com.google.gwt.sweng.reads.shared.Category;
import com.google.gwt.sweng.reads.shared.Comment;
import com.google.gwt.sweng.reads.shared.Gender;
import com.google.gwt.sweng.reads.shared.Like;
import com.google.gwt.sweng.reads.shared.Subscription;
import com.google.gwt.sweng.reads.shared.User;
import com.google.gwt.user.client.rpc.AsyncCallback;

/**
 * La controparte "async" di GreetingService.
 */

public interface GreetingServiceAsync {

	void printArticle(String tit, AsyncCallback<Article> callback) throws IllegalArgumentException;

	void getArticleComments(String tit, AsyncCallback<ArrayList<Comment>> callback) throws IllegalArgumentException;
	
	void newComment(Comment newComm, AsyncCallback<Boolean> callback) throws IllegalArgumentException;
	
	void newLike(Like newLi, AsyncCallback<Boolean> callback) throws IllegalArgumentException;
	
	void getSubscriptionInfo(User utenteLoggato,AsyncCallback<Subscription> callback) throws IllegalArgumentException;
	
	void getArticles(String cat, AsyncCallback<ArrayList<Article>> callback) throws IllegalArgumentException;
	
	void getArticlesSubCat(String cat, AsyncCallback<ArrayList<Article>> callback) throws IllegalArgumentException;

	void getListArticles(AsyncCallback<ArrayList<Article>> callback) throws IllegalArgumentException;

	void adminLogin(String password, AsyncCallback<Admin> callback) throws IllegalArgumentException;

	void login(String username, String password, AsyncCallback<User> callback) throws IllegalArgumentException;

	void getUserInfo(String nomeUtenteCorrente, AsyncCallback<User> asyncCallback) throws IllegalArgumentException;

	void registration(String username, String email, String password, String name, String surname, Date birthDate, String birthCity, Gender genre, String address, ArrayList<String> listFavCat, AsyncCallback<User> callback) throws IllegalArgumentException;

	void subscribe(String nomeUtente, String cardNumber, String name, String surname, String expirationDate, String CVV, String nation, String cap, AsyncCallback<User> asyncCallback) throws IllegalArgumentException;

	void printAllUsers(AsyncCallback<String> asyncCallback) throws IllegalArgumentException;
	
	void getArticleAuthor(String titoloArticolo, AsyncCallback<Author> asyncCallback) throws IllegalArgumentException;


	/*
	 * ADMIN
	 */

	 void getAllTitles(AsyncCallback<ArrayList<String>> callback) throws IllegalArgumentException;

	 void getCategories(AsyncCallback<ArrayList<String>> callback) throws IllegalArgumentException;

	 void getSubCategories(String cat, AsyncCallback<ArrayList<String>> callback) throws IllegalArgumentException;

	 void getAllAuthors(AsyncCallback<ArrayList<String>> callback);

	 void getArticle(String art, AsyncCallback<Article> callback) throws IllegalArgumentException;

	 void addArticle(Article art, AsyncCallback<Boolean> callback) throws IllegalArgumentException;

	 void modifyArticle(String t, Article a, AsyncCallback<Boolean> callback) throws IllegalArgumentException;

	 void deleteArticle(String t, AsyncCallback<Boolean> callback) throws IllegalArgumentException;

	 void addCategory(Category c, AsyncCallback<Boolean> callback) throws IllegalArgumentException;

	 void addSubCategory(String cat, String subcat, AsyncCallback<Boolean> callback) throws IllegalArgumentException;

	 void deleteCategory(String cat, AsyncCallback<Boolean> callback) throws IllegalArgumentException;

	 void deleteSubCategory(String cat, String subcat, AsyncCallback<Boolean> callback) throws IllegalArgumentException;

	 void getUnmoderatedComments(AsyncCallback<ArrayList<Comment>> callback) throws IllegalArgumentException;

	 void getUnmoderatedComments(String t, AsyncCallback<ArrayList<Comment>> callback) throws IllegalArgumentException;

	 void getModeratedComments(AsyncCallback<ArrayList<Comment>> callback) throws IllegalArgumentException;

	 void getModeratedComments(String t, AsyncCallback<ArrayList<Comment>> callback) throws IllegalArgumentException;

	 void approveComment(String text, AsyncCallback<Boolean> callback) throws IllegalArgumentException;

	 void deleteComment(String text, AsyncCallback<Boolean> callback) throws IllegalArgumentException;

	void authorRegistration(String username, String email, String pw, String name, String surname, Date dateReg,
			AsyncCallback<Author> asyncCallback);

	void getActivityComments(String name, AsyncCallback<ArrayList<String>> callback);

	void getActivityLikes(String username, AsyncCallback<ArrayList<String>> asyncCallback);

	void loginAuthor(String username, String password, AsyncCallback<Author> asyncCallback);

	void getListArticleAuthor(String username, AsyncCallback<ArrayList<String>> asyncCallback);


}
