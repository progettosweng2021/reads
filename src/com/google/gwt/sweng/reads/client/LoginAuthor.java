package com.google.gwt.sweng.reads.client;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.sweng.reads.shared.Author;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.PasswordTextBox;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;

public class LoginAuthor {

	//Create a remote service proxy to talk to the server-side Greeting service.   
	private static final GreetingServiceAsync greetingService = GWT.create(GreetingService.class);
	private static final SessionServiceAsync sessionService = GWT.create(SessionService.class); 

	//OGGETTI LOGIN
	final static String styleButton = "m-2 btn btn-primary btn-sm";
	private static Label entryLabelLogin = new Label("Login Autore");
	private static Label nameLabel = new Label("Username: ");
	private static TextBox nameTextBoxLogin = new TextBox();
	private static PasswordTextBox pwLabel = new PasswordTextBox();
	private static Label password = new Label("Password: ");
	private static Button login = new Button("Login");
	private static Button goBackLogin = new Button("Torna indietro");	
	private static Label reg = new Label("Sei un autore e non ti sei ancora registrato?");
	private static Button regButton = new Button("Registrati!");	
	private static HTML serverResponseLabel = new HTML();

	//LAYOUT LOGIN
	public static VerticalPanel vpLogin;
	public static FlowPanel fpLogin = new FlowPanel();
	public static FlowPanel fpReg = new FlowPanel();
	public static FlowPanel fpAut = new FlowPanel();
	public static FlowPanel fpContainer = new FlowPanel();

	public static void main() {
		//LOGIN front
		nameTextBoxLogin.setText("");
		pwLabel.setText("");

		login.setStyleName(styleButton);
		goBackLogin.setStyleName("btn btn-outline-secondary btn-sm mt-3");
		entryLabelLogin.setStyleName("h4");
		nameTextBoxLogin.setStyleName("m-2");
		pwLabel.setStyleName("m-2");
		nameLabel.setStyleName( "d-inline mx-auto", true);
		password.setStyleName( "d-inline mx-auto", true);
		reg.setStyleName("d-inline h6 align-items-center");
		regButton.setStyleName("m-2 btn btn-outline-primary btn-sm");

		fpLogin.add(nameLabel);
		fpLogin.add(nameTextBoxLogin);
		fpLogin.add(password);
		fpLogin.add(pwLabel);
		fpLogin.add(login);
		fpLogin.getElement().setAttribute("align", "center");

		fpReg.add(reg);
		fpReg.add(regButton);
		fpReg.getElement().setAttribute("align", "center");
		fpAut.getElement().setAttribute("align", "center");

		fpContainer.add(entryLabelLogin);
		fpContainer.add(fpLogin);
		fpContainer.add(fpReg);
		fpContainer.add(fpAut);
		fpContainer.add(goBackLogin);		

		fpContainer.add(serverResponseLabel);
		fpContainer.getElement().setAttribute("align", "center");
		fpContainer.getElement().addClassName("col-8 mx-auto container rounded-corners");

		// Focus the cursor on the name field when the app loads
		nameTextBoxLogin.setFocus(true);
		nameTextBoxLogin.selectAll();

		if (vpLogin == null) {

			vpLogin = new VerticalPanel();

			setClickHandlers();

		} 

		vpLogin.add(fpContainer);
		vpLogin.getElement().addClassName("w-100");

		RootPanel.get().add(vpLogin);
		vpLogin.setVisible(true);

	}

	private static void setClickHandlers() {
		//button action
		login.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent clickEvent) {

				String username = nameTextBoxLogin.getValue();
				String password = pwLabel.getValue();

				//controllo login autore
				greetingService.loginAuthor(username,password, new AsyncCallback<Author>() {

					@Override
					public void onFailure(Throwable caught) {
						Window.alert("Errore Login: "+ caught.toString());
					}

					@Override
					public void onSuccess(Author result) {

						if (result != null) {
							Window.alert("Login Effettuato da " + result.getUsername());
							
							//viene settata la sessione autore
							sessionService.setSession(result, new AsyncCallback<Void>() {

								@Override
								public void onFailure(Throwable caught) {
									System.err.println("errore sessione. " + caught.toString());
								}

								@Override
								public void onSuccess(Void result) {
									vpLogin.setVisible(false);
									ReadInfoAuthor.main();        						
								}
							});

						} else {
							Window.alert("Errore nell'inserimento dei dati");
							vpLogin.setVisible(false);
							Home.main();
						}
					}
				});

			}
		});
		
		//handler bottone torna indietro
		goBackLogin.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent clickEvent) {
				vpLogin.setVisible(false);
				Home.main();
			}
		});

		
		//handler bottone registrazione autore
		regButton.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				vpLogin.setVisible(false);
				RegistrationAuthor.main();
			}
		});

	}


}
