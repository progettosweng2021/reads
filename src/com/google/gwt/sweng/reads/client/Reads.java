package com.google.gwt.sweng.reads.client;

import com.google.gwt.core.client.EntryPoint;


/**
 * Entry point classes define <code>onModuleLoad()</code>.
 */
public class Reads implements EntryPoint {


	public Reads() {}

	/**
	 * This is the entry point method.
	 */
	public void onModuleLoad() {
		Home.main();
	}
}
