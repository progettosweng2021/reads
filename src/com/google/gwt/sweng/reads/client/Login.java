package com.google.gwt.sweng.reads.client;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.sweng.reads.shared.Admin;
import com.google.gwt.sweng.reads.shared.User;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.PasswordTextBox;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;

public class Login {

	//Create a remote service proxy to talk to the server-side Greeting service.   
	private static final GreetingServiceAsync greetingService = GWT.create(GreetingService.class);
	private static final SessionServiceAsync sessionService = GWT.create(SessionService.class); 

	
	//OGGETTI LOGIN
	final static String styleButton = "m-2 btn btn-primary btn-sm";
	private static HTMLPanel titleLogin = new HTMLPanel("<h4>LOGIN</h4>");
	private static Label nameLabel = new Label("Username: ");
	private static TextBox nameTextBoxLogin = new TextBox();
	private static PasswordTextBox pwLabel = new PasswordTextBox();
	private static Label password = new Label("Password: ");
	private static Button login = new Button("Login");
	private static Button goBackLogin = new Button("Torna indietro");	
	private static Label reg = new Label("Non sei registrato?");
	private static Button regButton = new Button("Registrati!");	
	private static HTML serverResponseLabel = new HTML();

	private static Label authorLabel = new Label("Sei un autore?");
	private static Button btnLogAutore = new Button("Login autore");

	//LAYOUT LOGIN
	public static VerticalPanel vpLogin;
	public static FlowPanel fpLogin = new FlowPanel();
	public static FlowPanel fpReg = new FlowPanel();
	public static FlowPanel fpAut = new FlowPanel();
	public static FlowPanel fpContainer = new FlowPanel();

	public static void main() {
		nameTextBoxLogin.setText("");
		pwLabel.setText("");
		
		login.setStyleName(styleButton);
		goBackLogin.setStyleName("btn btn-outline-secondary btn-sm mt-3");
		nameTextBoxLogin.setStyleName("m-2");
		pwLabel.setStyleName("m-2");
		nameLabel.setStyleName( "d-inline mx-auto", true);
		password.setStyleName( "d-inline mx-auto", true);
		reg.setStyleName("d-inline h6 align-items-center");
		regButton.setStyleName("m-2 btn btn-outline-primary btn-sm");
		authorLabel.setStyleName("d-inline align-items-center");
		btnLogAutore.setStyleName("m-2 btn btn-outline-primary btn-sm");

		fpLogin.add(nameLabel);
		fpLogin.add(nameTextBoxLogin);
		fpLogin.add(password);
		fpLogin.add(pwLabel);
		fpLogin.add(login);
		fpLogin.getElement().setAttribute("align", "center");
		
		fpReg.add(reg);
		fpReg.add(regButton);
		fpAut.add(authorLabel);
		fpAut.add(btnLogAutore);
		fpReg.getElement().setAttribute("align", "center");
		fpAut.getElement().setAttribute("align", "center");
		
		titleLogin.getElement().addClassName("title"); 
		
		fpContainer.add(titleLogin);
		fpContainer.add(fpLogin);
		fpContainer.add(fpReg);
		fpContainer.add(fpAut);
		fpContainer.add(goBackLogin);		
		
		fpContainer.add(serverResponseLabel);
		fpContainer.getElement().setAttribute("align", "center");
		fpContainer.getElement().addClassName("col-8 mx-auto container rounded-corners");

		nameTextBoxLogin.setFocus(true);
		nameTextBoxLogin.selectAll();

		
		//Per evitare il problema della duplicazione
		if (vpLogin == null) {

			vpLogin = new VerticalPanel();

			setClickHandlers();

		} 

		vpLogin.add(fpContainer);
		vpLogin.getElement().addClassName("w-100");

		RootPanel.get().add(vpLogin);
		vpLogin.setVisible(true);

	}

	
	//Gestione dei ClickHandlers
	private static void setClickHandlers() {
		
		//Login
		login.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent clickEvent) {

				String username = nameTextBoxLogin.getValue();
				String password = pwLabel.getValue();

				//Se effettua l'accesso l'admin
				if (username.equals("admin")) {

					greetingService.adminLogin(password, new AsyncCallback<Admin>() {

						@Override
						public void onFailure(Throwable caught) {
							
							Window.alert("Errore Login Admin:\n"+ caught.toString());
							
						}

						public void onSuccess(Admin result) {
							if (result != null ) {
								
								Window.alert("Reindirizzamento ad amministrazione");

								vpLogin.setVisible(false);
								AdminDashboard.main();
								
							} else {
								
								Window.alert("Errore nell'inserimento della password dell'admin");
								
							}
						}
					});

				} else {

					greetingService.login(username,password, new AsyncCallback<User>() {

						@Override
						public void onFailure(Throwable caught) {
							
							Window.alert("Errore Login:\n"+ caught.toString());
						
						}

						@Override
						public void onSuccess(User result) {

							if (result != null) {
								//Messaggio di conferma del login
								Window.alert("Login Effettuato da " + result.getUsername());

								//Attivo la sessione con l'utente che ha effettuato l'accesso
								sessionService.setSession(result, new AsyncCallback<Void>() {

									@Override
									public void onFailure(Throwable caught) {
										
										Window.alert("Errore setSession:\n"+ caught.toString());
										
									}

									@Override
									public void onSuccess(Void result) {
										
										vpLogin.setVisible(false);
										Home.main();     
										
									}
								});

							} else {
								
								Window.alert("Errore nell'inserimento dei dati!");
								
							}
						}
					});
				}
			}
		});

		//Go back
		goBackLogin.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent clickEvent) {
				
				vpLogin.setVisible(false);
				Home.main();
				
			}
		});
		
		//Registrazione
		regButton.addClickHandler(new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				
				vpLogin.setVisible(false);
				Registration.main();
				
			}
		});
		
		//Login autore
		btnLogAutore.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				
				vpLogin.setVisible(false);
				LoginAuthor.main();			
				
			}
		});
	}
}
