package com.google.gwt.sweng.reads.client;

import java.util.ArrayList;
import java.util.Date;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.sweng.reads.shared.Gender;
import com.google.gwt.sweng.reads.shared.User;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Grid;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.PasswordTextBox;
import com.google.gwt.user.client.ui.RadioButton;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.datepicker.client.DatePicker;

public class Registration {

	// Create a remote service proxy to talk to the server-side Greeting service.
	private static final GreetingServiceAsync greetingService = GWT.create(GreetingService.class);

	// OGGETTI REGISTRAZIONE
	private static HTMLPanel titleReg = new HTMLPanel("<h4>REGISTRAZIONE</h4>");
	private static Grid gridRegistrazione = new Grid(4, 4);
	private static Label entryLabelRegistrazione = new Label("Inserisci i tuoi dati:");
	private static Label nameLabelRegistrazione = new Label("Nome (*):");
	private static TextBox nameTextBoxRegistrazione = new TextBox();
	private static Label surnameLabelRegistrazione = new Label("Cognome (*):");
	private static TextBox surnameTextBoxRegistrazione = new TextBox();
	private static Label usernameLabelRegistrazione = new Label("Username (*):");
	private static TextBox usernameTextBoxRegistrazione = new TextBox();
	private static Label dateLabelRegistrazione = new Label("Data di nascita (*):");
	private static DatePicker dateDatePickerRegistrazione = new DatePicker();
	private static Label emailLabelRegistrazione = new Label("Email (*):");
	private static TextBox emailTextBoxRegistrazione = new TextBox();
	private static Label pwLabelRegistrazione = new Label("Password (*):");
	private static PasswordTextBox pwPasswordTextBoxRegistrazione = new PasswordTextBox();
	private static Label addressLabelRegistrazione = new Label("Indirizzo (*):");
	private static TextBox addressTextBoxRegistrazione = new TextBox();
	private static Label sexLabelRegistrazione = new Label("Genere:");
	private static RadioButton maleRadioButtonRegistrazione = new RadioButton("RadioGender", "Male");
	private static RadioButton femaleRadioButtonRegistrazione = new RadioButton("RadioGender", "Female");
	private static RadioButton otherRadioButtonRegistrazione = new RadioButton("RadioGender", "Other");
	private static Label birthPlaceLabelRegistrazione = new Label("Luogo di nascita:");
	private static TextBox birthPlaceTextBoxRegistrazione = new TextBox();
	private static Label categoryLabelRegistrazione = new Label("Categorie preferite:");
	private static ArrayList<String> listFavCat = new ArrayList<String>();
	private static Label necessaryLabelRegistrazione = new Label("(*): campi obbligatori");
	private static Button registrazione = new Button("Registrati");
	private static Label loginLabel = new Label("Sei registrato?");
	private static Button login = new Button("Login");
	private static Button goBackRegistrazione = new Button("Torna indietro");
	public static Label esito = new Label();

	// LAYOUT REGISTRAZIONE
	private static VerticalPanel vpRegistrazione;
	private static FlowPanel fpDateRegistrazione = new FlowPanel();
	private static FlowPanel fpSexRegistrazione = new FlowPanel();
	private static FlowPanel fpCategoryRegistrazione = new FlowPanel();
	private static FlowPanel fpButtonsRegistrazione = new FlowPanel();
	private static FlowPanel fpLogin = new FlowPanel();
	// container
	public static FlowPanel fpContainer = new FlowPanel();

	final static String styleLabel = "mb-4 d-inline";
	final static String styleTB = "mr-4";

	public static void main(){
		nameLabelRegistrazione.setStyleName(styleLabel);
		addressLabelRegistrazione.setStyleName(styleLabel);
		birthPlaceLabelRegistrazione.setStyleName(styleLabel);
		categoryLabelRegistrazione.setStyleName(styleLabel);
		dateLabelRegistrazione.setStyleName(styleLabel);
		emailLabelRegistrazione.setStyleName(styleLabel);
		usernameLabelRegistrazione.setStyleName(styleLabel);
		surnameLabelRegistrazione.setStyleName(styleLabel);
		pwLabelRegistrazione.setStyleName(styleLabel);
		sexLabelRegistrazione.setStyleName(styleLabel);
		nameTextBoxRegistrazione.setStyleName(styleTB);
		addressTextBoxRegistrazione.setStyleName(styleTB);
		birthPlaceTextBoxRegistrazione.setStyleName(styleTB);
		emailTextBoxRegistrazione.setStyleName(styleTB);
		usernameTextBoxRegistrazione.setStyleName(styleTB);
		surnameTextBoxRegistrazione.setStyleName(styleTB);
		pwPasswordTextBoxRegistrazione.setStyleName(styleTB);
		dateDatePickerRegistrazione.setYearAndMonthDropdownVisible(true);
		
		registrazione.setStyleName("m-2 btn btn-primary btn-sm");
		goBackRegistrazione.setStyleName("m-2 btn btn-outline-secondary btn-sm");
		login.setStyleName("m-2 btn btn-primary btn-sm");
		loginLabel.setStyleName(styleLabel);
		
		fpSexRegistrazione.add(maleRadioButtonRegistrazione);
		fpSexRegistrazione.add(femaleRadioButtonRegistrazione);
		fpSexRegistrazione.add(otherRadioButtonRegistrazione);
		fpDateRegistrazione.add(dateLabelRegistrazione);
		fpDateRegistrazione.add(dateDatePickerRegistrazione);
		fpButtonsRegistrazione.add(registrazione);
		fpButtonsRegistrazione.add(goBackRegistrazione);
		fpLogin.add(loginLabel);
		fpLogin.add(login);

		titleReg.getElement().addClassName("title");
		
		fpSexRegistrazione.getElement().setAttribute("align", "center");
		
		gridRegistrazione.setWidget(0, 0, nameLabelRegistrazione);
		gridRegistrazione.setWidget(0, 1, nameTextBoxRegistrazione);
		gridRegistrazione.setWidget(0, 2, surnameLabelRegistrazione);
		gridRegistrazione.setWidget(0, 3, surnameTextBoxRegistrazione);
		gridRegistrazione.setWidget(1, 0, usernameLabelRegistrazione);
		gridRegistrazione.setWidget(1, 1, usernameTextBoxRegistrazione);
		gridRegistrazione.setWidget(2, 0, emailLabelRegistrazione);
		gridRegistrazione.setWidget(2, 1, emailTextBoxRegistrazione);
		gridRegistrazione.setWidget(1, 2, pwLabelRegistrazione);
		gridRegistrazione.setWidget(1, 3, pwPasswordTextBoxRegistrazione);
		gridRegistrazione.setWidget(2, 2, addressLabelRegistrazione);
		gridRegistrazione.setWidget(2, 3, addressTextBoxRegistrazione);
		gridRegistrazione.setWidget(3, 2, sexLabelRegistrazione);
		gridRegistrazione.setWidget(3, 3, fpSexRegistrazione);
		gridRegistrazione.setWidget(3, 0, birthPlaceLabelRegistrazione);
		gridRegistrazione.setWidget(3, 1, birthPlaceTextBoxRegistrazione);
		
		fpContainer.add(titleReg);
		fpContainer.add(entryLabelRegistrazione);
		fpContainer.add(gridRegistrazione);
		fpContainer.add(fpDateRegistrazione);
		fpContainer.add(fpCategoryRegistrazione);
		fpContainer.add(necessaryLabelRegistrazione);
		fpContainer.add(fpButtonsRegistrazione);
		fpContainer.add(fpLogin);
		
		fpContainer.getElement().addClassName("col-8 mx-auto container border rounded-corners");
		fpContainer.getElement().setAttribute("align", "center");

		// azione dateDatePickerRegistrazione
		dateDatePickerRegistrazione.addValueChangeHandler(new ValueChangeHandler<Date>() {
			@Override
			public void onValueChange(ValueChangeEvent<Date> valueChangeEvent) {
				Date date = valueChangeEvent.getValue();
				String dateString = DateTimeFormat.getMediumDateFormat().format(date);
				// necessaryLabelRegistrazione.setText(dateString);
			}
		});

		if (vpRegistrazione == null) {

			vpRegistrazione = new VerticalPanel();

			setClickHandlers();

		}

		// stampo la lista delle categorie
		greetingService.getCategories(new AsyncCallback<ArrayList<String>>() {

			@Override
			public void onFailure(Throwable caught) {
				Window.alert("Errore nel caricamento delle categorie");

			}

			@Override
			public void onSuccess(ArrayList<String> result) {

				fpCategoryRegistrazione.clear();

				fpCategoryRegistrazione.add(categoryLabelRegistrazione);

				for (final String cat : result) {

					final CheckBox cb = new CheckBox(cat);

					fpCategoryRegistrazione.add(cb);

					// memorizzo in listFavCat tutte e solo le categorie selezionate
					cb.addClickHandler(new ClickHandler() {

						@Override
						public void onClick(ClickEvent event) {

							if (cb.isChecked()) {
								listFavCat.add(cat);
							} else {
								listFavCat.remove(cat);
							}

						}

					});
				}

			}

		});

		vpRegistrazione.add(fpContainer);
		vpRegistrazione.getElement().addClassName("w-100");

		RootPanel.get().add(vpRegistrazione);

		vpRegistrazione.setVisible(true);

	}

	private static void setClickHandlers() {

		registrazione.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent clickEvent) {

				String name = nameTextBoxRegistrazione.getText();
				String surname = surnameTextBoxRegistrazione.getText();
				String username = usernameTextBoxRegistrazione.getText();
				Date birthDate = dateDatePickerRegistrazione.getValue();
				String email = emailTextBoxRegistrazione.getText();
				String pw = pwPasswordTextBoxRegistrazione.getText();
				String address = addressTextBoxRegistrazione.getText();
				String birthCity = birthPlaceTextBoxRegistrazione.getText();

				// controllo che i valori OBBLIGATORI siano stati tutti inseriti
				if (name == "" || surname == "" || username == "" || birthDate == null || email == "" || pw == ""
						|| address == "") {
					Window.alert("ERRORE:\n"
							+ "Per effettuare la registrazione � obbligatorio inserire le seguenti informazioni:\n"
							+ "> Nome;\n" + "> Cognome;\n" + "> Username;\n" + "> Data di nascita;\n" + "> Email;\n"
							+ "> Password;\n" + "> Indirizzo;");
				} else {
					greetingService.registration(username, email, pw, name, surname, birthDate, birthCity,
							genereSelezionato(), address, listFavCat, new AsyncCallback<User>() {

								@Override
								public void onFailure(Throwable caught) {
									Window.alert("Errore Registrazione: " + caught.toString());
								}

								@Override
								public void onSuccess(User result) {
									if (result != null) {
										Window.alert("Registrazione effettuata con successo!");
										vpRegistrazione.setVisible(false);

										Login.main();
									} else {
										Window.alert("Errore nell'inserimento dei dati!\n"
												+ "Per effettuare la registrazione � obbligatorio inserire le seguenti informazioni:\n"
												+ "> Nome;\n" + "> Cognome;\n" + "> Username;\n"
												+ "> Data di nascita;\n" + "> Email;\n" + "> Password;\n"
												+ "> Indirizzo;");
									}

								}
							});
				}

			}
		});

		// azione goBackRegistrazione
		goBackRegistrazione.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent clickEvent) {
				vpRegistrazione.setVisible(false);
				Home.main();
			}
		});
		
		login.addClickHandler(new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				vpRegistrazione.setVisible(false);
				Login.main();
			}
		});

	}

	private static Gender genereSelezionato() {
		if (maleRadioButtonRegistrazione.isChecked()) {
			return Gender.Male;
		} else if (femaleRadioButtonRegistrazione.isChecked()) {
			return Gender.Female;
		} else {
			return Gender.Other;
		}
	}
}