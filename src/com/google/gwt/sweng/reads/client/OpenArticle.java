package com.google.gwt.sweng.reads.client;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.sweng.reads.shared.Article;
import com.google.gwt.sweng.reads.shared.Comment;
import com.google.gwt.sweng.reads.shared.Like;
import com.google.gwt.sweng.reads.shared.User;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.*;

import java.util.ArrayList;

public class OpenArticle {

	//Create a remote service proxy to talk to the server-side Greeting service.   
	private static final GreetingServiceAsync greetingService = GWT.create(GreetingService.class);
	private static final SessionServiceAsync sessionService = GWT.create(SessionService.class); 

	//OGGETTI
	final static String styleButton = "m-2 btn btn-primary btn-sm";
	private static Button like = new Button("Like");
	private static Button comment = new Button("Commento");
	private static Button infoAuthor = new Button("Info autore");
	private static Button goBack = new Button("Torna indietro");


	//LAYOUT
	private static VerticalPanel vpOpenArticle;
	static String titleArt;
	static User utenteLoggato;
	
	private static FlowPanel fpButtons = new FlowPanel();
	//container
	public static FlowPanel fpContainerArt = new FlowPanel();
	public static FlowPanel fpContainerCom = new FlowPanel();
	public static FlowPanel fpContainerBtt = new FlowPanel();
	public static FlowPanel fpContainer = new FlowPanel();


	public static void main(String title) {
		
		
		like.setStyleName(styleButton);
		comment.setStyleName(styleButton);
		infoAuthor.setStyleName(styleButton);
		goBack.setStyleName("m-2 btn btn-outline-secondary btn-sm");
		goBack.setStyleName("float-right",true);
		
		titleArt = title;
		fpButtons.add(like);
		fpButtons.add(comment);
		fpButtons.add(infoAuthor);
		fpButtons.add(goBack);

		
		//Per evitare il problema della duplicazione
		if (vpOpenArticle == null) {

			vpOpenArticle = new VerticalPanel();

			setClickHandlers();

		}
		
		//devo recuperare l'username dell'utente loggato					
		sessionService.getSession(new AsyncCallback<User>() {
			
			@Override
			public void onFailure(Throwable caught) {
				Window.alert("Errore nel caricamento della sessione. " + caught.toString());
			}

			@Override
			public void onSuccess(User result) {
				
				utenteLoggato = result;
				
			}

		});
	
		
		//stampo l'articolo
		greetingService.printArticle(title, new AsyncCallback<Article>() {
			@Override
			public void onFailure(Throwable caught) {
				Window.alert("Errore stampa articoli: "+ caught.toString());
			}
			
			@Override
			public void onSuccess(Article result) {
				if (result != null ) {
					fpContainerArt.add(result.getPanelArticle());
					
					HTMLPanel likeComments = new HTMLPanel("<span class=\"col-2 badge badge-light\"> " + result.getNumTotLike() + " Mi piace</span> "
							+ "<span class=\"badge badge-light\"> " + result.getNumTotComm() + " Commenti</span>");
					likeComments.setStyleName("align-items-center");

					FlowPanel fpLastLine = new FlowPanel();
					fpLastLine.add(likeComments);
					fpLastLine.getElement().setClassName("mt-2");
					likeComments.setStyleName("d-inline");
					fpContainerArt.add(fpLastLine);
					
				} else {
					Window.alert("Errore nell'inserimento dei dati");
				}
			}
		});

		
		greetingService.getArticleComments(title, new AsyncCallback<ArrayList<Comment>>() {
			@Override
			public void onFailure(Throwable caught) {
				Window.alert("Errore stampa commenti: " + caught.toString());
			}

			@Override
			public void onSuccess(ArrayList<Comment> result) {
				if (result != null ) {
					
					for (Comment comm : result) {
						
						//aggiungo il commento solo se questo 
						if(comm.isApproved())
							
							fpContainerCom.add(comm.getPanelComment());
						
					}
					
				} else {
					
					Window.alert("Errore nell'inserimento dei dati");
					
				}
			}
		});
		
		fpContainerBtt.add(fpButtons);
		
		fpContainer.add(fpContainerArt);
		fpContainer.add(fpContainerCom);
		fpContainer.add(fpContainerBtt);
		fpContainer.getElement().addClassName("col-8 mx-auto container border border-primary rounded-corners");
		
		vpOpenArticle.add(fpContainer);
		vpOpenArticle.getElement().addClassName("w-100");
		RootPanel.get().add(vpOpenArticle);
		vpOpenArticle.setVisible(true);

	}

	private static void setClickHandlers() {
		//azione goBackRegistrazione
		goBack.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent clickEvent) {
				fpContainerArt.clear();		//pulisco il panel cos� che, entrando poi con un nuovo articolo, questo non si sovrapponga
				fpContainerCom.clear();	
				fpContainerBtt.clear();		
				vpOpenArticle.setVisible(false);

				Home.main();
			}
		});
		
		infoAuthor.addClickHandler(new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				fpContainerArt.clear();		//pulisco il panel cos� che, entrando poi con un nuovo articolo, questo non si sovrapponga
				fpContainerCom.clear();	
				fpContainerBtt.clear();
				vpOpenArticle.setVisible(false);

				InfoAuthor.main(titleArt);
								
			}
		});
		
		
		like.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				fpContainerArt.clear();		//pulisco il panel cos� che, entrando poi con un nuovo articolo, questo non si sovrapponga
				fpContainerCom.clear();	
				fpContainerBtt.clear();
				vpOpenArticle.setVisible(false);
				
				//controllo che l'utente sia loggato, altrimenti non potrebbe mettere like
				if(utenteLoggato != null) {
					
					//creo un nuovo like
					Like newLi = new Like(titleArt, utenteLoggato.getUsername());
					
					greetingService.newLike(newLi, new AsyncCallback<Boolean>() {

						@Override
						public void onFailure(Throwable caught) {
							
							Window.alert("Errore nel mettere mi piace:\n" + caught.toString());	
							
						}

						@Override
						public void onSuccess(Boolean result) {
							if(result == false) {
								Window.alert("Errore nell'inserimento del like: hai gi� messo mi piace al commento! ");
							} else {
								Window.alert("Like inserito correttamente!");
							}
							
							OpenArticle.main(titleArt);		//ricarico nuovamente la stessa pagina

						}

					});
				} else {
					
					//se l'utente non � loggato, lo reindirizzo alla registrazione
					Registration.main();
					
				}
			}
			
		});
		
		
		comment.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				fpContainerArt.clear();		//pulisco il panel cos� che, entrando poi con un nuovo articolo, questo non si sovrapponga
				fpContainerCom.clear();	
				fpContainerBtt.clear();
				vpOpenArticle.setVisible(false);
				
				//controllo che l'utente sia loggato, altrimenti non potrebbe scrivere nuovi commenti
				if(utenteLoggato != null) {
					
					NewComment.main(titleArt);
				
				} else {
					
					//se l'utente non � loggato, lo reindirizzo alla registrazione
					Registration.main();
					
				}
				
			}
			
		});
	}

}
