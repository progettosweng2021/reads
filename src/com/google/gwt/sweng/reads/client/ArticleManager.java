package com.google.gwt.sweng.reads.client;

import java.util.ArrayList;
import java.util.Date;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.sweng.reads.shared.Article;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Grid;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.TabPanel;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;

public class ArticleManager {
 
	private static final GreetingServiceAsync greetingService = GWT.create(GreetingService.class);

	public static VerticalPanel vpArticleManager;
	public static FlowPanel fpContainerA = new FlowPanel();
	public static FlowPanel fpBackButton = new FlowPanel();
	public static TabPanel tabPanel = new TabPanel();

	// Widget aggiunta nuovo articolo
	public static FlowPanel fpNewArticle = new FlowPanel();
	private static Grid newArticleGrid = new Grid(7, 2);
	private static HTMLPanel titleNewArticle = new HTMLPanel("<h4>Inserimento nuovo articolo</h4>");
	private static Label labelAdd = new Label("Compilare tutti i campi e premere il tasto di conferma");
	private static Button addArticle = new Button("Aggiungi nuovo articolo");
	private static Label labelTitle = new Label("Titolo articolo:");
	private static TextBox title = new TextBox();
	private static Label labelText = new Label("Testo dell'articolo:");
	private static TextArea text = new TextArea();
	private static Label selectListCat = new Label("Categoria:");
	private static ListBox listCat = new ListBox();
	private static Label selectListSubCat = new Label("Sottocategoria:");
	private static ListBox listSubCat = new ListBox();
	private static Label selectAuthor = new Label("Autore:");
	private static ListBox listAuthor = new ListBox();
	private static CheckBox checkPrem = new CheckBox("Premium");

	// Widget modifica articolo
	public static FlowPanel fpModifyArticle = new FlowPanel();
	private static Grid modifyArticleGrid = new Grid(8,2);
	private static HTMLPanel titleModifyArticle = new HTMLPanel("<h4>Modifica di un articolo</h4>");
	private static Label labelModify = new Label("Selezionare l'articolo da modificare e aggiornare i campi scelti, poi premere il tasto di conferma");
	private static Label selectArticle = new Label("Articolo:");
	private static ListBox listArticle = new ListBox();
	private static Label labelNewTitle = new Label("Nuovo titolo:");
	private static TextBox newTitle = new TextBox();
	private static Label labelNewText = new Label("Nuovo testo:");
	private static TextArea newText = new TextArea();
	private static Label selectListNewCat = new Label("Nuovo categoria:");
	private static ListBox listNewCat = new ListBox();
	private static Label selectListNewSubCat = new Label("Nuovo sottocategoria:");
	private static ListBox listNewSubCat = new ListBox();
	private static Label selectListNewAuthor = new Label("Nuovo autore:");
	private static ListBox listNewAuthor = new ListBox();
	private static CheckBox checkNewPrem = new CheckBox("Premium");
	private static Button modifyArticle = new Button("Modifica articolo");

	// Widget elimina articolo
	public static FlowPanel fpDeleteArticle = new FlowPanel();
	private static Grid deleteArticleGrid = new Grid(2,2);
	private static HTMLPanel titleDeleteArticle = new HTMLPanel("<h4>Cancellazione di un articolo</h4>");
	private static Label labelDelete = new Label("Selezionare l'articolo da eliminare e premere il tasto di conferma");
	private static Label selectDelete = new Label("Articolo:");
	private static ListBox listDelete = new ListBox();
	private static Button deleteArticle = new Button("Elimina articolo");

	private static Button backBtn = new Button("Torna indietro");
	private static String layoutBtn = "btn btn-primary btn-sm";

	// Metodo per controllare che tutti i campi del form siano riempiti
	private static boolean isAllFilled() {
		if (title.getText().isEmpty()
				|| text.getText().isEmpty() 
				|| listCat.getSelectedItemText().equals("-") 
				|| listAuthor.getSelectedItemText().equals("-"))
			return false;
		else return true;
	}

	public static void main() {

		
		backBtn.setStyleName("btn btn-outline-secondary btn-sm float-right");
		
		addArticle.setStyleName(layoutBtn);
		modifyArticle.setStyleName(layoutBtn);
		deleteArticle.setStyleName(layoutBtn);
		
		fpNewArticle.getElement().addClassName("col-12");
		fpNewArticle.getElement().setAttribute("align", "center");
		fpNewArticle.add(titleNewArticle);
		fpNewArticle.add(labelAdd);
		newArticleGrid.setWidget(0, 0, labelTitle);
		newArticleGrid.setWidget(0, 1, title);
		newArticleGrid.setWidget(1, 0, labelText);
		newArticleGrid.setWidget(1, 1, text);
		newArticleGrid.setWidget(2, 0, selectListCat);
		newArticleGrid.setWidget(2, 1, listCat);
		newArticleGrid.setWidget(3, 0, selectListSubCat);
		newArticleGrid.setWidget(3, 1, listSubCat);
		newArticleGrid.setWidget(4, 0, selectAuthor);
		newArticleGrid.setWidget(4, 1, listAuthor);
		newArticleGrid.setWidget(5, 0, checkPrem);
		newArticleGrid.setWidget(6, 1, addArticle);
		fpNewArticle.add(newArticleGrid);

		fpModifyArticle.getElement().addClassName("col-12");
		fpModifyArticle.getElement().setAttribute("align", "center");
		fpModifyArticle.add(titleModifyArticle);
		fpModifyArticle.add(labelModify);
		modifyArticleGrid.setWidget(0, 0, selectArticle);
		modifyArticleGrid.setWidget(0, 1, listArticle);
		modifyArticleGrid.setWidget(1, 0, labelNewTitle);
		modifyArticleGrid.setWidget(1, 1, newTitle);
		modifyArticleGrid.setWidget(2, 0, labelNewText);
		modifyArticleGrid.setWidget(2, 1, newText);
		modifyArticleGrid.setWidget(3, 0, selectListNewCat);
		modifyArticleGrid.setWidget(3, 1, listNewCat);
		modifyArticleGrid.setWidget(4, 0, selectListNewSubCat);
		modifyArticleGrid.setWidget(4, 1, listNewSubCat);
		modifyArticleGrid.setWidget(5, 0, selectListNewAuthor);
		modifyArticleGrid.setWidget(5, 1, listNewAuthor);
		modifyArticleGrid.setWidget(6, 0, checkNewPrem);
		modifyArticleGrid.setWidget(7, 1, modifyArticle);
		fpModifyArticle.add(modifyArticleGrid);

		fpDeleteArticle.getElement().addClassName("col-12");
		fpDeleteArticle.getElement().setAttribute("align", "center");
		fpDeleteArticle.add(titleDeleteArticle);
		fpDeleteArticle.add(labelDelete);
		deleteArticleGrid.setWidget(0, 0, selectDelete);
		deleteArticleGrid.setWidget(0, 1, listDelete);
		deleteArticleGrid.setWidget(1, 1, deleteArticle);
		fpDeleteArticle.add(deleteArticleGrid);

		tabPanel.getElement().addClassName("col-10 mx-auto container");
		tabPanel.getElement().setAttribute("align", "center");
		tabPanel.add(fpNewArticle,"Nuovo articolo");
		tabPanel.add(fpModifyArticle,"Modifica articolo");
		tabPanel.add(fpDeleteArticle,"Elimina articolo");
		tabPanel.selectTab(0);

		fpBackButton.add(backBtn);
		fpBackButton.setStyleName("mb-4");

		fpContainerA.add(fpBackButton);
		fpContainerA.add(tabPanel);
		fpContainerA.getElement().addClassName("col-8 mx-auto container border border-primary rounded-corners");

		/*
		 * Popolamento ListBox
		 */

		greetingService.getAllTitles(new AsyncCallback<ArrayList<String>>() {

			@Override
			public void onFailure(Throwable caught) {
				Window.alert("Errore caricamento degli articoli. " + caught.toString());				
			}

			@Override
			public void onSuccess(ArrayList<String> result) {
				listArticle.clear();
				listDelete.clear();
				listArticle.addItem("-");
				listDelete.addItem("-");

				for (String c: result) {
					listArticle.addItem(c);
					listDelete.addItem(c);
				}
			}

		});

		greetingService.getCategories(new AsyncCallback<ArrayList<String>>() {

			@Override
			public void onFailure(Throwable caught) {
				Window.alert("Errore caricamento delle categorie. " + caught.toString());				
			}

			@Override
			public void onSuccess(ArrayList<String> result) {
				listCat.clear();
				listNewCat.clear();
				listCat.addItem("-");
				listNewCat.addItem("-");

				for (String c: result) {
					listCat.addItem(c);
					listNewCat.addItem(c);
				}

			}

		});

		greetingService.getAllAuthors(new AsyncCallback<ArrayList<String>>() {

			@Override
			public void onFailure(Throwable caught) {
				Window.alert("Errore caricamento degli autori. " + caught.toString());				
			}

			@Override
			public void onSuccess(ArrayList<String> result) {
				listAuthor.clear();
				listAuthor.addItem("-");
				listNewAuthor.clear();
				listNewAuthor.addItem("-");

				for (String a: result) {
					listAuthor.addItem(a);
					listNewAuthor.addItem(a);
				}
			}



		});


		/*
		 * CHANGE HANDLERS
		 */

		listCat.addChangeHandler(new ChangeHandler() {

			@Override
			public void onChange(ChangeEvent event) {

				greetingService.getSubCategories(listCat.getSelectedItemText(), 
						new AsyncCallback<ArrayList<String>>() {

					@Override
					public void onFailure(Throwable caught) {
						Window.alert("Errore nel caricamento delle sottocategorie");
					}

					@Override
					public void onSuccess(ArrayList<String> result) {
						listSubCat.clear();
						listSubCat.addItem("-");
						for (String c: result) {
							listSubCat.addItem(c);
						}
					}

				});

			}

		});

		listNewCat.addChangeHandler(new ChangeHandler() {

			@Override
			public void onChange(ChangeEvent event) {

				greetingService.getSubCategories(listNewCat.getSelectedItemText(), 
						new AsyncCallback<ArrayList<String>>() {

					@Override
					public void onFailure(Throwable caught) {
						Window.alert("Errore nel caricamento delle sottocategorie");
					}

					@Override
					public void onSuccess(ArrayList<String> result) {
						listNewSubCat.clear();
						listNewSubCat.addItem("-");
						for (String c: result) {
							listNewSubCat.addItem(c);
						}
					}

				});

			}

		});

		listArticle.addChangeHandler(new ChangeHandler() {

			@Override
			public void onChange(ChangeEvent event) {

				greetingService.getArticle(listArticle.getSelectedItemText(), 
						new AsyncCallback<Article>() {

					@Override
					public void onFailure(Throwable caught) {
						Window.alert("Errore nel caricamento dell'articolo!");								
					}

					@SuppressWarnings("deprecation")
					@Override
					public void onSuccess(Article result) {
						newTitle.setText(result.getTitle());
						newText.setText(result.getText());
						checkNewPrem.setChecked(result.isPremium());
					}

				});

			}

		});

		if (vpArticleManager == null) {

			vpArticleManager = new VerticalPanel();

			setClickHandlers();

		}

		vpArticleManager.add(fpContainerA);
		vpArticleManager.getElement().addClassName("w-100");

		RootPanel.get().add(vpArticleManager);

		vpArticleManager.setVisible(true);
	}

	/*
	 * CLICK HANDLERS
	 */
	
	private static void setClickHandlers() {

		backBtn.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				vpArticleManager.setVisible(false);
				AdminDashboard.main();
			}
		});

		addArticle.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {	

				if (isAllFilled()) {

					String t = title.getValue();
					String d = text.getValue();
					Date oggi = new Date();
					String cat = listCat.getSelectedItemText();
					String sub = listSubCat.getSelectedItemText();
					boolean p = checkPrem.getValue();
					String aut = listAuthor.getSelectedItemText();

					Article art = new Article(t,d,oggi,cat,aut,p,0,0);				

					if (!sub.equals("-")) {
						art.setSubCategory(sub);
					}

					greetingService.addArticle(art, new AsyncCallback<Boolean>() {

						@Override
						public void onFailure(Throwable caught) {
							Window.alert("Errore durante l'aggiunta dell'articolo. " + caught.toString());
						}

						@Override
						public void onSuccess(Boolean result) {

							if (result) {
								Window.alert("Articolo Aggiunto!");
								title.setText("");
								text.setText("");
								checkPrem.setChecked(false);
								vpArticleManager.setVisible(false);
								ArticleManager.main();
							} else {
								Window.alert("Errore durante l'inserimento o articolo gi� inserito");
							}

						}

					});

				} else {
					Window.alert("Compila tutti i campi");
				}
			}
		});

		modifyArticle.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {

				String tochange = listArticle.getSelectedItemText();
				String t = newTitle.getValue();
				String d = newText.getValue();
				Date oggi = new Date();
				String cat = listNewCat.getSelectedItemText();
				String sub = listNewSubCat.getSelectedItemText();
				boolean p = checkNewPrem.getValue();
				String aut = listNewAuthor.getSelectedItemText();

				Article art = new Article(t,d,oggi,cat,aut,p,0,0);				

				if (!sub.equals("-")) {
					art.setSubCategory(sub);
				}

				greetingService.modifyArticle(tochange, art, new AsyncCallback<Boolean>() {

					@Override
					public void onFailure(Throwable caught) {
						Window.alert("Errore durante la modifica dell'articolo. " + caught.toString());
					}

					@Override
					public void onSuccess(Boolean result) {
						if (result) {
							Window.alert("Articolo modificato!");
							newTitle.setText("");
							newText.setText("");
							vpArticleManager.setVisible(false);
							ArticleManager.main();
						} else {
							Window.alert("Errore durante la modifica o articolo non trovato");
						}
					}

				});
			}
		});

		deleteArticle.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {

				String t = listDelete.getSelectedItemText();

				greetingService.deleteArticle(t, new AsyncCallback<Boolean>() {

					@Override
					public void onFailure(Throwable caught) {
						Window.alert("Errore durante l'eliminazione dell'articolo. " + caught.toString());
					}

					@Override
					public void onSuccess(Boolean result) {
						if (result) {
							Window.alert("Articolo eliminato!");
							vpArticleManager.setVisible(false);
							ArticleManager.main();
						} else {
							Window.alert("Errore durante l'eliminazione o articolo non trovato");
						}
					}

				});

			}
		});

	}

}
