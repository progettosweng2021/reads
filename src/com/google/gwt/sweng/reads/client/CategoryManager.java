package com.google.gwt.sweng.reads.client;

import java.util.ArrayList;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.sweng.reads.shared.Category;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Grid;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.TabPanel;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;

public class CategoryManager {

	private static final GreetingServiceAsync greetingService = GWT.create(GreetingService.class);

	public static VerticalPanel vpCategoryManager;
	public static FlowPanel fpContainerB = new FlowPanel();
	public static FlowPanel fpBackButton = new FlowPanel();
	public static TabPanel tabPanel = new TabPanel();

	// Widget aggiunta categoria
	public static FlowPanel fpNewCategory = new FlowPanel();
	private static Grid newCatGrid = new Grid(2, 2);
	private static HTMLPanel titleNewCategory = new HTMLPanel("<h4>Inserimento nuova categoria</h4>");
	private static Label labelNewCat = new Label("Compilare tutti i campi e premere il tasto i conferma.");
	private static Label labelCatName = new Label("Nome categoria:");
	private static TextBox catName = new TextBox();
	private static Button addCat = new Button("Aggiungi");

	// Widget aggiunta sottocategoria
	public static FlowPanel fpNewSubCat = new FlowPanel();
	private static Grid newSubCatGrid = new Grid(3, 2);
	private static HTMLPanel titleNewSubCat = new HTMLPanel("<h4>Inserimento nuova sottocategoria</h4>");
	private static Label labelNewSubCat = new Label("Compilare tutti i campi e premere il tasto di conferma.");
	private static Label labelCat = new Label("Categoria madre:");
	private static ListBox listCat = new ListBox();
	private static Label labelSubCatName = new Label("Nome sottocategoria:");
	private static TextBox subCatName = new TextBox();
	private static Button addSubCat = new Button("Aggiungi");

	// Widget eliminazione categoria
	public static FlowPanel fpDelCat = new FlowPanel();
	private static Grid delCatGrid = new Grid(2, 2);
	private static HTMLPanel titleDelCat = new HTMLPanel("<h4>Eliminazione categoria</h4>");
	private static Label labelDelCat = new Label("Selezionare la categoria da elliminare");
	private static Label labelSelectCat = new Label("Categoria:");
	private static ListBox selectCat = new ListBox();
	private static Button deleteCat = new Button("Elimina");

	// Widget eliminazione categoria
	public static FlowPanel fpDelSubCat = new FlowPanel();
	private static Grid delSubCatGrid = new Grid(3, 2);
	private static HTMLPanel titleDelSubCat = new HTMLPanel("<h4>Eliminazione categoria</h4>");
	private static Label labelDelSubCat = new Label("Selezionare una categoria e successivamente la sottocategoria da elliminare");
	private static Label labelSelectCat2 = new Label("Categoria:");
	private static ListBox selectCat2 = new ListBox();
	private static Label labelSelectSubCat = new Label("Sottocategoria:");
	private static ListBox selectSubCat = new ListBox();
	private static Button deleteSubCat = new Button("Elimina");

	private static Button backBtn = new Button("Torna indietro");
	private static String layoutBtn = "btn btn-primary btn-sm";

	public static void main() {
		
		backBtn.setStyleName("btn btn-outline-secondary btn-sm float-right");
		
		addCat.setStyleName(layoutBtn);
		addSubCat.setStyleName(layoutBtn);
		deleteCat.setStyleName(layoutBtn);
		deleteSubCat.setStyleName(layoutBtn);

		fpNewCategory.getElement().addClassName("col-12");
		fpNewCategory.getElement().setAttribute("align", "center");
		fpNewCategory.add(titleNewCategory);
		fpNewCategory.add(labelNewCat);
		newCatGrid.setWidget(0, 0, labelCatName);
		newCatGrid.setWidget(0, 1, catName);
		newCatGrid.setWidget(1, 1, addCat);
		fpNewCategory.add(newCatGrid);

		fpNewSubCat.getElement().addClassName("col-12");
		fpNewSubCat.getElement().setAttribute("align","center");
		fpNewSubCat.add(titleNewSubCat);
		fpNewSubCat.add(labelNewSubCat);
		newSubCatGrid.setWidget(0, 0, labelCat);
		newSubCatGrid.setWidget(0, 1, listCat);
		newSubCatGrid.setWidget(1, 0, labelSubCatName);
		newSubCatGrid.setWidget(1, 1, subCatName);
		newSubCatGrid.setWidget(2, 1, addSubCat);
		fpNewSubCat.add(newSubCatGrid);

		fpDelCat.getElement().addClassName("col-12");
		fpDelCat.getElement().setAttribute("align", "center");
		fpDelCat.add(titleDelCat);
		fpDelCat.add(labelDelCat);
		delCatGrid.setWidget(0, 0, labelSelectCat);
		delCatGrid.setWidget(0, 1, selectCat);
		delCatGrid.setWidget(1, 1, deleteCat);
		fpDelCat.add(delCatGrid);

		fpDelSubCat.getElement().addClassName("col-12");
		fpDelSubCat.getElement().setAttribute("align", "center");
		fpDelSubCat.add(titleDelSubCat);
		fpDelSubCat.add(labelDelSubCat);
		delSubCatGrid.setWidget(0, 0, labelSelectCat2);
		delSubCatGrid.setWidget(0, 1, selectCat2);
		delSubCatGrid.setWidget(1, 0, labelSelectSubCat);
		delSubCatGrid.setWidget(1, 1, selectSubCat);
		delSubCatGrid.setWidget(2, 1, deleteSubCat);
		fpDelSubCat.add(delSubCatGrid);

		tabPanel.getElement().addClassName("col-10 mx-auto container");
		tabPanel.getElement().setAttribute("align", "center");
		tabPanel.add(fpNewCategory,"Nuova categoria");
		tabPanel.add(fpNewSubCat,"Nuova sottocategoria");
		tabPanel.add(fpDelCat,"Elimina categoria");
		tabPanel.add(fpDelSubCat,"Elimina sottocategoria");
		tabPanel.selectTab(0);

		fpBackButton.add(backBtn);
		fpBackButton.setStyleName("mb-4");

		fpContainerB.add(fpBackButton);
		fpContainerB.add(tabPanel);
		fpContainerB.getElement().addClassName("col-8 mx-auto container border border-primary rounded-corners");

		/*
		 * Popolamento listbox
		 */

		greetingService.getCategories(new AsyncCallback<ArrayList<String>>() {

			@Override
			public void onFailure(Throwable caught) {
				Window.alert("Errore caricamento delle categorie. " + caught.toString());				
			}

			@Override
			public void onSuccess(ArrayList<String> result) {
				listCat.clear();
				selectCat.clear();
				selectCat2.clear();
				selectSubCat.clear();
				listCat.addItem("-");
				selectCat.addItem("-");
				selectCat2.addItem("-");

				for (String c: result) {
					listCat.addItem(c);
					selectCat.addItem(c);
					selectCat2.addItem(c);
				}

			}

		});

		/*
		 * CHANGE HANDLERS
		 */

		selectCat2.addChangeHandler(new ChangeHandler() {

			@Override
			public void onChange(ChangeEvent event) {

				greetingService.getSubCategories(selectCat2.getSelectedItemText(), 
						new AsyncCallback<ArrayList<String>>() {

					@Override
					public void onFailure(Throwable caught) {
						Window.alert("Errore nel caricamento delle sottocategorie");
					}

					@Override
					public void onSuccess(ArrayList<String> result) {
						selectSubCat.clear();
						selectSubCat.addItem("-");
						for (String c: result) {
							selectSubCat.addItem(c);
						}
					}

				});

			}

		});

		if (vpCategoryManager == null) {

			vpCategoryManager = new VerticalPanel();

			setClickHandlers();
			
		}

		vpCategoryManager.add(fpContainerB);
		vpCategoryManager.getElement().addClassName("w-100");

		RootPanel.get().add(vpCategoryManager);

		vpCategoryManager.setVisible(true);

	}

	/*
	 * CLICK HANDLERS
	 */
	
	private static void setClickHandlers() {

		backBtn.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				vpCategoryManager.setVisible(false);
				AdminDashboard.main();
			}
		});

		addCat.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {

				if (catName.getValue().equals("")) {
					Window.alert("Inserire un nome per la nuova categoria");
				} else {
					Category cat = new Category(catName.getValue());

					greetingService.addCategory(cat, new AsyncCallback<Boolean>() {

						@Override
						public void onFailure(Throwable caught) {
							Window.alert("Errore durante l'inserimento della categoria. " + caught.toString());
						};

						@Override
						public void onSuccess(Boolean result) {

							if (result) {
								Window.alert("Categoria aggiunta!");
								catName.setText("");
								vpCategoryManager.setVisible(false);
								CategoryManager.main();
							} else {
								Window.alert("Errore durante l'inserimento della categoria. ");
							}

						}

					});
				}

			}
		});

		addSubCat.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {

				if (listCat.getSelectedItemText().equals("-") || subCatName.getValue().equals("")) {
					Window.alert("Riempire tutti i campi");
				} else {

					greetingService.addSubCategory(listCat.getSelectedItemText(), subCatName.getValue(), new AsyncCallback<Boolean>() {

						@Override
						public void onFailure(Throwable caught) {
							Window.alert("Errore durante l'inserimento della sottocategoria. " + caught.toString());
						}

						@Override
						public void onSuccess(Boolean result) {

							if (result) {
								Window.alert("Sottocategoria aggiunta!");
								subCatName.setText("");
								vpCategoryManager.setVisible(false);
								CategoryManager.main();
							} else {
								Window.alert("Errore durante l'inserimento della sottocategoria.");
							}
						}

					});

				}

			}
		});

		deleteCat.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {

				if (selectCat.getSelectedItemText().equals("-")) {
					Window.alert("Riempire tutti i campi");
				} else {

					greetingService.deleteCategory(selectCat.getSelectedItemText(), new AsyncCallback<Boolean>() {

						@Override
						public void onFailure(Throwable caught) {
							Window.alert("Errore durante l'eliminazione della categoria. " + caught.toString());
						};

						@Override
						public void onSuccess(Boolean result) {

							if (result) {
								Window.alert("Categoria eliminata correttamente!");
								vpCategoryManager.setVisible(false);
								CategoryManager.main();
							} else {
								Window.alert("Errore durante l'eliminazione della categoria.");
							}

						}

					});
				}

			}
		});

		deleteSubCat.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {

				if (selectCat2.getSelectedItemText().equals("-") || selectSubCat.getSelectedItemText().equals("")) {
					Window.alert("Riempire tutti i campi");
				} else {

					greetingService.deleteSubCategory(selectCat2.getSelectedItemText(), selectSubCat.getSelectedItemText() ,new AsyncCallback<Boolean>() {

						@Override
						public void onFailure(Throwable caught) {
							Window.alert("Errore durante l'eliminazione della sottocategoria. " + caught.toString());
						};

						@Override
						public void onSuccess(Boolean result) {

							if (result) {
								Window.alert("Sottocategoria eliminata correttamente!");
								vpCategoryManager.setVisible(false);
								CategoryManager.main();
							} else {
								Window.alert("Errore durante l'eliminazione della sottocategoria.");
							}

						}

					});
				}

			}
		});

	}

}
