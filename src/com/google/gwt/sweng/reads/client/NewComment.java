package com.google.gwt.sweng.reads.client;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.sweng.reads.shared.Comment;
import com.google.gwt.sweng.reads.shared.User;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.*;

import java.util.Date;

public class NewComment {

	//Create a remote service proxy to talk to the server-side Greeting service.   
	private static final GreetingServiceAsync greetingService = GWT.create(GreetingService.class);
	private static final SessionServiceAsync sessionService = GWT.create(SessionService.class); 

	//OGGETTI
	private static HTMLPanel titleNewComm = new HTMLPanel("<h4>NUOVO COMMENTO</h4>");
	private static TextArea textComm = new TextArea();
	private static Button confirmComment = new Button("Commenta");
	private static Button goBack = new Button("Torna indietro");
	static String titleArt;
	static String userName;

	//LAYOUT
	private static VerticalPanel vpNewComment;	
	private static FlowPanel fpButtons = new FlowPanel();

	public static FlowPanel fpContainer = new FlowPanel();


	public static void main(String title) {
		
		confirmComment.setStyleName("btn btn-primary btn-sm");
		goBack.setStyleName("ml-2 btn btn-outline-secondary btn-sm");
		textComm.setPixelSize(200, 100);

		//Recupero l'username dell'utente loggato					
		sessionService.getSession(new AsyncCallback<User>() {

			@Override
			public void onFailure(Throwable caught) {

				Window.alert("Errore nel caricamento della sessione. " + caught.toString());

			}

			@Override
			public void onSuccess(User result) {
				if (result != null) {

					userName = result.getUsername();

				} else {

					Window.alert("Errore nel caricamento della sessione: non risulta che ci sia un utente loggato.");

				}
			}

		});

		titleArt = title;
		fpButtons.add(confirmComment);
		fpButtons.add(goBack);

		fpContainer.getElement().addClassName("col-8 mx-auto container border rounded-corners");

		//Per evitare il problema della duplicazione
		if (vpNewComment == null) {

			vpNewComment = new VerticalPanel();

			setClickHandlers();
		}


		titleNewComm.getElement().addClassName("title"); 

		fpContainer.add(titleNewComm);
		fpContainer.add(textComm);
		fpContainer.add(fpButtons);
		
		fpContainer.getElement().setAttribute("align", "center");

		vpNewComment.add(fpContainer);
		vpNewComment.getElement().addClassName("w-100");
		RootPanel.get().add(vpNewComment);
		vpNewComment.setVisible(true);

	}

	//Gestione dei ClickHandlers
	private static void setClickHandlers() {
		//Go back
		goBack.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent clickEvent) {
				fpContainer.clear();		//pulisco il panel cos� che, entrando poi con un nuovo articolo, questo non si sovrapponga

				vpNewComment.setVisible(false);

				OpenArticle.main(titleArt);
			}
		});

		//Conferma del nuovo commento
		confirmComment.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent clickEvent) {

				if(textComm.getText() != null && textComm.getText() != "") {

					//data dell'inserimento del commento
					Date data = new Date();

					//creo il nuovo commento
					Comment newComm = new Comment(userName, titleArt, data, textComm.getText(), false);


					greetingService.newComment(newComm, new AsyncCallback<Boolean>() {

						@Override
						public void onFailure(Throwable caught) {
							Window.alert("Errore caricamento commento: "+ caught.toString());
						}

						@Override
						public void onSuccess(Boolean result) {
							if (result != true ) {

								Window.alert("Errore nel caricamento del commento!\n" + result);

							} else {

								Window.alert("Commento inserito correttamente e in attesa di approvazione da parte dell'admin!");

								fpContainer.clear();		//pulisco il panel cos� che, entrando poi con un nuovo articolo, questo non si sovrapponga

								vpNewComment.setVisible(false);

								OpenArticle.main(titleArt);
							}							
						}

					});
				} else {

					Window.alert("Commento vuoto!\nInserire un commento valido");

				}
			}
		});
	}
}
