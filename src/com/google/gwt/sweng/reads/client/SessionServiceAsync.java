package com.google.gwt.sweng.reads.client;

import com.google.gwt.sweng.reads.shared.Author;
import com.google.gwt.sweng.reads.shared.User;

import com.google.gwt.user.client.rpc.AsyncCallback;

public interface SessionServiceAsync {
	
	void setSession(User user, AsyncCallback<Void> callback);

    void exitSession(AsyncCallback<Void> callback);
    
    void getSession(AsyncCallback<User> callback);

	void setSession(Author author, AsyncCallback<Void> callback);

	void getSessionAut(AsyncCallback<Author> callback);
    
}
