package com.google.gwt.sweng.reads.client;

import java.util.ArrayList;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.sweng.reads.shared.Article;
import com.google.gwt.sweng.reads.shared.Comment;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Grid;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.TabPanel;
import com.google.gwt.user.client.ui.VerticalPanel;

public class CommentManager {

	private static final GreetingServiceAsync greetingService = GWT.create(GreetingService.class);

	public static VerticalPanel vpCommentManager;
	public static FlowPanel fpContainerC = new FlowPanel();
	public static FlowPanel fpBackButton = new FlowPanel();
	public static TabPanel tabPanel = new TabPanel();

	// Widget moderazione commento
	public static FlowPanel fpModComment = new FlowPanel();
	private static Grid modCommentGrid = new Grid(3,2);
	private static HTMLPanel titleModComment = new HTMLPanel("<h4>Moderazione commento</h4>");
	private static Label labelModComment = new Label("Scegliere il commento da moderare (selezionando l'articolo verrano filtrati solo i commenti relativi ad esso)");
	private static Label labelSelectArticle = new Label("Articolo:");
	private static ListBox listArticles = new ListBox();
	private static Label labelSelectComment = new Label("Commento:");
	private static ListBox listComments = new ListBox();
	private static Button approveComment = new Button("Approva");
	private static Button banComment = new Button("Scarta");

	// Widget eliminazione commento
	public static FlowPanel fpDelComment = new FlowPanel();
	private static Grid delCommentGrid = new Grid(3,2);
	private static HTMLPanel titleDelComment = new HTMLPanel("<h4>Eliminazione commento</h4>");
	private static Label labelDelComment = new Label("Scegliere il commento moderato in precedenza da eliminare (selezionando l'articolo verrano filtrati solo i commenti relativi ad esso)");
	private static Label labelSelectArticleDel = new Label("Articolo:");
	private static ListBox listArticlesDel = new ListBox();
	private static Label labelSelectCommentDel = new Label("Commento:");
	private static ListBox listCommentsDel = new ListBox();
	private static Button deleteComment = new Button("Elimina");

	private static Button backBtn = new Button("Torna indietro");
	private static String layoutBtn = "btn btn-primary btn-sm";

	public static void main() {

		backBtn.setStyleName("btn btn-outline-secondary btn-sm float-right");

		approveComment.setStyleName("btn btn-success btn-sm");
		deleteComment.setStyleName(layoutBtn);
		banComment.setStyleName("btn btn-danger btn-sm");
		
		fpModComment.getElement().addClassName("col-12");
		fpModComment.getElement().setAttribute("align", "center");
		fpModComment.add(titleModComment);
		fpModComment.add(labelModComment);
		modCommentGrid.setWidget(0, 0, labelSelectArticle);
		modCommentGrid.setWidget(0, 1, listArticles);
		modCommentGrid.setWidget(1, 0, labelSelectComment);
		modCommentGrid.setWidget(1, 1, listComments);
		modCommentGrid.setWidget(2, 0, approveComment);
		modCommentGrid.setWidget(2, 1, banComment);
		fpModComment.add(modCommentGrid);

		fpDelComment.getElement().addClassName("col-12");
		fpDelComment.getElement().setAttribute("align", "center");
		fpDelComment.add(titleDelComment);
		fpDelComment.add(labelDelComment);
		delCommentGrid.setWidget(0, 0, labelSelectArticleDel);
		delCommentGrid.setWidget(0, 1, listArticlesDel);
		delCommentGrid.setWidget(1, 0, labelSelectCommentDel);
		delCommentGrid.setWidget(1, 1, listCommentsDel);
		delCommentGrid.setWidget(2, 1, deleteComment);
		fpDelComment.add(delCommentGrid);

		tabPanel.getElement().addClassName("col-10 mx-auto container");
		tabPanel.getElement().setAttribute("align", "center");
		tabPanel.add(fpModComment,"Modera commento");
		tabPanel.add(fpDelComment,"Elimina commento");
		tabPanel.selectTab(0);

		fpBackButton.add(backBtn);
		fpBackButton.setStyleName("mb-4");

		fpContainerC.add(fpBackButton);
		fpContainerC.add(tabPanel);
		fpContainerC.getElement().addClassName("col-8 mx-auto container border border-primary rounded-corners");
		//fpContainerC.getElement().setAttribute("align", "center");

		/*
		 * Popolamento listbox
		 */

		greetingService.getModeratedComments(new AsyncCallback<ArrayList<Comment>>() {

			@Override
			public void onFailure(Throwable caught) {
				Window.alert("Errore caricamento dei commenti. " + caught.toString());
			}

			@Override
			public void onSuccess(ArrayList<Comment> result) {

				listCommentsDel.clear();
				listCommentsDel.addItem("-");

				for (Comment c: result) {
					listCommentsDel.addItem(c.getTextComment());
				}
			}
		});

		greetingService.getUnmoderatedComments(new AsyncCallback<ArrayList<Comment>>() {

			@Override
			public void onFailure(Throwable caught) {
				Window.alert("Errore caricamento dei commenti. " + caught.toString());
			}

			@Override
			public void onSuccess(ArrayList<Comment> result) {

				listComments.clear();
				listComments.addItem("-");

				for (Comment c: result) {
					listComments.addItem(c.getTextComment());
				}
			} 
		});

		greetingService.getAllTitles(new AsyncCallback<ArrayList<String>>() {

			@Override
			public void onFailure(Throwable caught) {
				Window.alert("Errore caricamento degli articoli. " + caught.toString());				
			}

			@Override
			public void onSuccess(ArrayList<String> result) {

				listArticles.clear();
				listArticlesDel.clear();
				listArticles.addItem("-");
				listArticlesDel.addItem("-");

				for (String a: result) {
					listArticles.addItem(a);
					listArticlesDel.addItem(a);
				}
			}

		});


		/*
		 * CHANGE HANDLERS
		 */

		listArticles.addChangeHandler(new ChangeHandler() {

			@Override
			public void onChange(ChangeEvent event) {

				greetingService.getUnmoderatedComments(listArticles.getSelectedItemText(),
						new AsyncCallback<ArrayList<Comment>>() {

					@Override
					public void onFailure(Throwable caught) {
						Window.alert("Errore caricamento dei commenti. " + caught.toString()); 
					}

					@Override
					public void onSuccess(ArrayList<Comment> result) {
						listComments.clear();
						listComments.addItem("-");

						for (Comment c: result) {
							listComments.addItem(c.getTextComment());
						}

					}

				});

			}

		});

		listArticlesDel.addChangeHandler(new ChangeHandler() {

			@Override
			public void onChange(ChangeEvent event) {

				greetingService.getModeratedComments(listArticlesDel.getSelectedItemText(),
						new AsyncCallback<ArrayList<Comment>>() {

					@Override
					public void onFailure(Throwable caught) {
						Window.alert("Errore caricamento dei commenti. " + caught.toString()); 
					}

					@Override
					public void onSuccess(ArrayList<Comment> result) {
						listCommentsDel.clear();
						listCommentsDel.addItem("-");

						for (Comment c: result) {
							listCommentsDel.addItem(c.getTextComment());
						}

					}

				});

			}

		});


		if (vpCommentManager == null) {

			vpCommentManager = new VerticalPanel();

			setClickHandlers();

		}

		vpCommentManager.add(fpContainerC);
		vpCommentManager.getElement().addClassName("w-100");

		RootPanel.get().add(vpCommentManager);

		vpCommentManager.setVisible(true);

	}

	/*
	 * CLICK HANDLERS
	 */

	private static void setClickHandlers() {

		backBtn.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				vpCommentManager.setVisible(false);
				AdminDashboard.main();				
			}
		});		


		approveComment.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				if (listComments.getSelectedItemText().equals("-")) {
					Window.alert("Riempire i campi necessari");
				} else {

					greetingService.approveComment(listComments.getSelectedItemText(), 
							new AsyncCallback<Boolean>() {

						@Override
						public void onFailure(Throwable caught) {
							Window.alert("Errore durante l'approvazione del commento. " + caught.toString());
						}

						@Override
						public void onSuccess(Boolean result) {
							if (result) {
								Window.alert("Commento approvato!");
								vpCommentManager.setVisible(false);
								CommentManager.main();
							} else {
								Window.alert("Errore durante l'approvazione del commento.");
							}

						}

					});

				}
			}
		});


		banComment.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				if (listComments.getSelectedItemText().equals("-")) {
					Window.alert("Riempire i campi necessari");
				} else {

					greetingService.deleteComment(listComments.getSelectedItemText(), 
							new AsyncCallback<Boolean>() {

						@Override
						public void onFailure(Throwable caught) {
							Window.alert("Errore durante il ban del commento. " + caught.toString());
						}

						@Override
						public void onSuccess(Boolean result) {
							if (result) {
								Window.alert("Commento bannato!");
								vpCommentManager.setVisible(false);
								CommentManager.main();
							} else {
								Window.alert("Errore durante il ban del commento.");
							}

						}

					});
				}
			}
		});


		deleteComment.addClickHandler(new ClickHandler() {	
			@Override
			public void onClick(ClickEvent event) {
				if (listCommentsDel.getSelectedItemText().equals("-")) {
					Window.alert("Riempire i campi necessari");
				} else {

					greetingService.deleteComment(listCommentsDel.getSelectedItemText(), 
							new AsyncCallback<Boolean>() {

						@Override
						public void onFailure(Throwable caught) {
							Window.alert("Errore durante l'eliminazione del commento. " + caught.toString());
						}

						@Override
						public void onSuccess(Boolean result) {
							if (result) {
								Window.alert("Commento eliminato!");
								vpCommentManager.setVisible(false);
								CommentManager.main();
							} else {
								Window.alert("Errore durante l'eliminazione del commento.");
							}

						}

					});

				}
			}
		});

	}

}
