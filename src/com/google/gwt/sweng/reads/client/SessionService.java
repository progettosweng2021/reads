package com.google.gwt.sweng.reads.client;

import com.google.gwt.sweng.reads.shared.Author;
import com.google.gwt.sweng.reads.shared.User;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

@RemoteServiceRelativePath("session")
public interface SessionService extends RemoteService {

	public void setSession(User user);

    public void exitSession();
    
    public User getSession();

	void setSession(Author author);

	Author getSessionAut();
	
}
