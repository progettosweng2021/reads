package com.google.gwt.sweng.reads.client;

import java.util.ArrayList;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.sweng.reads.shared.Author;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Grid;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.TabPanel;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;


public class ReadInfoAuthor {
	
	//pagina dedicata alle informazioni dell'autore loggato
	
	//Create a remote service proxy to talk to the server-side Greeting service.   
	private static final GreetingServiceAsync greetingService = GWT.create(GreetingService.class);
	private static final SessionServiceAsync sessionService = GWT.create(SessionService.class); 

	//Widget info
	public static VerticalPanel vpReadInfoAuthor;
	public static FlowPanel fpNewDatiPersonali = new FlowPanel();
	private static Grid gridDatiPersonali = new Grid(3, 4);
	private static HTMLPanel titleDatiPersonali = new HTMLPanel("<h4>Dati personali</h4>");

	private static Label nameLabelDatiPersonali = new Label("Nome:");
	private static TextBox nameTextBoxDatiPersonali = new TextBox();
	private static Label surnameLabelDatiPersonali = new Label("Cognome:");
	private static TextBox surnameTextBoxDatiPersonali = new TextBox();
	private static Label usernameLabelDatiPersonali = new Label("Username:");
	private static TextBox dateTextBoxDatiPersonali = new TextBox();
	private static Label emailLabelDatiPersonali = new Label("Email:");
	private static TextBox emailTextBoxDatiPersonali = new TextBox();
	private static Label pwLabelDatiPersonali = new Label("Password:");
	private static TextBox pwTextBoxDatiPersonali = new TextBox();

	//widget lista articoli

	public static FlowPanel fpListaArticoli = new FlowPanel();
	private static Grid gridArticoli = new Grid(10, 1);
	private static HTMLPanel titoloArticoli = new HTMLPanel("<h4>Articoli pubblicati</h4>");


	private static Button logout = new Button("Logout");
	private static Button dashboardBtn = new Button("Gestione Articoli");
	static Author autoreLog;

	
	public static FlowPanel fpContainerA = new FlowPanel();
	public static FlowPanel fbButtons = new FlowPanel();
	public static TabPanel tabPanel = new TabPanel();
	final static String styleLabel = "mb-4";
	final static String styleTB = "mr-4 mb-4";


	public static void main() {

		//css
		nameLabelDatiPersonali.setStyleName(styleLabel);
		emailLabelDatiPersonali.setStyleName(styleLabel);
		usernameLabelDatiPersonali.setStyleName(styleLabel);
		surnameLabelDatiPersonali.setStyleName(styleLabel);
		pwLabelDatiPersonali.setStyleName(styleLabel);
		nameTextBoxDatiPersonali.setStyleName(styleTB);
		dateTextBoxDatiPersonali.setStyleName(styleLabel);
		emailTextBoxDatiPersonali.setStyleName(styleTB);
		surnameTextBoxDatiPersonali.setStyleName(styleLabel);
		pwTextBoxDatiPersonali.setStyleName(styleLabel);

		//dati personali
		fpNewDatiPersonali.getElement().addClassName("col-12");
		fpNewDatiPersonali.getElement().setAttribute("align", "center");
		fpNewDatiPersonali.add(titleDatiPersonali);

		gridDatiPersonali.setWidget(0, 0, nameLabelDatiPersonali);
		gridDatiPersonali.setWidget(0, 1, nameTextBoxDatiPersonali);
		gridDatiPersonali.setWidget(0, 2, surnameLabelDatiPersonali);
		gridDatiPersonali.setWidget(0, 3, surnameTextBoxDatiPersonali);
		gridDatiPersonali.setWidget(2, 0, emailLabelDatiPersonali);
		gridDatiPersonali.setWidget(2, 1, emailTextBoxDatiPersonali);
		gridDatiPersonali.setWidget(2, 2, pwLabelDatiPersonali);
		gridDatiPersonali.setWidget(2, 3, pwTextBoxDatiPersonali);

		fpNewDatiPersonali.add(gridDatiPersonali);


		//lista articoli
		fpListaArticoli.getElement().addClassName("col-12");
		fpListaArticoli.getElement().setAttribute("align", "center");
		fpListaArticoli.add(titoloArticoli);
		fpListaArticoli.add(gridArticoli);

		//tabPanel
		tabPanel.getElement().addClassName("col-10 mx-auto container rounded-corners");
		tabPanel.add(fpNewDatiPersonali,"Dati Personali");
		tabPanel.add(fpListaArticoli, "Articoli pubblicati");
		tabPanel.selectTab(0);


		//goBack
		dashboardBtn.setStyleName("btn btn-primary btn-small mt-4 mr-4");
		logout.setStyleName("btn btn-outline-secondary btn-small mt-4");
		fbButtons.add(dashboardBtn);
		fbButtons.add(logout);
		fbButtons.getElement().setAttribute("align", "center");


		//fpContainerA
		fpContainerA.add(tabPanel);
		fpContainerA.add(fbButtons);
		fpContainerA.getElement().addClassName("col-8 mx-auto container border border-primary rounded-corners");



		sessionService.getSessionAut(new AsyncCallback<Author>() {

			@Override
			public void onSuccess(Author result) {

				if (result == null) {

					//dati personali
					nameTextBoxDatiPersonali.setText("");
					surnameTextBoxDatiPersonali.setText("");
					dateTextBoxDatiPersonali.setText("");
					emailTextBoxDatiPersonali.setText("");
					pwTextBoxDatiPersonali.setText("");
				} else {

					autoreLog = result;
					gridArticoli.clear();

					//riempio le TextBox dei dati personali
					nameTextBoxDatiPersonali.setText(result.getName());
					nameTextBoxDatiPersonali.setEnabled(false);
					surnameTextBoxDatiPersonali.setText(result.getSurname());
					surnameTextBoxDatiPersonali.setEnabled(false);
					emailTextBoxDatiPersonali.setText(result.getEmail());
					emailTextBoxDatiPersonali.setEnabled(false);
					pwTextBoxDatiPersonali.setText(result.getPassword());
					pwTextBoxDatiPersonali.setEnabled(false);

					greetingService.getListArticleAuthor(autoreLog.getUsername(), new AsyncCallback<ArrayList<String>>() {

						@Override
						public void onSuccess(ArrayList<String> result) {

							if (result.size()==0) {
								Label art = new Label();
								art.setStyleName("container border border-danger m-1");
								art.setText("Non hai pubblicato nessun articolo");
								gridArticoli.setWidget(0, 0, art);
							}else {
								int i = 0;
								for (String str : result) {
									Label art = new Label();
									art.setStyleName("container border border-success m-1");
									art.setText(str);
									gridArticoli.setWidget(i, 0, art);
									i++;
								}
							}
							fpListaArticoli.setVisible(true);


						}

						@Override
						public void onFailure(Throwable caught) {
							Window.alert("Errore nel caricamento delle informazioni");
							vpReadInfoAuthor.setVisible(false);
							Home.main();
						}
					});
				}
			}

			@Override
			public void onFailure(Throwable caught) {
				Window.alert("Errore nel caricamento della sessione");
				vpReadInfoAuthor.setVisible(false);
				Home.main();
			}
		});
		
		
		if (vpReadInfoAuthor == null) {
			vpReadInfoAuthor = new VerticalPanel();
			setClickHandlers();
		}
		
		vpReadInfoAuthor.add(fpContainerA);
		vpReadInfoAuthor.getElement().addClassName("w-100");
		
		RootPanel.get().add(vpReadInfoAuthor);
		vpReadInfoAuthor.setVisible(true);
		
	}

	private static void setClickHandlers() {
		//azione logout
		logout.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent clickEvent) {
				sessionService.exitSession(new AsyncCallback<Void>() {

					@Override
					public void onSuccess(Void result) {
						vpReadInfoAuthor.setVisible(false);
						Home.main();
						Window.alert("Sessione chiusa.");
					}

					@Override
					public void onFailure(Throwable caught) {
						Window.alert("Errore chiusura sessione. " + caught.toString());
						vpReadInfoAuthor.setVisible(false);
						ReadInfoAuthor.main();
					}
				});
			}
		});
		
		
		//azione dashboard autore
		dashboardBtn.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				fpContainerA.clear();
				vpReadInfoAuthor.setVisible(false);
				AuthorDashboard.main();				
			}
		});
	}

}