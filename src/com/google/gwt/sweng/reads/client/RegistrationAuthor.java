package com.google.gwt.sweng.reads.client;

import java.util.Date;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.sweng.reads.shared.Author;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Grid;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.PasswordTextBox;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;


public class RegistrationAuthor {
	
	//pagina per la registrazione di un nuovo autore

	//Create a remote service proxy to talk to the server-side Greeting service.   
	private static final GreetingServiceAsync greetingService = GWT.create(GreetingService.class);

	//OGGETTI REGISTRAZIONE
	final static String styleButton = "btn btn-primary btn-sm mt-2";
	private static Label entryLabel = new Label("Registrazione Autore");
	private static Label nameLabel = new Label("Nome:");
	private static TextBox nameTextBox = new TextBox();
	private static Label surnameLabel = new Label("Cognome:");
	private static TextBox surnameTextBox = new TextBox();
	private static Label usernameLabel = new Label("User:");
	private static TextBox usernameTextBox = new TextBox();
	private static Label emailLabel = new Label("Email:");
	private static TextBox emailTextBox = new TextBox();
	private static Label pwLabel = new Label("Password:");
	private static PasswordTextBox pwPasswordTextBox = new PasswordTextBox();
	private static Button registrazioneBtt = new Button("Registrati");
	private static Button goBack = new Button("Torna indietro");
	public static Label esito = new Label();

	//LAYOUT REGISTRAZIONE
	private static VerticalPanel vpRegistrazione;
	private static Grid grid = new Grid(6,2);
	
	//container
	public static FlowPanel fpContainer = new FlowPanel();

	public static void main() {
		
		nameTextBox.setText("");
		emailTextBox.setText("");
		surnameTextBox.setText("");
		usernameTextBox.setText("");
		pwPasswordTextBox.setText("");
		
		//css
		entryLabel.setStyleName("h4 mb-3");
		registrazioneBtt.setStyleName(styleButton);
		goBack.setStyleName("btn btn-outline-secondary btn-sm mt-2");
		
		//griglia registrazione
		grid.setWidget(0, 0, nameLabel);
		grid.setWidget(0, 1, nameTextBox);
		grid.setWidget(1, 0, surnameLabel);
		grid.setWidget(1, 1, surnameTextBox);
		grid.setWidget(2, 0, usernameLabel);
		grid.setWidget(2, 1, usernameTextBox);
		grid.setWidget(3, 0, emailLabel);
		grid.setWidget(3, 1, emailTextBox);
		grid.setWidget(4, 0, pwLabel);
		grid.setWidget(4, 1, pwPasswordTextBox);
		grid.setWidget(5, 0, registrazioneBtt);
		grid.setWidget(5, 1, goBack);
		
		//layout
		fpContainer.add(entryLabel);
		fpContainer.add(grid);
		

		fpContainer.getElement().addClassName("col-8 mx-auto container border rounded-corners");
		fpContainer.getElement().setAttribute("align", "center");


		if (vpRegistrazione == null) {

			vpRegistrazione = new VerticalPanel();

			setClickHandlers();

		}

		vpRegistrazione.add(fpContainer);
		vpRegistrazione.getElement().addClassName("w-100");

		RootPanel.get().add(vpRegistrazione);

		vpRegistrazione.setVisible(true);

	}

	private static void setClickHandlers() {
		//controllo registarzione
		registrazioneBtt.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent clickEvent) {

				String name = nameTextBox.getText();
				String surname = surnameTextBox.getText();
				String username = usernameTextBox.getText();
				String email = emailTextBox.getText();
				String pw = pwPasswordTextBox.getText();
				Date dateReg = new Date();


				greetingService.authorRegistration(username, email, pw, name, surname, dateReg,  new AsyncCallback<Author>() {

					@Override
					public void onFailure(Throwable caught) {
						Window.alert("Errore Registrazione: "+ caught.toString());
						vpRegistrazione.setVisible(false);
						RegistrationAuthor.main();
					}

					@Override
					public void onSuccess(Author result) {
						if (result != null) {
							Window.alert("Registrazione effettuata con successo!");
							vpRegistrazione.setVisible(false);
							ReadInfoAuthor.main();
							
						} else {
							Window.alert("Errore nell'inserimento dei dati");
							vpRegistrazione.setVisible(false);
							Home.main();
						}
					}
				});

			}
		});

		//azione goBackRegistrazione
		goBack.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent clickEvent) {
				vpRegistrazione.setVisible(false);
				Home.main();		      }
		});

	}
}
