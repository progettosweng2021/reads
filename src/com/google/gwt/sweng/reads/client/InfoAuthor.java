package com.google.gwt.sweng.reads.client;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.sweng.reads.shared.Author;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;


public class InfoAuthor {
	
	//pagina che visualizza le informazioni relative all'autore dell'articolo

	//Create a remote service proxy to talk to the server-side Greeting service.   
	private static final GreetingServiceAsync greetingService = GWT.create(GreetingService.class);
	
	final static String styleButton = "mt-4 btn btn-primary btn-sm";
	
	//OGGETTI AUTORE
	private static HTMLPanel entryLabel = new HTMLPanel("<h4>Dati relativi all'autore dell'articolo:</h4>");
	private static Label nameLabel = new Label("Nome:");
	private static TextBox nameTextBox = new TextBox();
	private static Label surnameLabel = new Label("Cognome:");
	private static TextBox surnameTextBox = new TextBox();
	private static Label usernameLabel = new Label("Username:");
	private static TextBox usernameTextBox = new TextBox();
	private static Label emailLabel = new Label("Email:");
	private static TextBox emailTextBox = new TextBox();
	private static Button goBack = new Button("Home");

	//LAYOUT AUTORE
	private static VerticalPanel vpInfo;
	private static FlowPanel fpName = new FlowPanel();
	private static FlowPanel fpSurname = new FlowPanel();
	private static FlowPanel fpUserName = new FlowPanel();
	private static FlowPanel fpEmail = new FlowPanel();
	private static FlowPanel fpFirstLine = new FlowPanel();

	public static FlowPanel fpContainer = new FlowPanel();


	public static void main(String title) {
		
		goBack.setStyleName(styleButton);
		
		fpName.add(nameLabel);
		fpName.add(nameTextBox);
		
		fpSurname.add(surnameLabel);
		fpSurname.add(surnameTextBox);
		
		fpUserName.add(usernameLabel);
		fpUserName.add(usernameTextBox);
		
		fpEmail.add(emailLabel);
		fpEmail.add(emailTextBox);
	
		fpFirstLine.add(entryLabel);
		
		fpContainer.add(fpFirstLine);
		fpContainer.add(fpName);
		fpContainer.add(fpSurname);
		fpContainer.add(fpUserName);
		fpContainer.add(fpEmail);
		fpContainer.add(goBack);
		

		fpContainer.getElement().addClassName("col-8 mx-auto container border rounded-corners");

		greetingService.getArticleAuthor(title, new AsyncCallback<Author>() {

			@Override
			public void onSuccess(Author result) {

				if (result == null) {
					
					nameTextBox.setText("");
					surnameTextBox.setText("");
					usernameTextBox.setText("");
					emailTextBox.setText("");
					
				} else {
					
					nameTextBox.setText(result.getName());
					nameTextBox.setEnabled(false);
					surnameTextBox.setText(result.getSurname());
					surnameTextBox.setEnabled(false);
					usernameTextBox.setText(result.getUsername());
					usernameTextBox.setEnabled(false);
					emailTextBox.setText(result.getEmail());
					emailTextBox.setEnabled(false);
				}
			}

			@Override
			public void onFailure(Throwable caught) {
				Window.alert("Errore nel recupero degli articoli scritti dall'autore");
			}
		});
		
		//Per evitare il problema della duplicazione
		if (vpInfo == null) {
			
			vpInfo = new VerticalPanel();
			
			setClickHandlers();
			
		}

		vpInfo.add(fpContainer);
		vpInfo.getElement().addClassName("w-100");
		
		RootPanel.get().add(vpInfo);
		
		vpInfo.setVisible(true);

	}
	
	//Gestione dei ClickHandlers
	private static void setClickHandlers() {
		
		//Go back
		goBack.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent clickEvent) {
				vpInfo.setVisible(false);
				Home.main();
			}
		});
	}
}
