package com.google.gwt.sweng.reads.client;

import java.util.ArrayList;
import java.util.Date;

import com.google.gwt.sweng.reads.shared.Admin;
import com.google.gwt.sweng.reads.shared.Article;
import com.google.gwt.sweng.reads.shared.Author;
import com.google.gwt.sweng.reads.shared.Category;
import com.google.gwt.sweng.reads.shared.Comment;
import com.google.gwt.sweng.reads.shared.Gender;
import com.google.gwt.sweng.reads.shared.Like;
import com.google.gwt.sweng.reads.shared.Subscription;
import com.google.gwt.sweng.reads.shared.User;
import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

/**
 * Il client-side del servizio RPC
 */

@RemoteServiceRelativePath("greet")
public interface GreetingService extends RemoteService {

	Article printArticle(String tit) throws IllegalArgumentException;

	ArrayList<Comment> getArticleComments(String tit) throws IllegalArgumentException;

	Boolean newComment(Comment newComm) throws IllegalArgumentException;

	Boolean newLike(Like newLi) throws IllegalArgumentException;
	
	Subscription getSubscriptionInfo(User utenteLoggato) throws IllegalArgumentException;
	
	ArrayList<Article> getArticles(String cat) throws IllegalArgumentException;
	
	ArrayList<Article> getArticlesSubCat(String cat) throws IllegalArgumentException;

	ArrayList<Article> getListArticles() throws IllegalArgumentException;

	Admin adminLogin(String password) throws IllegalArgumentException;

	User login(String username, String password) throws IllegalArgumentException;

	User getUserInfo(String nomeUtenteCorrente) throws IllegalArgumentException;

	User registration(String username, String email, String password, String name, String surname, Date birthDate, String birthCity, Gender genre, String address, ArrayList<String> listFavCat) throws IllegalArgumentException;

	User subscribe(String nomeUtente, String cardNumber, String name, String surname, String expirationDate, String CVV, String nation, String cap) throws IllegalArgumentException;

	String printAllUsers() throws IllegalArgumentException;

	Author getArticleAuthor(String titoloArticolo);

	/*
	 * ADMIN
	 */

	 ArrayList<String> getAllTitles() throws IllegalArgumentException;

	 ArrayList<String> getCategories() throws IllegalArgumentException;

	 ArrayList<String> getSubCategories(String cat) throws IllegalArgumentException;

	 ArrayList<String> getAllAuthors() throws IllegalArgumentException;

	 Article getArticle(String art) throws IllegalArgumentException;

	 Boolean addArticle(Article art) throws IllegalArgumentException;

	 Boolean modifyArticle(String t, Article a) throws IllegalArgumentException;

	 Boolean deleteArticle(String t) throws IllegalArgumentException;

	 Boolean addCategory(Category c) throws IllegalArgumentException;

	 Boolean addSubCategory(String cat, String subcat) throws IllegalArgumentException;

	 Boolean deleteCategory(String cat) throws IllegalArgumentException;

	 Boolean deleteSubCategory(String cat, String subcat) throws IllegalArgumentException;

	 ArrayList<Comment> getUnmoderatedComments() throws IllegalArgumentException;

	 ArrayList<Comment> getUnmoderatedComments(String t) throws IllegalArgumentException;

	 ArrayList<Comment> getModeratedComments() throws IllegalArgumentException;

	 ArrayList<Comment> getModeratedComments(String t) throws IllegalArgumentException;

	 Boolean approveComment(String text) throws IllegalArgumentException;

	 Boolean deleteComment(String text) throws IllegalArgumentException;

	 Author authorRegistration(String username, String email, String pw, String name, String surname, Date dateReg);
	 
	 ArrayList<String> getActivityComments(String name);

	ArrayList<String> getActivityLikes(String username);

	Author loginAuthor(String username, String password);

	ArrayList<String> getListArticleAuthor(String username);

}
