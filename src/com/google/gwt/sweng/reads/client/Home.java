package com.google.gwt.sweng.reads.client;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.sweng.reads.shared.Article;
import com.google.gwt.sweng.reads.shared.User;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.*;

import java.util.ArrayList;

public class Home {

	//OGGETTI HOME
	final static String styleButton = "m-2 btn btn-primary btn-sm";
	private static Button bttLogin = new Button("Login");
	private static Button bttRegistrazione = new Button("Registrati");
	private static Label authorLabel = new Label("Vuoi diventare autore?");
	private static Button bttRegAut = new Button("Registrati come autore");
	private static Button bttAbbonamento = new Button("Abbonati");
	private static Button bttReadInfo = new Button("Leggi info");
	private static Button bttLogOut = new Button("Logout");
	private static ListBox listCategory = new ListBox();
	private static ListBox listSubCat = new ListBox();

	static Integer numTotLike;
	static Integer numTotComm;
	static User utenteLoggato;
	static String currentCategory;


	//LAYOUT HOME
	public static FlowPanel fpContainer = new FlowPanel();
	public static FlowPanel fpHome = new FlowPanel();
	public static FlowPanel fpCategory = new FlowPanel();
	public static FlowPanel artContainer = new FlowPanel();
	public static FlowPanel fpAuthor = new FlowPanel();
	public static VerticalPanel vpHome;


	//SERVICE
	private static final GreetingServiceAsync greetingService = GWT.create(GreetingService.class);
	private static final SessionServiceAsync sessionService = GWT.create(SessionService.class); 


	public static void main() {

		//Oggetti "barra di navigazione"
		fpHome.add(bttLogin);
		fpHome.add(bttRegistrazione);
		fpHome.add(bttLogOut);
		fpHome.add(bttReadInfo);
		fpHome.add(bttAbbonamento);

		listCategory.setStyleName("m-2 btn btn-secondary dropdown-toggle");		
		listSubCat.setStyleName("m-2 btn btn-secondary dropdown-toggle");

		bttLogin.setStyleName(styleButton);
		bttAbbonamento.setStyleName(styleButton);
		bttLogOut.setStyleName(styleButton);
		bttReadInfo.setStyleName(styleButton);
		bttRegAut.setStyleName(styleButton);
		bttRegistrazione.setStyleName(styleButton);
		
		
		//Select categorie e sottocategorie
		fpCategory.add(listCategory);
		fpCategory.add(listSubCat);
		fpCategory.getElement().addClassName("mx-auto");
		fpCategory.getElement().setAttribute("align", "center");
		
		
		//Sezione autore
		fpAuthor.add(authorLabel);
		authorLabel.setStyleName("d-inline");
		fpAuthor.add(bttRegAut);

		fpAuthor.getElement().addClassName("author");

		artContainer.getElement().addClassName("col-8 mx-auto border rounded-corners button");

		
		//Container di tutti i precedenti layout
		fpContainer.add(fpAuthor);
		fpContainer.add(fpHome);
		fpContainer.add(fpCategory);
		fpContainer.add(artContainer);

		fpContainer.getElement().addClassName("col-8 mx-auto container border rounded-corners");

		
		//SESSION
		sessionService.getSession(new AsyncCallback<User>() {

			@Override
			public void onFailure(Throwable caught) {
				Window.alert("Errore nel caricamento della sessione.\n" + caught.toString());
			}

			@Override
			public void onSuccess(User result) {
				if (result != null) {
					bttAbbonamento.setStyleName("float-right", true);
					
					if(result.isSubscribed())
						bttAbbonamento.setVisible(false);
					else
						bttAbbonamento.setVisible(true);
					bttLogin.setVisible(false);
					bttRegistrazione.setVisible(false);
					authorLabel.setVisible(false);
					bttRegAut.setVisible(false);
					fpCategory.setVisible(false);
					fpHome.add(listCategory);
					fpHome.add(listSubCat);
					listCategory.setStyleName("float-left", true);
					listSubCat.setStyleName("float-left", true);
					bttLogOut.setVisible(true);
					bttLogOut.setStyleName("float-right", true);
					bttReadInfo.setVisible(true);
					bttReadInfo.setStyleName("float-right", true);
					fpHome.setStyleName("mb-5",true);

					fpAuthor.setVisible(false);

				} else {
					
					listCategory.setVisible(true);
					listSubCat.setVisible(true);
					bttAbbonamento.setVisible(false);
					bttLogin.setVisible(true);
					bttRegistrazione.setVisible(true);
					bttLogOut.setVisible(false);
					bttReadInfo.setVisible(false);
					fpAuthor.setVisible(true);
					bttRegAut.setVisible(true);
					authorLabel.setVisible(false);
					bttLogOut.setVisible(false);
					bttReadInfo.setVisible(false);
				}

				//Memorizzo nella variabile locale utenteLoggato il valore restituito dalla chiamata getSession()
				utenteLoggato = result;
			}

		});


		//Caricamento di tutte le categorie
		greetingService.getCategories(new AsyncCallback<ArrayList<String>>() {

			@Override
			public void onFailure(Throwable caught) {
				Window.alert("Errore caricamento delle categorie.\n" + caught.toString());				
			}

			@Override
			public void onSuccess(ArrayList<String> result) {
				listCategory.clear();
				listSubCat.clear();
				listCategory.addItem("Tutti gli articoli");	
				listSubCat.addItem("-");

				for (String c: result) {
					listCategory.addItem(c);
				}

			}

		});


		//stampa di tutti gli articoli: SERVE SOLO QUANDO SI FA IL PRIMO ACCESSO!
		greetingService.getListArticles(new AsyncCallback<ArrayList<Article>>() {

			@Override
			public void onFailure(Throwable caught) {
				Window.alert("Errore nel caricamento di tutti gli articoli!\n" + caught.toString());	
			}

			@Override
			public void onSuccess(ArrayList<Article> result) {

				artContainer.clear();

				for (final Article art : result) {

					//Struttura articolo
					HTMLPanel likeComments = new HTMLPanel("<span class=\"col-2 badge badge-light\"> " + art.getNumTotLike() + " Mi piace</span> "
							+ "<span class=\"badge badge-light\"> " + art.getNumTotComm() + " Commenti</span>");
					Button gotoarticle = new Button("Apri articolo");
					likeComments.setStyleName("align-items-center");

					FlowPanel fpLastLine = new FlowPanel();
					fpLastLine.add(gotoarticle);
					fpLastLine.add(likeComments);
					fpLastLine.getElement().setClassName("mt-2");

					gotoarticle.setStyleName("btn btn-info btn-sm float-right");
					likeComments.setStyleName("d-inline");

					//aggiungo la stampa dell'articolo al layout
					artContainer.add(art.getPanelArticleShortVersion());
					artContainer.add(fpLastLine);
					HTMLPanel hr = new HTMLPanel("<div></div><hr>");
					artContainer.add(hr);

					
					//Implemento l'azione di quando un utente preme il pulsante "Apri articolo"
					gotoarticle.addClickHandler(new ClickHandler() {
						@Override
						public void onClick(ClickEvent clickEvent) {
							vpHome.setVisible(false);

							//Gestione di tutti i casi legati all'apertura di un articolo
							if(utenteLoggato == null && art.isPremium()) {		//se l'utente NON � loggato e l'articolo � premium
								Registration.main();		//Viene mostrata la finestra di registrazione
							} else if(utenteLoggato != null && !utenteLoggato.isSubscribed() && art.isPremium()) {		//se l'utente � loggato ma non � abbonato e l'articolo � premium
								Subscribe.main();		//Viene mostrata la finestra di abbonamento
							} else {
								OpenArticle.main(art.getTitle());		//Viene mostrato l'articolo				
							}
							
						}
					});		
				}
			}
		});


		//Per evitare il problema della duplicazione
		if (vpHome == null) {	

			vpHome = new VerticalPanel();

			setClickHandlers();
		}


		vpHome.add(fpContainer);
		vpHome.getElement().addClassName("w-100");

		RootPanel.get().add(vpHome);
		vpHome.setVisible(true);

	}

	//Gestione dei ClickHandlers
	private static void setClickHandlers() {

		//Login
		bttLogin.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent clickEvent) {
				vpHome.setVisible(false);
				Login.main();
			}
		});

		//Registrazione
		bttRegistrazione.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent clickEvent) {
				vpHome.setVisible(false);
				Registration.main();
			}
		});

		//Registrazione autore
		bttRegAut.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent clickEvent) {
				vpHome.setVisible(false);
				RegistrationAuthor.main();
			}
		});

		//Abbonamento
		bttAbbonamento.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent clickEvent) {
				vpHome.setVisible(false);
				Subscribe.main();
			}
		});

		//Visualizzazione informazioni utente loggato
		bttReadInfo.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent clickEvent) {
				vpHome.setVisible(false);
				ReadInfo.main();
			}
		});

		//Logout
		bttLogOut.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {

				sessionService.exitSession(new AsyncCallback<Void>() {

					@Override
					public void onSuccess(Void result) {
						vpHome.setVisible(false);
						main();
					}

					@Override
					public void onFailure(Throwable caught) {
						Window.alert("Errore chiusura sessione. " + caught.toString());
					}
				});
			}

		});

		//A seconda della categoria scelta dall'utente tramite l'apposita select, vengono mostrati solo gli articoli appartenenti a codesta categoria
		//Attenzione: si tratta di un "addChangeHandler"!
		listCategory.addChangeHandler(new ChangeHandler() {

			@Override
			public void onChange(ChangeEvent event) {

				//Memorizzo in una variabile locale la categoria selezionata tramite la select
				currentCategory = listCategory.getSelectedItemText();

				//Se la categoria selezionata � costituita da sottocategorie, dovr� andare a inserirle all'interno della seconda select 
				greetingService.getSubCategories(listCategory.getSelectedItemText(), new AsyncCallback<ArrayList<String>>() {

					@Override
					public void onFailure(Throwable caught) {
						Window.alert("Errore nel caricamento delle sottocategorie");
					}

					@Override
					public void onSuccess(ArrayList<String> result) {

						//Aggiorno la lista di sottocategorie
						listSubCat.clear();

						if (result.size() != 0) {		//Se sono presenti della sottocategorie associate alla categoria selezionata
							
							listSubCat.addItem("Tutte le sottocategorie");
							
							for (String c: result) {
								listSubCat.addItem(c);
							}
							
						} else {
							
							listSubCat.addItem(" - ");
							
						}

					}
				});


				//Stampo la lista di articoli associati alla categoria selezionata
				greetingService.getArticles(listCategory.getSelectedItemText(), new AsyncCallback<ArrayList<Article>>()  {

					@Override
					public void onFailure(Throwable caught) {
						Window.alert("Errore nel caricamento degli articoli dopo la selezione delle categorie!");
					}

					@Override
					public void onSuccess(ArrayList<Article> result) {
						
						artContainer.clear();

						for (final Article art : result) {

							HTMLPanel likeComments = new HTMLPanel("<span class=\"col-2 badge badge-light\"> " + art.getNumTotLike() + " Mi piace</span> "
									+ "<span class=\"badge badge-light\"> " + art.getNumTotComm() + " Commenti</span>");
							Button gotoarticle = new Button("Apri articolo");
							likeComments.setStyleName("align-items-center");

							FlowPanel fpLastLine = new FlowPanel();
							fpLastLine.add(gotoarticle);
							fpLastLine.add(likeComments);
							fpLastLine.getElement().setClassName("mt-2");

							gotoarticle.setStyleName("btn btn-info btn-sm float-right");
							likeComments.setStyleName("d-inline");

							//aggiungo la stampa dell'articolo al layout
							artContainer.add(art.getPanelArticleShortVersion());
							artContainer.add(fpLastLine);
							HTMLPanel hr = new HTMLPanel("<div></div><hr>");
							artContainer.add(hr);

							//Implemento nuovamente l'azione di quando un utente preme il pulsante "Apri articolo"
							gotoarticle.addClickHandler(new ClickHandler() {
								@Override
								public void onClick(ClickEvent clickEvent) {
									
									vpHome.setVisible(false);

									if(utenteLoggato == null && art.isPremium()) {
										
										Registration.main();
										
									} else if(utenteLoggato != null && !utenteLoggato.isSubscribed() && art.isPremium()) {
										
										Subscribe.main();
										
									} else {
										
										OpenArticle.main(art.getTitle());				
										
									}
								}
							});		
						}
					}
				});
			}

		});


		//A seconda della sottocategoria scelta dall'utente tramite l'apposita select, vengono mostrati solo gli articoli appartenenti a codesta sottocategoria
		//Attenzione: anche questo si tratta di un "addChangeHandler"!
		listSubCat.addChangeHandler(new ChangeHandler() {

			@Override
			public void onChange(ChangeEvent event) {

				//Se vengono selezionate tutte le sottocategorie, allora devo stampare tutti e soli gli articoli della categoria
				if (listSubCat.getSelectedItemText().equals("Tutte le sottocategorie")) {

					//stampo la lista di articoli di quella categoria
					greetingService.getArticles(currentCategory, new AsyncCallback<ArrayList<Article>>()  {

						@Override
						public void onFailure(Throwable caught) {

							Window.alert("Errore nel caricamento degli articoli dopo la selezione della sottocategorie");

						}

						@Override
						public void onSuccess(ArrayList<Article> result) {
							
							artContainer.clear();

							for (final Article art : result) {

								HTMLPanel likeComments = new HTMLPanel("<span class=\"col-2 badge badge-light\"> " + art.getNumTotLike() + " Mi piace</span> "
										+ "<span class=\"badge badge-light\"> " + art.getNumTotComm() + " Commenti</span>");
								Button gotoarticle = new Button("Apri articolo");
								likeComments.setStyleName("align-items-center");

								FlowPanel fpLastLine = new FlowPanel();
								fpLastLine.add(gotoarticle);
								fpLastLine.add(likeComments);
								fpLastLine.getElement().setClassName("mt-2");

								gotoarticle.setStyleName("btn btn-info btn-sm float-right");
								likeComments.setStyleName("d-inline");

								//aggiungo la stampa dell'articolo al layout
								artContainer.add(art.getPanelArticleShortVersion());
								artContainer.add(fpLastLine);
								HTMLPanel hr = new HTMLPanel("<div></div><hr>");
								artContainer.add(hr);

								//Implemento nuovamente l'azione di quando un utente preme il pulsante "Apri articolo"
								gotoarticle.addClickHandler(new ClickHandler() {
									@Override
									public void onClick(ClickEvent clickEvent) {
										vpHome.setVisible(false);

										if(utenteLoggato == null && art.isPremium()) {
											
											Registration.main();
											
										} else if(utenteLoggato != null && !utenteLoggato.isSubscribed() && art.isPremium()) {
											
											Subscribe.main();
											
										} else {
											
											OpenArticle.main(art.getTitle());						
											
										}
									}
								});		
							}
						}
					});
				}

				
				//Se viene selezionata una particolare sottocategoria, devo stampare solo gli articoli di quella determinata sottocategoria
				else {
					greetingService.getArticlesSubCat(listSubCat.getSelectedItemText(), new AsyncCallback<ArrayList<Article>>()  {

						@Override
						public void onFailure(Throwable caught) {

							Window.alert("Errore nel caricamento degli articoli dopo la selezione della sottocategoria");

						}

						@Override
						public void onSuccess(ArrayList<Article> result) {

							artContainer.clear();

							for (final Article art : result) {

								HTMLPanel likeComments = new HTMLPanel("<span class=\"col-2 badge badge-light\"> " + art.getNumTotLike() + " Mi piace</span> "
										+ "<span class=\"badge badge-light\"> " + art.getNumTotComm() + " Commenti</span>");
								Button gotoarticle = new Button("Apri articolo");
								likeComments.setStyleName("align-items-center");

								FlowPanel fpLastLine = new FlowPanel();
								fpLastLine.add(gotoarticle);
								fpLastLine.add(likeComments);
								fpLastLine.getElement().setClassName("mt-2");

								gotoarticle.setStyleName("btn btn-info btn-sm float-right");
								likeComments.setStyleName("d-inline");

								//aggiungo la stampa dell'articolo al layout
								artContainer.add(art.getPanelArticleShortVersion());
								artContainer.add(fpLastLine);
								HTMLPanel hr = new HTMLPanel("<div></div><hr>");
								artContainer.add(hr);

								//Implemento nuovamente l'azione di quando un utente preme il pulsante "Apri articolo"
								gotoarticle.addClickHandler(new ClickHandler() {
									@Override
									public void onClick(ClickEvent clickEvent) {
										vpHome.setVisible(false);

										if(utenteLoggato == null && art.isPremium()) {
											
											Registration.main();
											
										} else if(utenteLoggato != null && !utenteLoggato.isSubscribed() && art.isPremium()) {
											
											Subscribe.main();
										
										} else {
										
											OpenArticle.main(art.getTitle());								
										
										}
									}
								});		
							}
						} 
					});
				}
			}
		});
	}
}
