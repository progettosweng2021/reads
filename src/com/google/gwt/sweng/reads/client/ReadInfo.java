package com.google.gwt.sweng.reads.client;

import java.util.ArrayList;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.sweng.reads.shared.Subscription;
import com.google.gwt.sweng.reads.shared.User;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.Grid;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.TabPanel;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;

public class ReadInfo {

	//Create a remote service proxy to talk to the server-side Greeting service.   
	private static final GreetingServiceAsync greetingService = GWT.create(GreetingService.class);
	private static final SessionServiceAsync sessionService = GWT.create(SessionService.class); 

	private static HTMLPanel titleInfo = new HTMLPanel("<h4>INFORMAZIONI UTENTE</h4>");

	//Widget info utente
	public static FlowPanel fpNewDatiPersonali = new FlowPanel();
	private static Grid gridDatiPersonali = new Grid(5, 4);
	private static HTMLPanel titleDatiPersonali = new HTMLPanel("<h4>Dati personali</h4>");
	private static Label nameLabelDatiPersonali = new Label("Nome:");
	private static TextBox nameTextBoxDatiPersonali = new TextBox();
	private static Label surnameLabelDatiPersonali = new Label("Cognome:");
	private static TextBox surnameTextBoxDatiPersonali = new TextBox();
	private static Label usernameLabelDatiPersonali = new Label("Username:");
	private static TextBox usernameTextBoxDatiPersonali = new TextBox();
	private static Label dateLabelDatiPersonali = new Label("Data di nascita:");
	private static TextBox dateTextBoxDatiPersonali = new TextBox();
	private static Label emailLabelDatiPersonali = new Label("Email:");
	private static TextBox emailTextBoxDatiPersonali = new TextBox();
	private static Label pwLabelDatiPersonali = new Label("Password:");
	private static TextBox pwTextBoxDatiPersonali = new TextBox();
	private static Label addressLabelDatiPersonali = new Label("Indirizzo:");
	private static TextBox addressTextBoxDatiPersonali = new TextBox();
	private static Label sexLabelDatiPersonali = new Label("Genere:");
	private static TextBox sexTextBoxDatiPersonali = new TextBox();
	private static Label birthPlaceLabelDatiPersonali = new Label("Luogo di nascita:");
	private static TextBox birthPlaceTextBoxDatiPersonali = new TextBox();
	private static Label categoryLabelDatiPersonali = new Label("Categorie preferite:");


	//Widget info dati fatturazione
	public static FlowPanel fpNewDatiFatturazione = new FlowPanel();
	private static Grid gridDatiFatturazione = new Grid(8, 2);
	private static HTMLPanel titleDatiFatturazione = new HTMLPanel("<h4>Dati Fatturazione</h4>");

	private static Label cardNumberLabelDatiFatturazione = new Label("Numero Carta:");
	private static TextBox cardNumberTextBoxDatiFatturazione = new TextBox();
	private static Label nameLabelDatiFatturazione = new Label("Nome:");
	private static TextBox nameTextBoxDatiFatturazione = new TextBox();
	private static Label surnameLabelDatiFatturazione = new Label("Cognome:");
	private static TextBox surnameTextBoxDatiFatturazione = new TextBox();
	private static Label dateLabelDatiFatturazione = new Label("Data Scadenza:");
	private static TextBox dateTextBoxDatiFatturazione = new TextBox();
	private static Label CVVLabelDatiFatturazione = new Label("CVV:");
	private static TextBox CVVTextBoxDatiFatturazione = new TextBox();
	private static Label nationLabelDatiFatturazione = new Label("Nazionalita':");
	private static TextBox nationTextBoxDatiFatturazione = new TextBox();
	private static Label CAPLabelDatiFatturazione = new Label("CAP:");
	private static TextBox CAPTextBoxDatiFatturazione = new TextBox();

	//non abbonato
	private static HTMLPanel message1DatiFatturazione = new HTMLPanel("<b>ATTENZIONE: l'utente non risulta ancora abbonato! </b>");
	private static HTMLPanel message2DatiFatturazione = new HTMLPanel("<b>Per effettuare l'abbonamento premere il tasto seguente.</b>");
	private static HTMLPanel attRecente;
	private static Button newSub = new Button("Abbonati");

	//attivita' recenti
	private static FlowPanel fpActivity = new FlowPanel();
	private static FlowPanel fpComments = new FlowPanel();
	private static FlowPanel fpLikes = new FlowPanel();
	private static Grid gridActivity = new Grid(10,2);
	private static HTMLPanel titleActivity = new HTMLPanel("<h4>Attivita' Recenti</h4>");


	private static Button goBack = new Button("Torna indietro");
	static User utenteLoggato;

	public static VerticalPanel vpReadInfo;
	public static FlowPanel fpContainerA = new FlowPanel();
	public static FlowPanel fpBackButton = new FlowPanel();
	public static TabPanel tabPanel = new TabPanel();
	final static String styleLabel = "mb-4";
	final static String styleTB = "mr-4 mb-4";


	public static void main() {

		nameLabelDatiPersonali.setStyleName(styleLabel);
		addressLabelDatiPersonali.setStyleName(styleLabel);
		birthPlaceLabelDatiPersonali.setStyleName(styleLabel);
		categoryLabelDatiPersonali.setStyleName(styleLabel);
		dateLabelDatiPersonali.setStyleName(styleLabel);
		emailLabelDatiPersonali.setStyleName(styleLabel);
		usernameLabelDatiPersonali.setStyleName(styleLabel);
		surnameLabelDatiPersonali.setStyleName(styleLabel);
		pwLabelDatiPersonali.setStyleName(styleLabel);
		sexLabelDatiPersonali.setStyleName(styleLabel);
		nameTextBoxDatiPersonali.setStyleName(styleTB);
		addressTextBoxDatiPersonali.setStyleName(styleTB);
		birthPlaceTextBoxDatiPersonali.setStyleName(styleTB);
		dateTextBoxDatiPersonali.setStyleName(styleLabel);
		emailTextBoxDatiPersonali.setStyleName(styleTB);
		usernameTextBoxDatiPersonali.setStyleName(styleTB);
		surnameTextBoxDatiPersonali.setStyleName(styleLabel);
		pwTextBoxDatiPersonali.setStyleName(styleLabel);
		sexTextBoxDatiPersonali.setStyleName(styleLabel);

		//dati personali
		fpNewDatiPersonali.getElement().addClassName("col-12");
		fpNewDatiPersonali.getElement().setAttribute("align", "center");
		fpNewDatiPersonali.add(titleDatiPersonali);

		gridDatiPersonali.setWidget(0, 0, nameLabelDatiPersonali);
		gridDatiPersonali.setWidget(0, 1, nameTextBoxDatiPersonali);
		gridDatiPersonali.setWidget(0, 2, surnameLabelDatiPersonali);
		gridDatiPersonali.setWidget(0, 3, surnameTextBoxDatiPersonali);
		gridDatiPersonali.setWidget(1, 0, usernameLabelDatiPersonali);
		gridDatiPersonali.setWidget(1, 1, usernameTextBoxDatiPersonali);
		gridDatiPersonali.setWidget(1, 2, dateLabelDatiPersonali);
		gridDatiPersonali.setWidget(1, 3, dateTextBoxDatiPersonali);
		gridDatiPersonali.setWidget(2, 0, emailLabelDatiPersonali);
		gridDatiPersonali.setWidget(2, 1, emailTextBoxDatiPersonali);
		gridDatiPersonali.setWidget(2, 2, pwLabelDatiPersonali);
		gridDatiPersonali.setWidget(2, 3, pwTextBoxDatiPersonali);
		gridDatiPersonali.setWidget(3, 0, addressLabelDatiPersonali);
		gridDatiPersonali.setWidget(3, 1, addressTextBoxDatiPersonali);
		gridDatiPersonali.setWidget(3, 2, sexLabelDatiPersonali);
		gridDatiPersonali.setWidget(3, 3, sexTextBoxDatiPersonali);
		gridDatiPersonali.setWidget(4, 0, birthPlaceLabelDatiPersonali);
		gridDatiPersonali.setWidget(4, 1, birthPlaceTextBoxDatiPersonali);
		gridDatiPersonali.setWidget(4, 2, categoryLabelDatiPersonali);

		fpNewDatiPersonali.add(gridDatiPersonali);


		//dati fatturazione
		fpNewDatiFatturazione.getElement().addClassName("col-12");
		fpNewDatiFatturazione.getElement().setAttribute("align", "center");
		fpNewDatiFatturazione.add(titleDatiFatturazione);


		gridDatiFatturazione.setWidget(0, 0, cardNumberLabelDatiFatturazione );
		gridDatiFatturazione.setWidget(0, 1, cardNumberTextBoxDatiFatturazione );
		gridDatiFatturazione.setWidget(1, 0, nameLabelDatiFatturazione );
		gridDatiFatturazione.setWidget(1, 1, nameTextBoxDatiFatturazione );
		gridDatiFatturazione.setWidget(2, 0, surnameLabelDatiFatturazione );
		gridDatiFatturazione.setWidget(2, 1, surnameTextBoxDatiFatturazione );
		gridDatiFatturazione.setWidget(3, 0, dateLabelDatiFatturazione );
		gridDatiFatturazione.setWidget(3, 1, dateTextBoxDatiFatturazione );
		gridDatiFatturazione.setWidget(4, 0, CVVLabelDatiFatturazione );
		gridDatiFatturazione.setWidget(4, 1, CVVTextBoxDatiFatturazione );
		gridDatiFatturazione.setWidget(5, 0, nationLabelDatiFatturazione );
		gridDatiFatturazione.setWidget(5, 1, nationTextBoxDatiFatturazione );
		gridDatiFatturazione.setWidget(6, 0, CAPLabelDatiFatturazione );
		gridDatiFatturazione.setWidget(6, 1, CAPTextBoxDatiFatturazione );



		//attivita' recenti
		fpActivity.add(titleActivity);
		fpActivity.add(gridActivity);
		fpActivity.getElement().addClassName("col-12");
		fpActivity.getElement().setAttribute("align", "center");

		titleInfo.getElement().addClassName("title"); 


		//tabPanel
		tabPanel.getElement().addClassName("col-10 mx-auto container rounded-corners");
		tabPanel.add(fpNewDatiPersonali,"Dati Personali");
		tabPanel.add(fpNewDatiFatturazione,"Dati Fatturazione");
		tabPanel.add(fpActivity, "Attivita' recenti");
		tabPanel.selectTab(0);


		//goBack
		goBack.setStyleName("btn btn-outline-secondary btn-small mt-4");
		fpBackButton.add(goBack);
		fpBackButton.getElement().setAttribute("align", "center");


		//fpContainerA
		fpContainerA.add(titleInfo);
		fpContainerA.add(tabPanel);
		fpContainerA.add(fpBackButton);
		fpContainerA.getElement().addClassName("col-8 mx-auto container border border-primary rounded-corners");


		//Recupero l'utente loggato
		sessionService.getSession(new AsyncCallback<User>() {

			@Override
			public void onSuccess(User result) {

				if (result == null) {

					//dati personali
					nameTextBoxDatiPersonali.setText("");
					surnameTextBoxDatiPersonali.setText("");
					usernameTextBoxDatiPersonali.setText("");
					dateTextBoxDatiPersonali.setText("");
					emailTextBoxDatiPersonali.setText("");
					pwTextBoxDatiPersonali.setText("");
					addressTextBoxDatiPersonali.setText("");
					sexTextBoxDatiPersonali.setText("");
					birthPlaceTextBoxDatiPersonali.setText("");

				} else {

					utenteLoggato = result;

					//riempio le TextBox dei dati personali
					nameTextBoxDatiPersonali.setText(result.getName());
					nameTextBoxDatiPersonali.setEnabled(false);
					surnameTextBoxDatiPersonali.setText(result.getSurname());
					surnameTextBoxDatiPersonali.setEnabled(false);
					usernameTextBoxDatiPersonali.setText(result.getUsername());
					usernameTextBoxDatiPersonali.setEnabled(false);
					dateTextBoxDatiPersonali.setText(result.getBirthDate().toLocaleString());
					dateTextBoxDatiPersonali.setEnabled(false);
					emailTextBoxDatiPersonali.setText(result.getEmail());
					emailTextBoxDatiPersonali.setEnabled(false);
					pwTextBoxDatiPersonali.setText(result.getPassword());
					pwTextBoxDatiPersonali.setEnabled(false);
					addressTextBoxDatiPersonali.setText(result.getAddress());
					addressTextBoxDatiPersonali.setEnabled(false);
					sexTextBoxDatiPersonali.setText(result.getGenre());
					sexTextBoxDatiPersonali.setEnabled(false);
					birthPlaceTextBoxDatiPersonali.setText(result.getBirthCity());
					birthPlaceTextBoxDatiPersonali.setEnabled(false);

					//recupero le categorie preferite
					ArrayList<String> listFavCat = new ArrayList<String>();
					listFavCat = result.getFavCat();

					//e le inserisco in una List Box
					ListBox tb = new ListBox();
					tb.setStyleName(styleTB);
					for(String cat : listFavCat) {
						tb.addItem(cat);
					}
					gridDatiPersonali.setWidget(4, 3, tb);


					//pulisco la griglia delle attivit�
					gridActivity.clear();



					//verifico che l'utente sia abbonato o meno
					if(utenteLoggato.isSubscribed()) {

						fpNewDatiFatturazione.clear();

						fpNewDatiFatturazione.add(gridDatiFatturazione);

						greetingService.getSubscriptionInfo(utenteLoggato, new AsyncCallback<Subscription>() {

							@Override
							public void onFailure(Throwable caught) {

								Window.alert("Errore recupero informazioni dati fatturazione. " + caught.toString());

							}

							@Override
							public void onSuccess(Subscription result) {

								cardNumberTextBoxDatiFatturazione.setText(result.getNumCard());
								cardNumberTextBoxDatiFatturazione.setEnabled(false);
								nameTextBoxDatiFatturazione.setText(result.getName()); 
								nameTextBoxDatiFatturazione.setEnabled(false);
								surnameTextBoxDatiFatturazione.setText(result.getSurname()); 
								surnameTextBoxDatiFatturazione.setEnabled(false);	
								dateTextBoxDatiFatturazione.setText(result.getDate().toString()); 
								dateTextBoxDatiFatturazione.setEnabled(false);
								CVVTextBoxDatiFatturazione.setText(result.getCvv()); 
								CVVTextBoxDatiFatturazione.setEnabled(false);
								nationTextBoxDatiFatturazione.setText(result.getNation()); 
								nationTextBoxDatiFatturazione.setEnabled(false);
								CAPTextBoxDatiFatturazione.setText(result.getCap()); 
								CAPTextBoxDatiFatturazione.setEnabled(false);

							}

						});						

					} else {

						fpNewDatiFatturazione.clear();

						fpNewDatiFatturazione.add(message1DatiFatturazione);
						fpNewDatiFatturazione.add(message2DatiFatturazione);
						newSub.setStyleName("btn btn-primary mt-2");
						fpNewDatiFatturazione.add(newSub);

					}
				}

				//Recupero le attivit� recenti legati ai commenti dell'autore loggato
				greetingService.getActivityComments(result.getUsername(), new AsyncCallback<ArrayList<String>>() {

					@Override
					public void onSuccess(ArrayList<String> result) {
						int i = 0;

						for (String str : result) {
							Label titleArt = new Label();
							titleArt.setStyleName("container-sm border border-success m-1");
							titleArt.setText("Hai commentato il post: "+str);
							gridActivity.setWidget(i,0,titleArt);
							i++;
						}						
					}

					@Override
					public void onFailure(Throwable caught) {
						Window.alert("Errore nel caricamento delle attivita' recenti");
						Home.main();
					}
				});

				//Recupero le attivit� recenti legate ai like dell'autore loggato
				greetingService.getActivityLikes(result.getUsername(), new AsyncCallback<ArrayList<String>>() {

					@Override
					public void onSuccess(ArrayList<String> result) {

						int i = 0;

						for (String str : result) {
							Label art = new Label();
							art.setStyleName("container-sm border border-success m-1");
							art.setText("Hai messo like al post: "+str);
							gridActivity.setWidget(i, 1, art);
							i++;
						}						
					}

					@Override
					public void onFailure(Throwable caught) {
						Window.alert("Errore nel caricamento delle attivita' recenti");
						Home.main();
					}
				});
			}

			@Override
			public void onFailure(Throwable caught) {
				Window.alert("Errore nel caricamento della sessione");
			}
		});


		//Per evitare il problema della duplicazione
		if (vpReadInfo == null) {

			vpReadInfo = new VerticalPanel();

			setClickHandlers();

		}

		vpReadInfo.add(fpContainerA);
		vpReadInfo.getElement().addClassName("w-100");

		RootPanel.get().add(vpReadInfo);

		vpReadInfo.setVisible(true);

	}

	//Gestione dei ClickHandlers
	private static void setClickHandlers() {
		//Go back
		goBack.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent clickEvent) {

				vpReadInfo.setVisible(false);
				Home.main();

			}
		});

		//Nuovo abbonamento
		newSub.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				
				fpNewDatiFatturazione.clear();
				vpReadInfo.setVisible(false);
				Subscribe.main();
				
			}

		});
	}
}