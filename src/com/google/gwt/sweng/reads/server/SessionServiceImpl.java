package com.google.gwt.sweng.reads.server;

import java.time.Instant;

import javax.servlet.http.HttpSession;
import com.google.gwt.sweng.reads.client.SessionService;
import com.google.gwt.sweng.reads.shared.Author;
import com.google.gwt.sweng.reads.shared.User;
import com.google.gwt.user.server.rpc.RemoteServiceServlet;

@SuppressWarnings("serial")
public class SessionServiceImpl extends RemoteServiceServlet implements SessionService{
	
	HttpSession session;
	
	/*
	 * Metodo che consente di "settare" la sessione per uno specifico utente
	 */
    @Override
    public void setSession(User user) {
    	session = this.getThreadLocalRequest().getSession();
    	if (session.getAttribute("user") != null) {
    		session.removeAttribute("user");
    		session.setAttribute("user", user);
    	} else {
    		session.setAttribute("user", user);
    	}
    	System.out.println("Session Settata - " + Instant.now());
    	
    }
    
    /*
     * Metodo che restituisce l'Utente presente nella sessione
     */
    @Override
    public User getSession() {
    	session = this.getThreadLocalRequest().getSession();
        if(session.getAttribute("user") != null) {
        	System.out.println("Richiesta Get Session - " + Instant.now());
        	return (User) session.getAttribute("user");
        } else {
        	System.err.println("Richiesta Session NULL - " + Instant.now());
        	return null;
        }
        
    }
    

    /* Metodo che consente di "settare" la sessione per uno specifico utente*/
    @Override
    public void setSession(Author author) {
    	session = this.getThreadLocalRequest().getSession();
    	if (session.getAttribute("author") != null) {
    		session.removeAttribute("author");
    		session.setAttribute("author", author);
    	} else {
    		session.setAttribute("author", author);
    	}
    	System.out.println("Session Settata - " + Instant.now());
    	
    }
    
    /*Metodo che restituisce l'Utente presente nella sessione*/
	@Override
    public Author getSessionAut() {
    	session = this.getThreadLocalRequest().getSession();
        if(session.getAttribute("author") != null) {
        	System.out.println("Richiesta Get Session - " + Instant.now());
        	return (Author) session.getAttribute("author");
        } else {
        	System.err.println("Richiesta Session NULL - " + Instant.now());
        	return null;
        }
        
    }
    
    /*Metodo che distrugge la sessione --> LOGOUT*/

    @Override
    public void exitSession() {
    	session = this.getThreadLocalRequest().getSession();
    	System.err.println("CHIUSURA SESSION - " + Instant.now());
        session.invalidate(); // kill session     
    }

}
