package com.google.gwt.sweng.reads.server;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Map.Entry;

import org.mapdb.DB;
import org.mapdb.DBMaker;

import java.util.*;
import java.util.concurrent.ConcurrentNavigableMap;

import com.google.gwt.sweng.reads.shared.*;
import com.google.gwt.sweng.reads.client.GreetingService;
import com.google.gwt.user.server.rpc.RemoteServiceServlet;

/**
 * Implementazione server-side del servizio RPC.
 */
@SuppressWarnings("serial")
public class GreetingServiceImpl extends RemoteServiceServlet implements GreetingService {

	/*
	 * Restituisce l'istanza del database
	 */
	public static DB getDB() {
		DB db = DBMaker.newFileDB(new File("Database")).closeOnJvmShutdown().make();
		return db;
	}

	/*
	 * Metodo di inizializzazione del database. Popola tutte le mappe con dei dati nel caso siano vuote
	 */
	public void init() {

		DB db = getDB();


		/*
		 *  Popolamento Categorie
		 */
		ConcurrentNavigableMap<Integer, Category> mapCat = db.getTreeMap("categoria");

		if (!mapCat.containsKey(0)) {

			Category cat0 = new Category("Italia");
			mapCat.put(0, cat0);

			Category cat1 = new Category("Mondo");
			mapCat.put(1, cat1);

			Category cat2 = new Category("Spettacolo");
			mapCat.put(2, cat2);

			Category cat3 = new Category("Sport");
			cat3.setSubCat("Calcio");
			cat3.setSubCat("Basket");
			cat3.setSubCat("Motori");
			mapCat.put(3, cat3);

			Category cat4 = new Category("Tecnologia");
			cat4.setSubCat("Tecnologia Automobilistica");
			cat4.setSubCat("Intelligenza Artificiale");

			mapCat.put(4, cat4);

			Category cat5 = new Category("Scienza");
			mapCat.put(5, cat5);

			Category cat6 = new Category("Politica");
			mapCat.put(6, cat6);

			Category cat7 = new Category("Business");
			mapCat.put(7, cat7);

			Category cat8 = new Category("Salute");
			mapCat.put(8, cat8);

			Category cat9 = new Category("Arte");
			mapCat.put(9, cat9);

		}

		db.commit();


		/*
		 *  Popolamento Utenti
		 */
		ConcurrentNavigableMap<Integer, User> mapUt = db.getTreeMap("utenti");

		if (!mapUt.containsKey(0)) {

			String sdata = "01-01-2000";
			Date dataUser1 = null;
			try {
				dataUser1 = new SimpleDateFormat("dd-MM-yyyy").parse(sdata);
			} catch (ParseException e) {
				e.printStackTrace();
			}

			ArrayList<String> listFavCatMR = new ArrayList<String>();
			listFavCatMR.add("Italia");
			listFavCatMR.add("Mondo");
			listFavCatMR.add("Spettacolo");

			User ut1 = new User("Marco Rossi","Marco.Rossi@gmail.com","password1","Marco","Rossi",dataUser1,"Milano",Gender.Male,"via miao, Bologna", listFavCatMR);

			mapUt.put(0, ut1);

			// --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- 

			String sdataLB = "16-04-1998";
			Date dataUserLB = null;
			try {
				dataUserLB = new SimpleDateFormat("dd-MM-yyyy").parse(sdataLB);
			} catch (ParseException e) {
				e.printStackTrace();
			}

			ArrayList<String> listFavCatLB = new ArrayList<String>();
			listFavCatLB.add("Sport");
			listFavCatLB.add("Tecnologia");
			listFavCatLB.add("Scienza");

			User utLB = new User("Lorenzo Battisti","lorenzo.battisti3@studio.unibo.it","password","Lorenzo","Cognome",dataUserLB,"Segrate",Gender.Male,"viale Rossi, Bergamo", listFavCatLB);

			utLB.setSubscribed(true);

			mapUt.put(1, utLB);

			// --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- 

			String sdataGC = "01-01-1999";
			Date dataUserGC = null;
			try {
				dataUserGC = new SimpleDateFormat("dd-MM-yyyy").parse(sdataGC);
			} catch (ParseException e) {
				e.printStackTrace();
			}

			ArrayList<String> listFavCatGC = new ArrayList<String>();
			listFavCatGC.add("Politica");
			listFavCatGC.add("Business");
			listFavCatGC.add("Salute");
			listFavCatGC.add("Arte");

			User utGC = new User("Giorgia Castelli","giorgia.castelli2@studio.unibo.it","password","Giorgia","Castelli",dataUserGC,"Bologna",Gender.Female,"via Roma 1, Bologna",listFavCatGC);

			utGC.setSubscribed(true);

			mapUt.put(2, utGC);

			// --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- 

			String sdataRN = "29-12-1999";
			Date dataUserRN = null;
			try {
				dataUserRN = new SimpleDateFormat("dd-MM-yyyy").parse(sdataRN);
			} catch (ParseException e) {
				e.printStackTrace();
			}

			ArrayList<String> listFavCatRN = new ArrayList<String>();
			listFavCatRN.add("Politica");
			listFavCatRN.add("Sport");
			listFavCatRN.add("Mondo");

			User utRN = new User("Riccardo Nassisi","riccarso.nassisi@studio.unibo.it","password","Riccardo","Nassisi",dataUserRN,"Reggio Emilia",Gender.Male,"via Roma 1, Reggio Emilia",listFavCatRN);

			utRN.setSubscribed(false);

			mapUt.put(3, utRN);

			// --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- 

			String sdataUserStandard = "01-01-1990";
			Date dataUserStandard = null;
			try {
				dataUserStandard = new SimpleDateFormat("dd-MM-yyyy").parse(sdataUserStandard);
			} catch (ParseException e) {
				e.printStackTrace();
			}

			ArrayList<String> listFavCatStandard = new ArrayList<String>();
			listFavCatStandard.add("Politica");
			listFavCatStandard.add("Tecnologia");

			User utStandard = new User("testuser","user@test.it","password","Test","User",dataUserStandard,"Bologna",Gender.Male,"via Roma 1, Bologna",listFavCatStandard);

			utStandard.setSubscribed(false);

			mapUt.put(4, utStandard);

			// --- --- --- --- --- --- --- --- --- --- --- --- --- --- --- 

			String sdataUserPremium =  "01-01-1990";
			Date dataUserPremium = null;
			try {
				dataUserPremium = new SimpleDateFormat("dd-MM-yyyy").parse(sdataUserPremium);
			} catch (ParseException e) {
				e.printStackTrace();
			}

			ArrayList<String> listFavCatPremium = new ArrayList<String>();
			listFavCatPremium.add("Politica");
			listFavCatPremium.add("Tecnologia");

			User utPremium = new User("testpremium","premium@test.it","password","Test","Premium",dataUserPremium,"Bologna",Gender.Male,"via Roma 1, Bologna",listFavCatPremium);

			utPremium.setSubscribed(true);

			mapUt.put(5, utPremium);
		}

		db.commit();


		/*
		 * Popolamento Abbonamenti
		 */
		ConcurrentNavigableMap<Integer, Subscription> mapAbb = db.getTreeMap("abbonamenti");

		if (!mapAbb.containsKey(0)) {

			String sDataAbbLB = "01-01-2025";
			Date dataAbbLB = null;
			try {
				dataAbbLB = new SimpleDateFormat("dd-MM-yyyy").parse(sDataAbbLB);
			} catch (ParseException e) {
				e.printStackTrace();
			}

			Subscription subLB = new Subscription("123456789", "Lorenzo", "Battisti", dataAbbLB.toString(), "123", "Italy", "24039", "Lorenzo Battisti");

			mapAbb.put(0, subLB);

			// --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---

			String sDataAbbRN = "01-06-2026";
			Date dataAbbRN = null;
			try {
				dataAbbRN = new SimpleDateFormat("dd-MM-yyyy").parse(sDataAbbRN);
			} catch (ParseException e) {
				e.printStackTrace();
			}

			Subscription subRN = new Subscription("987654321", "Riccardo", "Nassisi", dataAbbRN.toString(), "456", "Italy", "42121", "Riccardo Nassisi");

			mapAbb.put(1, subRN);

			// --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---

			String sDataAbbGC = "01-10-2024";
			Date dataAbbGC = null;
			try {
				dataAbbGC = new SimpleDateFormat("dd-MM-yyyy").parse(sDataAbbGC);
			} catch (ParseException e) {
				e.printStackTrace();
			}

			Subscription subGC = new Subscription("456123789", "Giorgia", "Castelli", dataAbbGC.toString(), "789", "Italy", "40121", "Giorgia Castelli");

			mapAbb.put(2, subGC);

			// --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---

			String sDataAbbPrem = "01-01-2030";
			Date dataAbbPrem = null;
			try {
				dataAbbPrem = new SimpleDateFormat("dd-MM-yyyy").parse(sDataAbbPrem);
			} catch (ParseException e) {
				e.printStackTrace();
			}

			Subscription subPrem = new Subscription("123456789", "Test", "Premium", dataAbbPrem.toString(), "111", "Italy", "40100", "testpremium");

			mapAbb.put(3, subPrem);
		}

		db.commit();


		/*
		 * Popolamento Articoli
		 */
		ConcurrentNavigableMap<Integer, Article> mapArt = db.getTreeMap("articolo");
		if (!mapArt.containsKey(0)) {

			String sDate = "10-11-2020";
			Date dateArt1 = null;
			try {
				dateArt1 = new SimpleDateFormat("dd-MM-yyyy").parse(sDate);
			} catch (ParseException e) {
				e.printStackTrace();
			}
			Article art1 = new Article("Il mondo dei ghepardi","Il ghepardo, il felino pi� veloce della terra, riuscir� a sfuggire all'estinzione?"
					+ " Negli ultimi 18 anni il numero di questa specie si  ridotta del 30%, ad oggi si contano poco"
					+ " pi di 6.600 mila esemplari che sopravvivo in Africa e alcuni in Iran. \r\n"
					+ "le due sottospecie dell'Iran (Acinonyx jubatus venaticus) e dell'Africa nord "
					+ "occidentale (Acinonyx jubatus heckii) sono state classificate come \"Critically Endangered\""
					+ " nella lista rossa dell'IUCN.",dateArt1,"Mondo","Luiginobirichino",false,0,0);
			mapArt.put(0, art1);

			// --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---

			String s2Date = "04-09-2021";
			Date dateArt2 = null;
			try {
				dateArt2 = new SimpleDateFormat("dd-MM-yyyy").parse(s2Date);
			} catch (ParseException e) {
				e.printStackTrace();
			}
			Article art2 = new Article("Meno zucchero in cibi e bevande: primi passi negli USA","Lo zucchero, assunto in quantit� eccessive, "
					+ "si trasforma in veleno per il nostro organismo: diabete, obesit�, malattie cardiovascolari sono solo alcune delle conseguenze "
					+ "negative per la nostra salute. Per questioni educative e di abitudini alimentari, il problema sembra essere particolarmente "
					+ "sentito negli USA, col risultato che pi� di due terzi degli adulti e quasi un terzo dei bambini sono in sovrappeso se non obesi, "
					+ "e circa un terzo degli adulti ha problemi di pressione - condizioni che aumentano il rischio di patologie altrimenti prevenibili, "
					+ "come diabete, malattie cardiache, ictus e anche alcuni tumori.\r\n Per cercare di ridurre l'impatto sulla salute e sull'economia, "
					+ "il Dipartimento di igiene e salute mentale della citt� di New York ha promosso la National Salt and Sugar Reduction Initiative "
					+ "(NSSRI), che offre delle linee guida sui consumi di sale e zucchero indirizzate a consumatori e produttori, invitando questi "
					+ "ultimi ad aderire a regole di igiene alimentare su base volontaria.",dateArt2,"Salute","ChiaraGuzzonato",true, 0, 0);		
			mapArt.put(1, art2);

			// --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---

			String s3Date = "17-08-2021";
			Date dateArt3 = null;
			try {
				dateArt3 = new SimpleDateFormat("dd-MM-yyyy").parse(s3Date);
			} catch (ParseException e) {
				e.printStackTrace();
			}

			Article art3 = new Article("Sotto il segno di Venere", "Il 2021 di Palazzo Te � posto interamente sotto il segno di Venere. Con il progetto \"Venere Divina. "
					+ "Armonia sulla terra\" e i suoi tre eventi scalati nell'anno, il primo dei quali, \"Il mito di Venere a Palazzo Te\", "
					+ "si apre dal 21 marzo al 12 dicembre, la Fondazione Palazzo Te prosegue, nella sua indagine sul femminile (sacro, come nella "
					+ "mostra su Tiziano e Gerhard Richter, 2018-19, e ultraprofano, come nel progetto del 2019-20 sull'arte erotica di Giulio Romano) "
					+ "puntando sul mito della dea, la cui immagine compare ben 25 volte, in affreschi e stucchi, nel palazzo commissionato a Giulio "
					+ "Romano da Federico II Gonzaga per il proprio �ispasso�, dove il signore di Mantova teneva feste e banchetti ma, soprattutto, "
					+ "consumava i suoi amori con Isabella Boschetti.\r\n Il percorso � evidenziato in una guida cartacea e digitale ed � arricchito "
					+ "dalla statua di Venere velata, appartenuta proprio a Giulio Romano, e dall'arazzo di Venere nel giardino con putti, "
					+ "tessuto su disegno dello stesso Giulio, entrato di recente nelle collezioni di Palazzo Ducale grazie all'acquisto del "
					+ "Mibact con Fondazione Palazzo Te.\r\n"
					+ "Il palinsesto, curato da Francesca Cappelletti e Claudia Cieri Via, proseguir� dal 22 giugno al 5 settembre con Tiziano, "
					+ "Venere benda amore, capolavoro della stagione tarda del maestro, in arrivo dalla Galleria Borghese, mentre dal 12 settembre "
					+ "al 12 dicembre si terr� la mostra \"Venere. Natura, ombra e bellezza\", ispirata dalle tavole di \"Mnemosyne di Warbur\", "
					+ "che attraverso esempi di statuaria antica e opere di Cranach, Guido Reni, Tiziano, Dosso Dossi, Rubens e altri, giunte dai "
					+ "maggiori musei europei, indagher� il mito di Venere, cos� caro alle corti europee del Cinquecento e Seicento, nelle sue "
					+ "declinazioni di forza generatrice della natura ma anche di protettrice di maghe e streghe, simbolo dei pericoli dell'amore.",
					dateArt3,"Arte","AdaMasoero",false, 0, 0);
			mapArt.put(2, art3);

			// --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---

			String s4Date = "01-03-2021";
			Date dateArt4 = null;
			try {
				dateArt4 = new SimpleDateFormat("dd-MM-yyyy").parse(s4Date);
			} catch (ParseException e) {
				e.printStackTrace();
			}

			Article art4 = new Article("Dove si vive meglio in Italia? Ecco la Classifica Ufficiale 2021!", "Qual � la citt� migliore in cui vivere in Italia? "
					+ "Beh, noi lo abbiamo chiesto a voi... attraverso un'indagine che ha messo a confronto diversi parametri: Servizi/Ambiente/Salute, "
					+ "Popolazione, Ordine Pubblico, Tempo Libero e Svago, Tenore di Vita, Affari e Lavoro. Abbiamo inviato un sondaggio ad un campione di "
					+ "20.000 utenti della community di Travel365, invitandoli ad esprimere le proprie preferenze, attribuendo un voto in decimi per ognuna "
					+ "delle 6 aree tematiche.\r\nCreando una media dei risultati � poi emersa una top 10 delle citt� pi� vivibili del Bel Paese, all'interno "
					+ "della quale tristemente non figura nessuna citt� del sud Italia... Scopriamo quali sono, secondo voi, le citt� migliori dove vivere!"
					+ "\nAl decimo posto troviamo Roma, al nono Firenze, all'ottavo Reggio Emilia, al settimo Brescia, al sesto Trento, al quinto Parma e "
					+ "al quarto Mantova. Sul gradino pi� basso de podio troviamo Torino, al secondo posto Milano e al primo posto, con il punteggio di 58.5/60, Bologna!",
					dateArt4,"Italia","Travel365",true, 0, 0);
			mapArt.put(3, art4);

			// --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---

			String s5Date = "27-09-2021";
			Date dateArt5 = null;
			try {
				dateArt5 = new SimpleDateFormat("dd-MM-yyyy").parse(s5Date);
			} catch (ParseException e) {
				e.printStackTrace();
			}

			Article art5 = new Article("La prima volta della Scala: uno spot per la campagna abbonamenti: \"C'� posto per tutti\"", 
					"Segno dei tempi: anche la Scala lancia la propria campagna abbonamenti con uno spot, il primo in assoluto "
							+ "della secolare storia del teatro. Quindici secondi che puntano proprio sulla tradizione. Una serie di persone "
							+ "di varie et� viene inquadrata mentre prende posto tra gli stucchi e i damaschi della sala, guardandosi intorno "
							+ "con ammirazione. E di sottofondo una voce: \"C'� chi � abbonato da vent'anni e chi viene per la prima volta, chi "
							+ "porta tutta la famiglia e chi la scopre prima dei trent'anni. Sotto il cielo della Scala c'� posto per tutti\". "
							+ "Il tutto in onda da oggi sulla Rai, presto anche ai cinema e sui social network.\r\n" + "\r\n" 
							+ "Un cambiamento netto di strategia di comunicazione per un posto che finora di farsi pubblicit� non aveva "
							+ "mai avuto eccessivo bisogno, al limite giusto la comunicazione degli spettacoli da venire. Ma appunto i tempi "
							+ "cambiano, il Covid ha colpito duro anche al teatro lirico pi� famoso del mondo che ha ampliato per la prima volta "
							+ "la sua strategia di comunicazione alla pubblicit� televisiva. Erano solo andati in onda spot che pubblicizzavano "
							+ "le messe in onda sulla tv di Stato delle Prime del 7 dicembre. Questo � il primo spot scaligero di marca Scala e, "
							+ "se la ripartenza degli spettacoli sar� ancora lenta, facile che non sia l'ultimo.",
							dateArt5,"Spettacolo","Luigi Bolognini",true, 0, 0);
			mapArt.put(4, art5);

			// --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---

			String s6Date = "05-10-2021";
			Date dateArt6 = null;
			try {
				dateArt6 = new SimpleDateFormat("dd-MM-yyyy").parse(s6Date);
			} catch (ParseException e) {
				e.printStackTrace();
			}

			Article art6 = new Article("Infortunio Pessina: � lesione al flessore: l'Atalanta spera di recuperarlo per dicembre", 
					"Lesione di secondo grado al flessore della coscia destra: � questo quanto emerge dagli esami ai quali si � "
							+ "sottoposto Matteo Pessina dopo l�infortunio rimediato nell�ultima gara di campionato giocata (e persa "
							+ "dall�Atalanta) al Gewiss Stadium contro il Milan; un problema che lo ha costretto a lasciare il campo in barella "
							+ "dopo 24 minuti di gioco. Una brutta tegola per Gasperini, che dovr� rinunciare a una delle pedine cardine del "
							+ "suo gioco per diverse settimane: in casa Atalanta, infatti, sperano di riavere nuovamente a disposizione il "
							+ "calciatore per dicembre. Pessina che, ricordiamo, � stato costretto a rinunciare alla chiamata del Ct Roberto "
							+ "Mancini che lo aveva convocato per gli impegni degli azzurri in Nations League.\r\n"
							+ "Continua dunque il momento sfortunato per l�Atalanta per quanto riguarda gli infortuni: il problema di Pessina, "
							+ "infatti, arriva pochi giorni dopo quello di Gosens (stop di almeno due mesi), altro pilastro nerazzurro. Prima "
							+ "ancora Gasperini � stato costretto a rinunciare ad Hateboer, a Zapata a inizio campionato e poi a Muriel.",
							dateArt6,"Sport","SkySport",false, 0, 0);
			art6.setSubCategory("Calcio");
			mapArt.put(5, art6);

			// --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---

			String s7Date = "02-10-2021";
			Date dateArt7 = null;
			try {
				dateArt7 = new SimpleDateFormat("dd-MM-yyyy").parse(s7Date);
			} catch (ParseException e) {
				e.printStackTrace();
			}

			Article art7 = new Article("Netflix da oggi si paga di pi�: ecco gli aumenti", 
					"La notizia � arrivata un po� a sorpresa: dalla giornata di oggi, 2 ottobre, aumentano i costi degli abbonamenti "
							+ "Standard e Premium di Netflix. L�incremento del prezzo, ed � questo il motivo principale del malumore degli utenti, "
							+ "� previsto anche per chi � gi� iscritto da tempo alla piattaforma streaming. Ma vediamo nel dettaglio quali sono i "
							+ "cambiamenti pi� rilevanti. Il piano Base resta invariato, a 7,99 euro al mese, mentre quello Standard passa dagli 11,99 "
							+ "euro ai 12,99 euro mensili, con un incremento di un euro. Ancora pi� consistente l�aumento del piano Premium. "
							+ "Gli abbonati, invece di 15,99 euro al mese, dovranno pagare 17,99 euro, ossia 2 euro in pi� ogni trenta giorni.\r\n" 
							+ "In ogni caso, la data del 2 ottobre per le tariffe aggiornate riguarda i nuovi iscritti, mentre i vecchi abbonati "
							+ "riceveranno una notifica email dal prossimo 9 ottobre che annuncer� l�incremento del prezzo a partire dai trenta "
							+ "giorni successivi. Una doccia gelata per chi guarda Netflix su due dispositivi in qualit� Hd e per chi � abilitato "
							+ "su quattro dispositivi in qualit� 4K. La politica adottata da Netflix ha disorientato il mercato, dato che le piattaforme "
							+ "concorrenti mantengono prezzi di abbonamento contenuti, anche se � riconosciuta da tutti la qualit� garantita dalla societ� "
							+ "statunitense. A fare la differenza sono soprattutto i contenuti proposti, tutti di un livello medio alto.\r\n" 
							+ "Attraverso una nota, Netflix ha giustificato in questo modo l�aumento dei prezzi degli abbonamenti Standard e Premium. "
							+ "�Il nostro obiettivo principale � hanno scritto i responsabili della piattaforma � � offrire un'esperienza di "
							+ "intrattenimento che superi le aspettative dei nostri abbonati. Stiamo aggiornando i nostri prezzi per riflettere i "
							+ "miglioramenti che apportiamo al nostro catalogo di film e di show e alla qualit� del nostro servizio e, cosa ancora "
							+ "pi� importante, per continuare a dare pi� opzioni e portare valore ai nostri abbonati. Proponiamo diversi piani di "
							+ "abbonamento affinch� ciascuno possa trovare l'opzione pi� soddisfacente per le proprie esigenze�.",
							dateArt7,"Tecnologia","Ignazio Riccio",true, 0, 0);
			mapArt.put(6, art7);

			// --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---

			String s8Date = "27-09-2021";
			Date dateArt8 = null;
			try {
				dateArt8 = new SimpleDateFormat("dd-MM-yyyy").parse(s8Date);
			} catch (ParseException e) {
				e.printStackTrace();
			}

			Article art8 = new Article("Messaggi spiati e a rischio? Altra bufera privacy per WhatsApp", 
					"La questione privacy � sempre un tema che tiene banco quando si parla di WhatsApp. Il rapporto con Facebook ha "
							+ "reso senz'altro pi� complicata la gestione dei dati personali e dei contenuti privati degli utenti. A sottolinearlo "
							+ "arriva un ulteriore capitolo della vicenda, che in questo caso ruota attorno alla questione codifica dei messaggi e "
							+ "degli allegati.\r\nAndiamo con ordine, a cominciare dalla questione legata alla crittografia dei messaggi inviati "
							+ "tra gli utenti. Forse non tutti sanno che WhatsApp assicura s� la crittografia \"end-to-end\" dei messaggi, ovvero "
							+ "solo mittente e ricevente possono conoscerne il contenuto, all'interno delle conversazioni, ma lo stesso non vale quando "
							+ "si tratta di salvataggio sui vari cloud.\r\nNel momento in cui avviene un backup su Google Drive (Android) o su iCloud "
							+ "(iOS Apple), tale codifica non avviene. Almeno finora, perch� lo stesso Mark Zuckerberg ha dichirato che � in arrivo un "
							+ "secondo livello di codifica, che riguarder� proprio questo ambito e si aggiunger� a quella dei messaggi in conversazione."
							+ "Si potrebbe pensare che la questione privacy relativa a WhatsApp possa definirsi risolta. Pare di no, almeno stando a quanto "
							+ "pubblicato su ProPublica.org. Come affermato dal sito indipendente no profit, circa mille dipendenti dell'azienda "
							+ "utilizzerebbero un software di Facebook per verificare i contenuti foto e video condivisi dagli utenti.\r\n" 
							+ "Per ciascun contenuto che appare sul loro schermo, tali addetti emetterebbero un giudizio relativo a eventuali violazioni "
							+ "in termini di violenza, pedopornografia, frode o spam, persino possibili complotti terroristici. Il tutto mediamente in "
							+ "meno di un minuto per ciascuno contenuto visionato.\r\nAd ogni modo, aggiunge il sito, tali contenuti verrebbero "
							+ "immessi in tale flusso unicamente se segnalati dagli utenti che vi hanno avuto accesso. Curioso appare per� un commento "
							+ "fatto dal direttore delle comunicazioni di WhatsApp, Carl Woog, secondo il quale tale pratica non verrebbe considerata "
							+ "dalla compagnia come \"moderazione dei contenuti\". Questo malgrado i vertici Facebook abbiano apertamente dichiarato che "
							+ "15mila addetti alle verifiche valutino ogni giorno i contenuti pubblicati sulla piattaforma madre o su Instagram.",
							dateArt8,"Tecnologia","Claudio Schirru",true, 0, 0);
			mapArt.put(7, art8);

			// --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---

			String s9Date = "05-03-2019";
			Date dateArt9 = null;
			try {
				dateArt9 = new SimpleDateFormat("dd-MM-yyyy").parse(s9Date);
			} catch (ParseException e) {
				e.printStackTrace();
			}

			Article art9 = new Article("I 150 anni della tavola periodica degli elementi. \"E' l'alfabeto per capire la materia\"", 
					"Era il 6 marzo del 1869 quando il chimico russo Dmitri Mendeleev pubblicava la classificazione di tutti gli elementi "
							+ "conosciuti sulla base del loro peso atomico, il sistema che avrebbe portato alla Tavola periodica degli elementi usata oggi. "
							+ "All'inizio contava 63 elementi, oggi dopo 150 anni � arrivata a ben 118.\r\n" 
							+ "\"E' l'alfabeto che ci permette di parlare con il mondo intorno a noi e che in futuro ci permetter� di costruire farmaci, "
							+ "materiali e metalli nuovi\", come prevede il chimico dell'Istituto Italiano di Tecnologia (Iit), Marco De Vivo."
							+ "La storia della tavola periodica ha in realt� radici ben pi� profonde. A intuire che ci fossero degli elementi fisici, "
							+ "come il platino, il mercurio e lo zinco, erano stati gi� alcuni filosofi dell'antichit�, come Aristotele, secondo cui la "
							+ "materia era il frutto della combinazione di una o pi� 'radici', chiamate poi elementi da Platone. Molto tempo dopo, nel 1869, "
							+ "Mendeleev organizz� gli elementi secondo la loro massa atomica, giocando quello che alcuni hanno definito un 'solitario chimico', "
							+ "dove le carte erano le informazioni note sui vari elementi.\r\n"
							+ "Per De Vivo, la tavola periodica \"� stato un passo fondamentale che ha permesso di comprendere gli elementi, le loro propriet� e "
							+ "differenze. E' come se avessimo capito l'alfabeto per parlare con il mondo intorno a noi\". Grazie ai progressi compiuti negli anni "
							+ "\"oggi siamo sempre pi� bravi a costruire oggetti nuovi, anche rispetto a quelli presenti in natura - conclude - In futuro sapremo "
							+ "combinare gli elementi sempre meglio, ottenendo molecole pi� intelligenti o materiali che oggi non possiamo immaginare. Basti pensare "
							+ "che dal carbonio � stato ottenuto il grafene\".",
							dateArt9,"Scienza","Elena Dusi",false, 0, 0);
			mapArt.put(8, art9);

			// --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---

			String s10Date = "17-06-2021";
			Date dateArt10 = null;
			try {
				dateArt10 = new SimpleDateFormat("dd-MM-yyyy").parse(s10Date);
			} catch (ParseException e) {
				e.printStackTrace();
			}

			Article art10 = new Article("Il sondaggio che ribalta Roma. Batosta per Raggi e Pd", 
					"Mentre il centrosinistra � ancora alle prese con le primarie per trovare il candidato sindaco, Enrico Michetti "
							+ "ha gi� iniziato la campagna elettorale nella Capitale. Il candidato del centrodestra � molto attivo sul territorio "
							+ "e il suo impegno sta dando i primi frutti nei sondaggi, dove � saldamente in testa nelle preferenze dei romani. "
							+ "Nelle intenzioni di voto rilevate dal sondaggio di BiDiMedia per RomaToday, Enrico Michetti gode del 28,5% dei consensi. "
							+ "Il sogno del professore � quello di riportare Roma a essere Caput mundi agendo dal basso, risolvendo i problemi che "
							+ "l'hanno portata al decadimento odierno.\r\nIl candidato del centrosinistra, che ancora dev'essere individuato, � fermo al "
							+ "27%: un consenso sulla fiducia per il momento da parte dei romani, in attesa di capire chi sar� a sfidare Michetti per "
							+ "la corsa al Campidoglio. Molto indietro Virginia Raggi, che paga i 5 anni di mandato e si ferma al 21,4 mentre Carlo "
							+ "Calenda al momento ha il 14,9% delle preferenze. Enrico Michetti � in testa anche nei sondaggi previsionali, con il 23% "
							+ "dei romani che lo d� come nuovo sindaco.\r\nNelle intenzioni di voto in riferimento ai partiti, � Fratelli d'Italia quello "
							+ "che raccoglie maggiori consensi all'interno della coalizione di centrodestra, con il 19% delle preferenze, seguito dalla "
							+ "Lega al 7,5% e da Forza Italia al 4%. In totale la coalizione del centrodestra gode del 30,5%, pi� di un punto percentuale "
							+ "in pi� rispetto alla coalizione di centrosinistra che non supera il 29,2%. Il Movimento 5 Stelle corre da solo e guadagna "
							+ "il 19,2% dei consensi ma il 66% degli elettori intervistati ha un giudizio negativo sull'attuale sindaco e ben il 43% ha "
							+ "espresso un giudizio molto negativo sulla Raggi.",
							dateArt10,"Politica","Francesca Galici",false, 0, 0);
			mapArt.put(9, art10);

			// --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---

			String s11Date = "04-10-2021";
			Date dateArt11 = null;
			try {
				dateArt11 = new SimpleDateFormat("dd-MM-yyyy").parse(s11Date);
			} catch (ParseException e) {
				e.printStackTrace();
			}

			Article art11 = new Article("\"L'Imu aumenta del 174%\". Scatta l'allarme sul catasto", 
					"Genera sempre pi� malumore la riforma sul catasto al quale sta lavorando il governo Draghi: un simile provvedimento, "
							+ "oltre a comportare un aumento sui costi della casa, rischia di tramutarsi nell'ennesima stangata per il ceto medio "
							+ "e medio-basso. A lanciare l'allarme adesso � anche Federcontribuenti, che spiega come la riforma potrebbe addirittura "
							+ "portare ad un crollo del mercato immobiliare a causa dell'aumento dei costi. L'esecutivo, dunque, deve pensare bene "
							+ "alle proprie mosse.\r\nFedercontribuenti riporta uno studio recente, in cui viene evidenziato che attualmente ci sono "
							+ "4,5 milioni di immobili sconosciuti alla macchina del Fisco. Al ministero dell'Economia e delle Finanze risultano 57"
							+ " milioni di unit� immobiliari appartenenti a persone fisiche in Italia. 19,5 milioni risultano essere abitazioni "
							+ "principali, mentre 13,3 milioni rappresentano le relative pertinenze. A questi dati si aggiungono 6 milioni di case in "
							+ "affitto e 1,2 milioni immobili concessi in uso gratuito a familiari o comproprietari. Risultano poi 6,3 milioni di "
							+ "seconde case, di cui il 55% non viene riportato nelle dichiarazioni dei redditi. \"Se andasse in porto questa riforma "
							+ "del catasto, il mercato immobiliare nelle grandi citt� crollerebbe\", � la conclusione di Federcontribuenti all'interno "
							+ "della nota, riportata da LaPresse. Per fare un esempio, \"a Milano si calcola che l'Imu aumenterebbe del 174,2% vale "
							+ "a dire che il milanese medio che paga 2mila euro di Imu ogni anno dovrebbe sborsare 3.484 con la riforma del catasto. "
							+ "Cos� in tutte le grandi citt� italiane. La riforma del catasto non deve essere lo scudo per nascondere l'incapacit� del "
							+ "governo di scovare gli evasori fiscali\".\r\nDel resto, come ricorda Federcontribuenti, l'Imu � la tassa pi� evasa del "
							+ "nostro Paese. Ben il 25,8% dell'imposta, dati del ministero dell'Economia e delle Finanze alla mano, \"non viene versata "
							+ "per un ammanco di 4 miliardi e 869 milioni di euro nel triennio 2015-2017 (ultimi dati aggiornati) il tax gap complessivo "
							+ "� stato di 107,2 miliardi di euro: 95,9 miliardi di mancate entrate tributarie e 11,3 miliardi di mancate entrate contributive\".",
							dateArt11,"Business","Federico Garau",false, 0, 0);
			mapArt.put(10, art11);

			// --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---

			String s12Date = "04-10-2021";
			Date dateArt12 = null;
			try {
				dateArt12 = new SimpleDateFormat("dd-MM-yyyy").parse(s12Date);
			} catch (ParseException e) {
				e.printStackTrace();
			}

			Article art12 = new Article("La strage dei ragazzini. Morire a 15 anni per la propria passione", 
					"Ci hanno raccontato Plauto e tanti altri che muor giovane colui che al cielo � caro. Ma veder cadere centauri bambini "
							+ "come fossero in guerra, e non � guerra solo una gara, non ci fa credere che questo sia il progetto. Ieri � toccato, "
							+ "sta diventando un conteggio come fosse una ineluttabilit�, a Dean Berta Vi�ales, ragazzino spagnolo di 15 anni, dal "
							+ "cognome famoso perch� lo porta anche Maverick, il cugino che corre in MotoGp sull'Aprilia. Berta era nato il 20 aprile "
							+ "2006 a Palau Saverdera, comune autonomo della Catalogna con meno di 100 abitanti, ed era ancora un novello del mondiale "
							+ "che stava correndo: 11 gare soltanto. Stavolta si provava sul circuito di Jerez de la Frontera, Yamaha numero 25 "
							+ "(il vecchio numero del cugino) nella gara 1 della Supersport 300, anticamera della Superbike. Decimo giro, Dean Berta "
							+ "vola via dalla moto, gli altri arrivano: gli passano sopra.\r\n" + 
							"\r\n" + "La gara viene immediatamente fermata. Nel groviglio sono rimasti coinvolti altri tre piloti fra i 15 e i 17 anni. "
							+ "Ma le condizioni di Vi�ales sono apparse subito gravi: lesioni al torace e alla testa. Il pilota assistito dai "
							+ "medici in pista, ma c'� voluto poco per capire che se n'era andato. Era in un bel momento di forma, raccontano "
							+ "i suiveurs del moto mondiale: a Magny Cours aveva conquistato un secondo posto, sul circuito di Barcelona-Catalunya "
							+ "una sesta posizione. Aveva la faccia di un bambino, � stato ucciso come ormai, troppo spesso, capita nelle gare "
							+ "motociclistiche. Sei davanti, sei nel gruppo, cadi, gli altri non possono evitarti e addio. Quest'anno � il terzo "
							+ "centauro bambino che se ne va: quattro mesi fa, al Mugello, � capitato a Jason Dupasquier, 19 anni, centauro "
							+ "svizzero travolto da due piloti al termine delle qualifiche del Gran Premio d'Italia. Ammazzato da lesioni al "
							+ "torace e da lesioni cerebrali. Il 25 luglio, durante il Motorland Aragon, � toccato a Hugo Millian, pure lui "
							+ "spagnolo, 14 anni appena, caduto a 13 giri dalla fine ma pronto a cercar la salvezza, se non lo avesse investito "
							+ "il polacco Oleg Pawelec mentre si rialzava per fuggire dal mezzo della pista. Lo chiamavano Iceman per la "
							+ "freddezza in sella. Una freddezza che stava per salvarlo. La tragica lista di piloti caduti cos� ormai � disumanamente "
							+ "lunga: limitandosi al motociclismo moderno la si fa partire dal 1973, gran premio d'Italia a Monza, dove lasciano la "
							+ "vita, il loro mondo, Jarno Sarinen e Renzo Pasolini. Poi una serie di nomi famosi o meno, giovanissimi oppure pi� "
							+ "celebrati. Fa ancora male, dieci anni dopo, il ricordo di Marco Simoncelli, a terra svenuto, il casco volato via."
							+ "Si dir� che la vita e la morte sono leggi della vita, ed anche dello sport. Ma lo sport non vuol celebrare la morte. "
							+ "Oppure � vero che lo sport, nel giro di poche ore, ci invita a celebrare il primo gol di un 19enne rampollo (Maldini) "
							+ "di nobile casata calcistica e la morte di un centauro-bambino dal cognome e dal cugino famoso. La morte non guarda mai "
							+ "il calendario.",
							dateArt12,"Sport","Riccardo Signorini",true, 0, 0);
			art12.setSubCategory("Motori");
			mapArt.put(11, art12);

			// --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---

			String s13Date = "12-06-2021";
			Date dateArt13 = null;
			try {
				dateArt13 = new SimpleDateFormat("dd-MM-yyyy").parse(s13Date);
			} catch (ParseException e) {
				e.printStackTrace();
			}

			Article art13 = new Article("Bologna spoglia Armani. La Virtus vent'anni dopo torna regina del basket", 
					"Sprofondo rosso e Virtus nel paradiso che mancava da 20 anni. Lo scudetto del basket va alla Segafredo, "
							+ "il trionfo � per Sasha Djordjevic l'argonauta cacciato per 24 ore a dicembre dall'incompetenza, la notte "
							+ "magica � tutta per Alessandro Pajola, un ragazzo di Ancona che ha vinto anche il premio come miglior giovane "
							+ "della stagione.\r\nPer l'Armani sfinita alla 91� partita dell'anno un cappotto fuori stagione che punge la pelle "
							+ "di campioni navigati, mai loro stessi nella volata finale dove sembravano pi� che altro fantasmi. Il meglio lo avevano "
							+ "lasciato nella semifinale di Eurolega a Colonia persa col Barcellona. Per la Virtus percorso netto anche se per arrivare "
							+ "al 73-62 della quarta partita, quello che ha permesso di mettere la bandierina sulla montagna scudetto, c'� voluta "
							+ "pazienza, ma, soprattutto, � diventato importante il timoniere della barca tricolore del Zanetti re del caff� che ora "
							+ "amer� i cestisti come i suoi ciclisti. Djordjevic ha letto nella mente di ragazzi che non vedevano l'ora e, in questi "
							+ "casi, capita di esagerare perdendo troppi palloni. Un'Armani con l'anima, ma non � bastato, cos� come � stato tardivo"
							+ " il pentimento di utilizzare davvero la panchina, cominciando da Cinciarini cancellato troppe volte o, magari il "
							+ "Biligha generoso al momento della resa.\r\nMilano � la sua anemia nei finali di partita, anche ieri 19 punti "
							+ "nei 20 minuti conclusivi, 8 soltanto nel terzo quarto, percentuali orribili al tiro cominciando da Rodriguez "
							+ "e quella che era la lama sottile per trafiggere difese ariose � diventata l'arma per il suicidio contro la difesa "
							+ "tosta, ben organizzata della Virtus. Ieri pi� Belinelli del solito mago Teodosic, ma hanno lasciato un segno anche "
							+ "Alibegovic, altro giovanissimo, Ricci nel finale, dopo una buona difesa, Wheems con i suoi colpi a sorpresa, Hunter "
							+ "e Gamble per rendere irrespirabile la vita dentro l'area senza il sostegno del Tessitori infortunato che sarebbe stato "
							+ "utilissimo. L'uomo della serata, per�, � stato un ragazzino dagli occhi vivaci, 8 punti, 5 rimbalzi, 4 assist, tanti "
							+ "palloni rubati o sporcati. Lui il motivo per cui si deve credere in una scuola italiana, quella del Consolini che lo "
							+ "ha svezzato, quella del Djordjevic che gli ha dato fiducia. Il capolavoro � di questo serbo che con la nazionale ha "
							+ "vinto tanto, come giocatore e allenatore, di questo poeta del rischio che ha casa a Milano in zona Brera e nella finale "
							+ "ha incatenato il sistema di Messina, un gioco che non ha funzionato contro una difesa costruita bene.\r\nEttore Messina "
							+ "lascia da signore l'arena dopo aver abbracciato il rivale, sapendo cosa si prova a Bologna in momenti come quelli di ieri. "
							+ "L'ultimo scudetto lo aveva dato proprio lui battendo la Fortitudo vent'anni fa, suo il filotto senza sconfitte come quello "
							+ "di questa Virtus che ha eguagliato anche il 2010 di Siena.\r\nLa Virtus ha rubato a Milano le sue trappole, mettendoci "
							+ "qualcosa in pi� con i dioscuri Teodosic e Belinelli, costruendo quello che Milano non � riuscita a fare in una stagione "
							+ "di gloria dove, per�, alla fine, ha scoperto, di dover cambiare molto se vuole restare al vertice europeo. L'usato sicuro "
							+ "non basta.",
							dateArt13,"Sport","Oscar Eleni",false, 0, 0);
			art13.setSubCategory("Basket");
			mapArt.put(12, art13);

			// --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---

			String s14Date = "12-06-2021";
			Date dateArt14 = null;
			try {
				dateArt14 = new SimpleDateFormat("dd-MM-yyyy").parse(s14Date);
			} catch (ParseException e) {
				e.printStackTrace();
			}

			Article art14 = new Article("Kabul, bomba in moschea. I sospetti cadono su Isis-K", 
					"Una forte esplosione all'ingresso della moschea Eid Gah a Kabul, seguita da due o tre minuti di conflitto a fuoco. "
							+ "Nell'edificio religioso si stavano svolgendo i funerali della madre del portavoce dei talebani Zabihullah Mujahid. "
							+ "� il primo grande attacco terroristico nella capitale afghana da quando le forze statunitensi si sono ritirate ad agosto. "
							+ "Secondo fonti dei talebani, citate dall'agenzia Sputnik, ci sarebbero 12 vittime, mentre per la tv afghana Ariana i morti"
							+ " sarebbero meno di una decina, i feriti invece 32. Mujahid - che ha dato la notizia - e altri membri della leadership "
							+ "talebana all'interno della moschea non sono stati feriti. Le vittime erano civili fuori dal cancello. Emergency, che gestisce "
							+ "un ospedale a Kabul, nel quartiere di Shahr-e-Naw, ha confermato su Twitter che ha in cura quattro persone ferite "
							+ "nell'esplosione.\r\nNonostante non vi sia stata alcuna rivendicazione immediata, lo Stato islamico della provincia del "
							+ "Khorasan, conosciuto anche come Isis-K, � l'unico gruppo jihadista di una certa rilevanza che opera in Afghanistan da quando "
							+ "i talebani hanno preso il potere il 15 agosto. L'Isis-K ha effettuato una serie di attacchi contro i talebani a Jalalabad "
							+ "nelle ultime settimane e ad agosto ha ucciso 13 militari statunitensi e circa 200 afgani all'ingresso dell'aeroporto di Kabul."
							+ "Questo attacco � importante perch� ha sottolineato le serie sfide alla sicurezza che i talebani dovranno affrontare. L'attuale "
							+ "leadership afghana infatti ha meno risorse dell'ex amministrazione appoggiata dagli Stati Uniti per monitorare le minacce "
							+ "terroristiche e nessun aiuto da parte dell'intelligence statunitense. I talebani sono cos� ancora pi� vulnerabili alle "
							+ "aggressioni dell'Isis. Il gruppo militante � stato responsabile anche di alcuni degli attacchi pi� mortali contro i civili "
							+ "afgani a Kabul negli ultimi anni, in particolare contro la minoranza sciita hazara. Nel frattempo la spirale di vendetta "
							+ "non si ferma. Tre cadaveri sono stati esposti dai talebani nella provincia del Nangarhar, nell'Afghanistan orientale. Un "
							+ "loro portavoce ha affermato che non � chiaro chi ci sia dietro gli omicidi, mentre fonti locali hanno sostenuto che si tratti "
							+ "di tre sospetti affiliati ad Isis-K, giustiziati dai talebani.",
							dateArt14,"Mondo","Chiara Clausi",false, 0, 0);
			mapArt.put(13, art14);

			// --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---

			String s15Date = "01-10-2021";
			Date dateArt15 = null;
			try {
				dateArt15 = new SimpleDateFormat("dd-MM-yyyy").parse(s15Date);
			} catch (ParseException e) {
				e.printStackTrace();
			}

			Article art15 = new Article("Le dieci tecnologie per l'auto del futuro", 
					"Una vera e propria rivoluzione travolger� la mobilit� nei prossimi anni, dalla guida autonoma all'idrogeno. "
							+ "Ecco come e cosa cambier� a bordo.\n\n� diventato un mantra che si sente sempre pi� spesso: cambier� tutto nel mondo "
							+ "nell�automobile nei prossimi anni. Eppure diverse delle tecnologie pi� rilevanti non le vedremo a bordo delle vetture ma "
							+ "attorno. Un esempio: le telecamere istallate sui modelli che hanno un certo grado di automazione, da quella impiegata nei "
							+ "sistemi pi� semplici per il parcheggio assistito fino alle otto montate su ogni Tesla, produrranno entro il 2025 pi� video "
							+ "di qualsiasi altra fonte superando piattaforme online come YouTube o Facebook.\r\n" 
							+ "Immaginate cosa si potr� fare, nel bene e nel male, analizzando questa montagna di informazioni. Immaginate anche, usando "
							+ "come metro di paragone quel che � accaduto sui nostri smartphone, quanti nuovi servizi potranno essere forniti scandagliando "
							+ "il come, dove e quando guidiamo, e quel che vede la vettura mentre � in funzione. I servizi cloud come quelli forniti dalla "
							+ "californiana Renovo, basati sull�intelligenza artificiale, sono solo un assaggio.\r\n" + 
							"Un altro pezzo di futuro lo potete invece trovare in tutte quelle sperimentazioni dove non si analizza solo l�ambiente circostante "
							+ "alla macchina ma anche cosa accade al suo interno: interpretazione dei gesti del guidatore, delle espressioni, del livello di "
							+ "attenzione come fa la Cerence. Altri lavorano sui cristalli a realt� aumentata per avere parabrezza che in sovraimpressione "
							+ "forniscano tutte le informazioni necessarie su direzione da prendere, condizioni del veicolo, meteorologiche e del percorso per "
							+ "non staccare gli occhi dalla strada. E poi la mobilit� di nuova generazione con app per il trasporto su bici elettriche e "
							+ "monopattini che via via stanno aggiungendo nuovi mezzi, dai motorini alle vetture, oltre al noleggio a lungo termine. Lo schema "
							+ "� abbastanza chiaro: permetteranno di andare dal punto A al punto B con il mezzo pi� adatto e senza mai doverlo possedere. Sullo "
							+ "sfondo la velocit� delle nuove reti di telecomunicazione, il 5G, forme sempre pi� evolute di guida autonoma, fonti di energia "
							+ "alternative agli idrocarburi come l�elettrico e l�idrogeno. Insomma, quel che sostiene il mantra � vero: con buona probabilit� "
							+ "cambier� tutto nel mondo nell�automobile. Ecco come.\r\n" + "Quel che l�automobile sapr� di te.\r\n" + 
							"� gi� successo con il Web: attorno al 2009, con l�introduzione dei primi filtri nei motori di ricerca capaci di capire chi c�era "
							+ "dall�altra parte della tastiera, la Rete ha cominciato a sondare le nostre abitudini co servizi e pubblicit� su misura. Qualcosa"
							+ " del genere sta per accadere anche nel campo dell�automobile grazie a veicoli sempre pi� connessi e capaci di immagazzinare dati "
							+ "riguardo a stili di guida, luoghi frequentati, consumi e comportamenti anche dentro l�abitacolo.\r\n" + "Come uno smartphone.\r\n" + 
							"Avremo aggiornamenti continui del software e delle vetture per ottimizzarle secondo l�uso che ne facciamo, ma anche per fornire "
							+ "soluzioni e servizi su misura.\r\n" + "La guida (quasi) autonoma.\r\n" + 
							"Gli advanced driver assistance systems (Adas) sono tutti quei sistemi che aiutano a parcheggiare e condurre la vettura. Per esteso "
							+ "si va dalle tecnologie  dell�antibloccaggio dei freni (Abs) alla guida autonoma di livello 5 dove nel veicolo condotto "
							+ "dall�intelligenza artificiale � stato tolto volante e pedaliera. Dimenticatevi per� di vedere auto simili nei centri urbani nel "
							+ "prossimo futuro. Non � solo una questione normativa ma di costi: un piccolo bus da otto posti in grado di procedere senza autista, "
							+ "parte dai 350 mila euro. Ecco perch� vedremo al contrario un graduale aumento delle funzioni automatizzate di serie come quella "
							+ "per il parcheggio, il seguire la carreggiata o l�adeguare l�andatura in autostrada al veicolo che ci precede.\r\n" + "L�idrogeno.\r\n" + 
							"L�infrastruttura, ovvero i distributori, � il principale ostacolo per la diffusione delle auto a zero emissioni basata sull�idrogeno. "
							+ "Motori del genere li hanno sperimentati in tanti, ma � la Toyota, con la Mirai, la prima ad averli messi sul mercato. � stata "
							+ "seguita dalla Nexo della Hyundai e dalla Clarity Fuel Cell di Honda. Il rifornimento richiede, a differenza delle auto elettriche, "
							+ "tre minuti. Troppo rare per� le stazioni in Italia, almeno fino ad ora.\r\n" + "L�elettrico.\r\n" + 
							"Pi� versatile dell�idrogeno, ma ha ancora grossi limiti nell�autonomia e nei tempi di ricarica. Si sta per� lavorando a batterie "
							+ "di nuova generazione, cos� come a colonnine che permettano una ricarica in tempi pi� brevi.\r\n" + "Le meraviglie (future) del 5G.\r\n" + 
							"Le reti per le telecomunicazioni di quinta generazione promettono prestazioni mirabolanti. Soprattutto consentiranno di avere un "
							+ "segnale stabile dieci volte pi� veloce del 4G anche quando si viaggia ad oltre 130 chilometri orari. E con una latenza, il tempo "
							+ "di risposta, infinitesimale. La prospettiva: veicoli che possono esser condotti da remoto con una precisione millimetrica, servizi "
							+ "streaming ad alta risoluzione anche quando si viaggia in autostrada, grandi quantit� di dati inviate dalle macchine in circolazione "
							+ "per una gestione della viabilit� in tempo reale. Fin qui il domani. L�oggi vede una copertura del 5G, non solo in Italia, ancora "
							+ "bassa e una velocit� che � �solo� due volte e mezzo quella del 4G.\r\n" + "La fine della propriet�.\r\n" + 
							"In Olanda la startup Swapfiets, che sta per sbarcare a Milano, via app affitta bici a lungo termine. Se si fora o se il mezzo "
							+ "viene rubato, entro 12 ore arriva una bici nuova o quella vecchia viene riparata. Si parte da 17 euro al mese per il modello base "
							+ "per arrivare ai 60 circa per quello elettrico. E sta gi� sperimentando i motorini elettrici a Berlino. Lime offre ora abbonamenti "
							+ "mensili per monopattini e bici e pensa anche lei ai motorini e auto elettriche. Uber in alcune citt� include il trasporto pubblico, "
							+ "oltre ai taxi e ai mezzi di Lime. ShareNow di Daimler e Bmw ormai consente di affittare l�auto non solo per una singola corsa ma "
							+ "per una giornata, una settimana o un mese. Nella guerra scoppiata fra le app per la mobilit� l�obiettivo � comune: far tramontare"
							+ " la propriet� affermando il modello preso dallo streaming.\r\n" + "La vista aumentata del parabrezza.\r\n" + 
							"La Apple lo scorso mese ha depositato un brevetto per un parabrezza che pu� visualizzare ologrammi con le indicazioni stradali, "
							+ "velocit� di crociera, informazioni sullo stato della macchina e perfino videochiamate. Anche Tesla, fra le altre, sta lavorando "
							+ "alla realt� aumentata come strumento da usare dentro l�abitacolo. In poche parole � allo studio il cruscotto del futuro.\r\n" + 
							"Quando le macchine si parleranno fra loro.\r\n" + 
							"Le infrastrutture stradali sono costose da aggiornare, ma in un mondo dove tutte le auto sono connesse molte opere non saranno pi� "
							+ "necessarie. O almeno, � questo che sostengono alcuni operatori telefonici. In una dimostrazione fatta al Lingotto di Torino lo "
							+ "scorso autunno, sono state equipaggiate due macchine con sim 5G. I veicoli condividevano reciprocamente la posizione e se uno dei"
							+ " due non rispettava uno stop all�incrocio, l�altro si fermava automaticamente da solo. Facile immaginare una piattaforma unica "
							+ "di raccolta dati che, in caso di incidente, all�istante indirizza le macchine in arrivo su percorsi alternativi. Il sistema funziona"
							+ " per� solo se � adottato da tutti i veicoli.\r\n" + "Il regno del cloud.\r\n" + 
							"Le aziende che offrono servizi online per la produttivit�, da Microsoft a Zoom, hanno fatto salto da gigante in borsa. "
							+ "Accadr� la stessa cosa con quelle compagnie che analizzano i dati delle vetture connesse intuendo, grazie agli algoritmi, "
							+ "quali sono le reale esigenze dei singoli automobilisti e consigliando le case costruttrici su cosa offrire ai propri clienti.",
							dateArt15,"Tecnologia","Jaime D'Alessandro",true, 0, 0);
			art15.setSubCategory("Tecnologia Automobilistica");
			mapArt.put(14, art15);

			// --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---

			String s16Date = "21-09-2021";
			Date dateArt16 = null;
			try {
				dateArt16 = new SimpleDateFormat("dd-MM-yyyy").parse(s16Date);
			} catch (ParseException e) {
				e.printStackTrace();
			}

			Article art16 = new Article("Il robot ricostruisce affreschi e mosaici deteriorati di Pompei", 
					"Il progetto RePAIR punta a ricomporre migliaia di frammenti delle pareti della Casa dei pittori al lavoro e della Schola Armaturarum.\n\n"
							+ "Robotica e archeologia, quali sviluppi attendono i novelli Indiana Jones? Una risposta concreta arriva dal progetto RePAIR, "
							+ "acronimo di Reconstruction the past: Artificial Intelligence and Robotics meet Cultural Heritage. In pratica un'infrastruttura "
							+ "robotica dotata di braccia meccaniche in grado di scansionare frammenti di affreschi e riconoscerli attraverso un sistema evoluto "
							+ "di digitalizzazione 3D capace di restituirgli la giusta collocazione sul mosaico originale.\r\n" + "\r\n" + 
							"La prima sperimentazione di RePAIR avr� una durata di tre anni e riguarder� inizialmente gli affreschi del soffitto della Casa dei "
							+ "Pittori al Lavoro nell'Insula dei Casti Amanti, gi� danneggiati nell'eruzione del 79 d.C. e poi ridotti in frantumi in seguito ai "
							+ "bombardamenti della Seconda Guerra Mondiale.Il gruppo di esperti di pitture murali dell'Universit� di Losanna, guidato dal professor"
							+ " Michel E. Fuchs, sta gi� portando avanti � dal 2018 � un programma di studio e di ricomposizione manuale.\r\n" + "\r\n" + 
							"L'attivazione del nuovo progetto, che proceder� parallelamente e in modo coordinato con quello in corso da parte dell'�quipe svizzera,"
							+ " consentir� di confrontare dunque due metodologie di lavoro e i rispettivi risultati.Ci sar� poi il secondo caso di studio, "
							+ "costituito dai frammenti degli affreschi della Schola Armaturarum, ancora non ricollocati e danneggiati a seguito del crollo "
							+ "dell'edificio nel 2010 causato dal dissesto idrogeologico.Un progetto ambizioso, frutto di ricerca e competenza tecnologica, "
							+ "che si pone l'obiettivo di risolvere un problema atavico, come ha sottolineato il direttore del Parco archeologico di Pompei "
							+ "Gabriel Zuchtriegel: �Le anfore, gli affreschi, i mosaici, vengono spesso portati alla luce frammentati e quando il numero dei "
							+ "frammenti � molto ampio, con migliaia di pezzi, la ricostruzione manuale ed il riconoscimento delle connessioni tra i frammenti "
							+ "� quasi sempre impossibile o comunque molto laborioso e lento. Questo fa s� che diversi reperti giacciano per lungo tempo nei "
							+ "depositi archeologici, senza poter essere ricostruiti e restaurati, e tantomeno restituiti all'attenzione del pubblico�.\r\n" + "\r\n" + 
							"Il progetto RePAIR � stato finanziato dal programma di ricerca e innovazione Horizon 2020 con 3 milioni e mezzo di euro e conta "
							+ "sull'apporto interdisciplinare di istituzioni - il Parco Archeologico di Pompei e il Ministero della Cultura - e diversi player "
							+ "della robotica e della computer vision: l'Universit� Ca' Foscari di Venezia come ente coordinatore, la Ben-Gurion University of"
							+ " the Negev di Israele, la Rheinische Friedrich Wilhelms Universitat di Bonn in Germania, l'Iit � Istituto Italiano di Tecnologia "
							+ "e l'Associacao do Instituto Superior Tecnico Para a Investigacao e Desenvolvimento del Portogallo.\r\n" + "\r\n" + 
							"�Con la sperimentazione in corso iniziata da pochi giorni vogliamo affinare ed esportare la tecnologia in contesti analoghi a "
							+ "quelli di Pompei, estendendo l'utilizzo anche su papiri e altri supporti fragili come scultura e vasellame, laddove il problema "
							+ "non pu� essere risolto a mano�, ci spiega il professor Marcello Pelillo ordinario di Computer Science dell'Unversit� Ca' Foscari,"
							+ " che coordina il progetto.\r\n" + "\r\n" + 
							"�Sar� una sfida tecnologica molto complicata che riguarder� tre fasi - prosegue Pelillo -. La prima � la scansione di tutti i "
							+ "pezzi dell'affresco con migliaia di pezzi che dovranno essere catalogati. In questo contesto abbiamo dovuto affrontare il problema"
							+ " che riguarda la manipolazione robotica, che non deve danneggiare i frammeni. Per questo useremo la tecnologia �soft robotic�, "
							+ "capace di agire in maniera estremamente delicata sui pezzi dell'affresco. La terza fase, la pi� complicata, coinvolger� il machine"
							+ " learning e l'intelligenza artificiale e riguarder� la risoluzione del puzzle, sfruttando le informazioni acquisite che riguardano"
							+ " dimensioni, geometria e colore dei frammenti. In questa fase la macchina avr� bisogno di integrare quello appreso dalla scansione"
							+ " con l'expertise dell'equipe che gi� stava lavorando. Sar� dunque fondamentale il supporto degli archeologici, che con i loro "
							+ "feedback aiuteranno la macchina nel risolvere il rompicapo�.\r\n" + "\r\n" + 
							"Una collaborazione che � si spera � potr� essere determinante nel portare alla luce quello che, senza l'ausilio robotico,"
							+ " sarebbe rimato nell'ombra.",
							dateArt16,"Tecnologia","Marco Trabucchi",false, 0, 0);
			art16.setSubCategory("Intelligenza Artificiale");
			mapArt.put(15, art16);
		}

		db.commit();


		/*
		 * Popolamento Commenti
		 */
		ConcurrentNavigableMap<Integer, Comment> mapCom = db.getTreeMap("commento");
		if (!mapCom.containsKey(0)) {

			String sDate = "20-09-2021";
			Date dateCom1 = null;
			try {
				dateCom1 = new SimpleDateFormat("dd-MM-yyyy").parse(sDate);
			} catch (ParseException e) {
				e.printStackTrace();
			}
			Comment com1 = new Comment("Marco Rossi","Il mondo dei ghepardi",dateCom1, "Che articolo interessante!",true);
			mapCom.put(0, com1);

			// --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---

			String sDate2 = "24-09-2021";
			Date dateCom2 = null;
			try {
				dateCom2 = new SimpleDateFormat("dd-MM-yyyy").parse(sDate2);
			} catch (ParseException e) {
				e.printStackTrace();
			}
			Comment com2 = new Comment("Lorenzo Battisti","Il mondo dei ghepardi",dateCom2, "Che animale fantastico!",true);
			mapCom.put(1, com2);

			// --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---

			String sDate3 = "23-09-2021";
			Date dateCom3 = null;
			try {
				dateCom3 = new SimpleDateFormat("dd-MM-yyyy").parse(sDate3);
			} catch (ParseException e) {
				e.printStackTrace();
			}
			Comment com3 = new Comment("Giorgia Castelli","Il mondo dei ghepardi",dateCom3, "Tutto qui? Pensavo fosse pi� lungo questo articolo!",false);
			mapCom.put(2, com3);

			// --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---

			String sDate4 = "18-09-2021";
			Date dateCom4 = null;
			try {
				dateCom4 = new SimpleDateFormat("dd-MM-yyyy").parse(sDate4);
			} catch (ParseException e) {
				e.printStackTrace();
			}
			Comment com4 = new Comment("Giorgia Castelli","Sotto il segno di Venere",dateCom4, "Sono la prima a commentare!",true);
			mapCom.put(3, com4);

			// --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---

			String sDate5 = "20-09-2021";
			Date dateCom5 = null;
			try {
				dateCom5 = new SimpleDateFormat("dd-MM-yyyy").parse(sDate5);
			} catch (ParseException e) {
				e.printStackTrace();
			}
			Comment com5 = new Comment("Riccardo Nassisi","Sotto il segno di Venere",dateCom5, "Articolo molto interessante, approfondir� sicuramente l'argomento!",false);
			mapCom.put(4, com5);

			// --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---

			String sDate6 = "03-10-2021";
			Date dateCom6 = null;
			try {
				dateCom6 = new SimpleDateFormat("dd-MM-yyyy").parse(sDate6);
			} catch (ParseException e) {
				e.printStackTrace();
			}
			Comment com6 = new Comment("Giorgia Castelli","Dove si vive meglio in Italia? Ecco la Classifica Ufficiale 2021!",dateCom6, "Al primo posto c'� la mia citt�!",true);
			mapCom.put(5, com6);

			// --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---

			String sDate7 = "10-10-2021";
			Date dateCom7 = null;
			try {
				dateCom7 = new SimpleDateFormat("dd-MM-yyyy").parse(sDate7);
			} catch (ParseException e) {
				e.printStackTrace();
			}
			Comment com7 = new Comment("Lorenzo Battisti","Dove si vive meglio in Italia? Ecco la Classifica Ufficiale 2021!",dateCom7, "Roma al decimo posto? Com'� possibile!?",false);
			mapCom.put(6, com7);

			// --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---

			String sDate8 = "10-10-2021";
			Date dateCom8 = null;
			try {
				dateCom8 = new SimpleDateFormat("dd-MM-yyyy").parse(sDate8);
			} catch (ParseException e) {
				e.printStackTrace();
			}
			Comment com8 = new Comment("Lorenzo Battisti","Infortunio Pessina: � lesione al flessore: l'Atalanta spera di recuperarlo per dicembre",dateCom8, "Anche lui? Peccato, buona guarigione, ti aspettiamo pi� forte di prima! Forza Atalanta!",true);
			mapCom.put(7, com8);

			// --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---

			String sDate9 = "8-10-2021";
			Date dateCom9 = null;
			try {
				dateCom9 = new SimpleDateFormat("dd-MM-yyyy").parse(sDate9);
			} catch (ParseException e) {
				e.printStackTrace();
			}
			Comment com9 = new Comment("Lorenzo Battisti","Netflix da oggi si paga di pi�: ecco gli aumenti",dateCom9, "Ci mancava pure questa...",true);
			mapCom.put(8, com9);

			// --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---

			String sDate10 = "12-10-2021";
			Date dateCom10 = null;
			try {
				dateCom10 = new SimpleDateFormat("dd-MM-yyyy").parse(sDate10);
			} catch (ParseException e) {
				e.printStackTrace();
			}
			Comment com10 = new Comment("Riccardo Nassisi","Netflix da oggi si paga di pi�: ecco gli aumenti",dateCom10, "Aumento dei costi? Credo proprio che annuller� il mio abbonamento!",true);
			mapCom.put(9, com10);

			// --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---

			String sDate11 = "29-09-2021";
			Date dateCom11 = null;
			try {
				dateCom11 = new SimpleDateFormat("dd-MM-yyyy").parse(sDate11);
			} catch (ParseException e) {
				e.printStackTrace();
			}
			Comment com11 = new Comment("Giorgia Castelli","Messaggi spiati e a rischio? Altra bufera privacy per WhatsApp",dateCom11, "Siete degli incompetenti, io non voglio essere spiato!",false);
			mapCom.put(10, com11);

			// --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---

			String sDate12 = "6-10-2021";
			Date dateCom12 = null;
			try {
				dateCom12 = new SimpleDateFormat("dd-MM-yyyy").parse(sDate12);
			} catch (ParseException e) {
				e.printStackTrace();
			}
			Comment com12 = new Comment("Marco Rossi","Il sondaggio che ribalta Roma. Batosta per Raggi e Pd",dateCom12, "Mi sembra che queste previsioni alla fine non sono state rispettate...",true);
			mapCom.put(11, com12);

			// --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---

			String sDate13 = "5-10-2021";
			Date dateCom13 = null;
			try {
				dateCom13 = new SimpleDateFormat("dd-MM-yyyy").parse(sDate13);
			} catch (ParseException e) {
				e.printStackTrace();
			}
			Comment com13 = new Comment("Lorenzo Battisti","\"L'Imu aumenta del 174%\". Scatta l'allarme sul catasto",dateCom13, "Questi sono fuori di testa!",false);
			mapCom.put(12, com13);

			// --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---

			String sDate14 = "6-10-2021";
			Date dateCom14 = null;
			try {
				dateCom14 = new SimpleDateFormat("dd-MM-yyyy").parse(sDate14);
			} catch (ParseException e) {
				e.printStackTrace();
			}
			Comment com14 = new Comment("Lorenzo Battisti","La strage dei ragazzini. Morire a 15 anni per la propria passione",dateCom14, "Che storia triste! Che riposi in pace!",true);
			mapCom.put(13, com14);

			// --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---

			String sDate15 = "5-10-2021";
			Date dateCom15 = null;
			try {
				dateCom15 = new SimpleDateFormat("dd-MM-yyyy").parse(sDate15);
			} catch (ParseException e) {
				e.printStackTrace();
			}
			Comment com15 = new Comment("Giorgia Castelli","La strage dei ragazzini. Morire a 15 anni per la propria passione",dateCom15, "Dovrebbero aumentare i controlli e le protezioni di sicurezza!",true);
			mapCom.put(14, com15);

			// --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---

			String sDate16 = "7-10-2021";
			Date dateCom16 = null;
			try {
				dateCom16 = new SimpleDateFormat("dd-MM-yyyy").parse(sDate16);
			} catch (ParseException e) {
				e.printStackTrace();
			}
			Comment com16 = new Comment("Riccardo Nassisi","La strage dei ragazzini. Morire a 15 anni per la propria passione",dateCom16, "Condoglianze alla famiglia.",true);
			mapCom.put(15, com16);

			// --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---

			String sDate17 = "7-10-2021";
			Date dateCom17 = null;
			try {
				dateCom17 = new SimpleDateFormat("dd-MM-yyyy").parse(sDate17);
			} catch (ParseException e) {
				e.printStackTrace();
			}
			Comment com17 = new Comment("Riccardo Nassisi","Bologna spoglia Armani. La Virtus vent'anni dopo torna regina del basket",dateCom17, "Forza Virtus!",true);
			mapCom.put(16, com17);

			// --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---

			String sDate18 = "6-10-2021";
			Date dateCom18 = null;
			try {
				dateCom18 = new SimpleDateFormat("dd-MM-yyyy").parse(sDate18);
			} catch (ParseException e) {
				e.printStackTrace();
			}
			Comment com18 = new Comment("Giorgia Castelli","Bologna spoglia Armani. La Virtus vent'anni dopo torna regina del basket",dateCom18, "Questo � lo sport che amo!",true);
			mapCom.put(17, com18);

			// --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---

			String sDate19 = "17-08-2021";
			Date dateCom19 = null;
			try {
				dateCom19 = new SimpleDateFormat("dd-MM-yyyy").parse(sDate19);
			} catch (ParseException e) {
				e.printStackTrace();
			}
			Comment com19 = new Comment("Marco Rossi","Kabul, bomba in moschea. I sospetti cadono su Isis-K",dateCom19, "Che brutta notizia!",false);
			mapCom.put(18, com19);

		}

		db.commit();

		//incremento il numero di commenti
		incrementNumComm("Il mondo dei ghepardi");
		incrementNumComm("Il mondo dei ghepardi");
		incrementNumComm("Sotto il segno di Venere");
		incrementNumComm("Dove si vive meglio in Italia? Ecco la Classifica Ufficiale 2021!");
		incrementNumComm("Infortunio Pessina: � lesione al flessore: l'Atalanta spera di recuperarlo per dicembre");
		incrementNumComm("Netflix da oggi si paga di pi�: ecco gli aumenti");
		incrementNumComm("Netflix da oggi si paga di pi�: ecco gli aumenti");
		incrementNumComm("Il sondaggio che ribalta Roma. Batosta per Raggi e Pd");
		incrementNumComm("La strage dei ragazzini. Morire a 15 anni per la propria passione");
		incrementNumComm("La strage dei ragazzini. Morire a 15 anni per la propria passione");
		incrementNumComm("La strage dei ragazzini. Morire a 15 anni per la propria passione");
		incrementNumComm("Bologna spoglia Armani. La Virtus vent'anni dopo torna regina del basket");
		incrementNumComm("Bologna spoglia Armani. La Virtus vent'anni dopo torna regina del basket");


		/*
		 * Popolamento Like 
		 */
		ConcurrentNavigableMap<Integer, Like> mapLike = db.getTreeMap("like");
		if (!mapLike.containsKey(0)) {

			Like newLike1 = new Like("Il mondo dei ghepardi", "Lorenzo Battisti");
			mapLike.put(0, newLike1);

			// --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---

			Like newLike2 = new Like("Il mondo dei ghepardi", "Giorgia Castelli");
			mapLike.put(1, newLike2);

			// --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---

			Like newLike3 = new Like("Sotto il segno di Venere", "Lorenzo Battisti");
			mapLike.put(2, newLike3);

			// --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---

			Like newLike4 = new Like("Il mondo dei ghepardi", "Riccardo Nassisi");
			mapLike.put(3, newLike4);

			// --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---

			Like newLike5 = new Like("Sotto il segno di Venere", "Giorgia Castelli");
			mapLike.put(4, newLike5);

			// --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---

			Like newLike6 = new Like("Sotto il segno di Venere", "Marco Rossi");
			mapLike.put(5, newLike6);

			// --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---

			Like newLike7 = new Like("Meno zucchero in cibi e bevande: primi passi negli USA", "Lorenzo Battisti");
			mapLike.put(6, newLike7);

			// --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---

			Like newLike8 = new Like("Dove si vive meglio in Italia? Ecco la Classifica Ufficiale 2021!", "Giorgia Castelli");
			mapLike.put(7, newLike8);

			// --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---

			Like newLike9 = new Like("Dove si vive meglio in Italia? Ecco la Classifica Ufficiale 2021!", "Riccardo Nassisi");
			mapLike.put(8, newLike9);

			// --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---

			Like newLike10 = new Like("La prima volta della Scala: uno spot per la campagna abbonamenti: \"C'� posto per tutti\"", "Giorgia Castelli");
			mapLike.put(9, newLike10);

			// --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---

			Like newLike11 = new Like("Infortunio Pessina: � lesione al flessore: l'Atalanta spera di recuperarlo per dicembre", "Marco Rossi");
			mapLike.put(10, newLike11);

			// --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---

			Like newLike12 = new Like("Infortunio Pessina: � lesione al flessore: l'Atalanta spera di recuperarlo per dicembre", "Lorenzo Battisti");
			mapLike.put(11, newLike12);

			// --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---

			Like newLike13 = new Like("Netflix da oggi si paga di pi�: ecco gli aumenti", "Riccardo Nassisi");
			mapLike.put(12, newLike13);

			// --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---

			Like newLike14 = new Like("Messaggi spiati e a rischio? Altra bufera privacy per WhatsApp", "Giorgia Castelli");
			mapLike.put(13, newLike14);

			// --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---

			Like newLike15 = new Like("Messaggi spiati e a rischio? Altra bufera privacy per WhatsApp", "Lorenzo Battisti");
			mapLike.put(14, newLike15);

			// --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---

			Like newLike16 = new Like("I 150 anni della tavola periodica degli elementi. \"E' l'alfabeto per capire la materia\"", "Marco Rossi");
			mapLike.put(15, newLike16);

			// --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---

			Like newLike17 = new Like("I 150 anni della tavola periodica degli elementi. \"E' l'alfabeto per capire la materia\"", "Lorenzo Battisti");
			mapLike.put(16, newLike17);

			// --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---

			Like newLike18 = new Like("Il sondaggio che ribalta Roma. Batosta per Raggi e Pd", "Marco Rossi");
			mapLike.put(17, newLike18);

			// --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---

			Like newLike19 = new Like("La strage dei ragazzini. Morire a 15 anni per la propria passione", "Lorenzo Battisti");
			mapLike.put(18, newLike19);

			// --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---

			Like newLike20 = new Like("La strage dei ragazzini. Morire a 15 anni per la propria passione", "Giorgia Castelli");
			mapLike.put(19, newLike20);

			// --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---

			Like newLike21 = new Like("La strage dei ragazzini. Morire a 15 anni per la propria passione", "Riccardo Nassisi");
			mapLike.put(20, newLike21);

			// --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---

			Like newLike22 = new Like("Bologna spoglia Armani. La Virtus vent'anni dopo torna regina del basket", "Riccardo Nassisi");
			mapLike.put(21, newLike22);

			// --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---

			Like newLike23 = new Like("Bologna spoglia Armani. La Virtus vent'anni dopo torna regina del basket", "Giorgia Castelli");
			mapLike.put(22, newLike23);

			// --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---

			Like newLike24 = new Like("Kabul, bomba in moschea. I sospetti cadono su Isis-K", "Marco Rossi");
			mapLike.put(23, newLike24);

			// --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---

			Like newLike25 = new Like("Le dieci tecnologie per l'auto del futuro", "Giorgia Castelli");
			mapLike.put(24, newLike25);

			// --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---

			Like newLike26 = new Like("Le dieci tecnologie per l'auto del futuro", "Lorenzo Battisti");
			mapLike.put(25, newLike26);

			// --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---

			Like newLike27 = new Like("Il robot ricostruisce affreschi e mosaici deteriorati di Pompei", "Marco Rossi");
			mapLike.put(26, newLike27);

		}

		db.commit();


		//incremento il numero di like
		incrementNumLike("Il mondo dei ghepardi");
		incrementNumLike("Il mondo dei ghepardi");
		incrementNumLike("Il mondo dei ghepardi");
		incrementNumLike("Sotto il segno di Venere");
		incrementNumLike("Sotto il segno di Venere");
		incrementNumLike("Sotto il segno di Venere");
		incrementNumLike("Meno zucchero in cibi e bevande: primi passi negli USA");
		incrementNumLike("Dove si vive meglio in Italia? Ecco la Classifica Ufficiale 2021!");
		incrementNumLike("Dove si vive meglio in Italia? Ecco la Classifica Ufficiale 2021!");
		incrementNumLike("La prima volta della Scala: uno spot per la campagna abbonamenti: \\\"C'� posto per tutti\\\"");
		incrementNumLike("Infortunio Pessina: � lesione al flessore: l'Atalanta spera di recuperarlo per dicembre");
		incrementNumLike("Infortunio Pessina: � lesione al flessore: l'Atalanta spera di recuperarlo per dicembre");
		incrementNumLike("Netflix da oggi si paga di pi�: ecco gli aumenti");
		incrementNumLike("Messaggi spiati e a rischio? Altra bufera privacy per WhatsApp");
		incrementNumLike("Messaggi spiati e a rischio? Altra bufera privacy per WhatsApp");
		incrementNumLike("I 150 anni della tavola periodica degli elementi. \"E' l'alfabeto per capire la materia\"");
		incrementNumLike("I 150 anni della tavola periodica degli elementi. \"E' l'alfabeto per capire la materia\"");
		incrementNumLike("Il sondaggio che ribalta Roma. Batosta per Raggi e Pd");
		incrementNumLike("La strage dei ragazzini. Morire a 15 anni per la propria passione");
		incrementNumLike("La strage dei ragazzini. Morire a 15 anni per la propria passione");
		incrementNumLike("La strage dei ragazzini. Morire a 15 anni per la propria passione");
		incrementNumLike("Bologna spoglia Armani. La Virtus vent'anni dopo torna regina del basket");
		incrementNumLike("Bologna spoglia Armani. La Virtus vent'anni dopo torna regina del basket");
		incrementNumLike("Kabul, bomba in moschea. I sospetti cadono su Isis-K");
		incrementNumLike("Le dieci tecnologie per l'auto del futuro");
		incrementNumLike("Le dieci tecnologie per l'auto del futuro");
		incrementNumLike("Il robot ricostruisce affreschi e mosaici deteriorati di Pompei");


		/*
		 * Popolamento Autori
		 */
		ConcurrentNavigableMap<Integer, Author> mapAut = db.getTreeMap("autore");

		if (!mapAut.containsKey(0)) {

			String sdata = "01-01-2021";
			Date dataAut1 = null;
			try {
				dataAut1 = new SimpleDateFormat("dd-MM-yyyy").parse(sdata);
			} catch (ParseException e) {
				e.printStackTrace();
			}

			Author aut1 = new Author("autore1","autore1@gmail.com","autore1","nomeA1","cognomeA1",dataAut1);

			mapAut.put(0, aut1);

			// --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---

			String sdataAM = "05-08-2021";
			Date dataAM = null;
			try {
				dataAM = new SimpleDateFormat("dd-MM-yyyy").parse(sdataAM);
			} catch (ParseException e) {
				e.printStackTrace();
			}

			Author autAM = new Author("AdaMasoero","AdaMasoero@gmail.com","pwAdaMasoero","Ada","AdaMasoero",dataAM);

			mapAut.put(1, autAM);

			// --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---

			String sdataCG = "05-02-2021";
			Date dataCG = null;
			try {
				dataCG = new SimpleDateFormat("dd-MM-yyyy").parse(sdataCG);
			} catch (ParseException e) {
				e.printStackTrace();
			}

			Author autCG = new Author("ChiaraGuzzonato","ChiaraGuzzonato@gmail.com","pwChiaraGuzzonato","Chiara","Guzzonato",dataCG);

			mapAut.put(2, autCG);

			// --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---

			String sdataLB = "15-02-2021";
			Date dataLB = null;
			try {
				dataLB = new SimpleDateFormat("dd-MM-yyyy").parse(sdataLB);
			} catch (ParseException e) {
				e.printStackTrace();
			}

			Author autLB = new Author("LuiginoBirichino","LuiginoBirichino@gmail.com","pwLuiginoBirichino","Luigino","Birichino",dataLB);

			mapAut.put(3, autLB);

			// --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---

			String sdataT365 = "25-12-2020";
			Date dataT365= null;
			try {
				dataT365 = new SimpleDateFormat("dd-MM-yyyy").parse(sdataT365);
			} catch (ParseException e) {
				e.printStackTrace();
			}

			Author autT365 = new Author("Travel365","Travel365@gmail.com","pwTravel365","Travel365","",dataT365);

			mapAut.put(4, autT365);

			// --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---

			String sdataLBo = "03-06-2020";
			Date dataLBo= null;
			try {
				dataLBo = new SimpleDateFormat("dd-MM-yyyy").parse(sdataLBo);
			} catch (ParseException e) {
				e.printStackTrace();
			}

			Author autLBo = new Author("Luigi Bolognini","LuigiBolognini@gmail.com","pwLuigiBolognini","Luigi","Bolognini",dataLBo);

			mapAut.put(5, autLBo);

			// --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---

			String sdataSS = "12-10-2020";
			Date dataSS= null;
			try {
				dataSS = new SimpleDateFormat("dd-MM-yyyy").parse(sdataSS);
			} catch (ParseException e) {
				e.printStackTrace();
			}

			Author autSS = new Author("SkySport","SkySport@gmail.com","pwSkySport","Sky","Sport",dataSS);

			mapAut.put(6, autSS);

			// --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---

			String sdataCS = "14-04-2021";
			Date dataCS= null;
			try {
				dataCS = new SimpleDateFormat("dd-MM-yyyy").parse(sdataCS);
			} catch (ParseException e) {
				e.printStackTrace();
			}

			Author autCS= new Author("Claudio Schirru","ClaudioSchirru@gmail.com","pwClaudioSchirru","Claudio","Schirru",dataCS);

			mapAut.put(7, autCS);

			// --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---

			String sdataED = "08-08-2021";
			Date dataED = null;
			try {
				dataED = new SimpleDateFormat("dd-MM-yyyy").parse(sdataED);
			} catch (ParseException e) {
				e.printStackTrace();
			}

			Author autED = new Author("Elena Dusi","ElenaDusi@gmail.com","pwElenaDusi","Elena","Dusi",dataED);

			mapAut.put(8, autED);

			// --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---

			String sdataFG = "20-02-2021";
			Date dataFG= null;
			try {
				dataFG = new SimpleDateFormat("dd-MM-yyyy").parse(sdataFG);
			} catch (ParseException e) {
				e.printStackTrace();
			}

			Author autFG = new Author("Francesca Galici","FrancescaGalici@gmail.com","pwFrancescaGalici","Francesca","Galici",dataFG);

			mapAut.put(9, autFG);

			// --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---

			String sdataFGa = "01-02-2021";
			Date dataFGa = null;
			try {
				dataFGa = new SimpleDateFormat("dd-MM-yyyy").parse(sdataFGa);
			} catch (ParseException e) {
				e.printStackTrace();
			}

			Author autFGa = new Author("Federico Garau","FedericoGarau@gmail.com","pwFedericoGarau","Federico","Garau",dataFGa);

			mapAut.put(10, autFGa);

			// --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---

			String sdataRS = "03-03-2021";
			Date dataRS= null;
			try {
				dataRS = new SimpleDateFormat("dd-MM-yyyy").parse(sdataRS);
			} catch (ParseException e) {
				e.printStackTrace();
			}

			Author autRS = new Author("Riccardo Signorini","RiccardoSignorini@gmail.com","pwRiccardoSignorini","Riccardo","Signorini",dataRS);

			mapAut.put(11, autRS);

			// --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---

			String sdataOE = "14-08-2021";
			Date dataOE = null;
			try {
				dataOE = new SimpleDateFormat("dd-MM-yyyy").parse(sdataOE);
			} catch (ParseException e) {
				e.printStackTrace();
			}

			Author autOE = new Author("Oscar Eleni","OscarEleni@gmail.com","pwOscarEleni","Oscar","Eleni",dataOE);

			mapAut.put(12, autOE);

			// --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---

			String sdataCC = "13-04-2021";
			Date dataCC= null;
			try {
				dataCC = new SimpleDateFormat("dd-MM-yyyy").parse(sdataCC);
			} catch (ParseException e) {
				e.printStackTrace();
			}

			Author autCC = new Author("Chiara Clausi","ChiaraClausi@gmail.com","pwChiaraClausi","Chiara","Clausi",dataCC);

			mapAut.put(13, autCC);

			// --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---

			String sdataJA = "10-10-2020";
			Date dataJA = null;
			try {
				dataJA = new SimpleDateFormat("dd-MM-yyyy").parse(sdataJA);
			} catch (ParseException e) {
				e.printStackTrace();
			}

			Author autJA = new Author("Jaime D'Alessandro","JaimeDAlessandro@gmail.com","pwJaimeDAlessandro","Jaime","D'Alessandro",dataJA);

			mapAut.put(14, autJA);

			// --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---

			String sdataMT = "25-03-2021";
			Date dataMT= null;
			try {
				dataMT = new SimpleDateFormat("dd-MM-yyyy").parse(sdataMT);
			} catch (ParseException e) {
				e.printStackTrace();
			}

			Author autMT = new Author("Marco Trabucchi","MarcoTrabucchi@gmail.com","pwMarcoTrabucchi","Marco","Trabucchi",dataMT);

			mapAut.put(15, autMT);

			// --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---

			String sdataIR = "22-02-2021";
			Date dataIR = null;
			try {
				dataIR = new SimpleDateFormat("dd-MM-yyyy").parse(sdataIR);
			} catch (ParseException e) {
				e.printStackTrace();
			}

			Author autIR = new Author("Ignazio Riccio","IgnazioRiccio@gmail.com","pwIgnazioRiccio","Ignazio","Riccio",dataIR);

			mapAut.put(16, autIR);

			// --- --- --- --- --- --- --- --- --- --- --- --- --- --- ---

			String sdataTest = "22-02-2021";
			Date dataTest = null;
			try {
				dataTest = new SimpleDateFormat("dd-MM-yyyy").parse(sdataTest);
			} catch (ParseException e) {
				e.printStackTrace();
			}

			Author autTest = new Author("testauthor","test@author.com","password","Test","Author",dataTest);

			mapAut.put(17, autTest);

		}

		db.commit();
		db.close();

	}

	/*****************************************************************************************************************************************
	 *****************************************************************************************************************************************
	 ******************************************* FINE POPOLAMENTO DATABASE *******************************************************************
	 *****************************************************************************************************************************************
	 *****************************************************************************************************************************************
	 */


	/*
	 * Metodo per aumentare il numero di commenti di un articolo
	 */
	public void incrementNumComm(String titleArt) {
		DB db = getDB();

		ConcurrentNavigableMap<Integer, Article> mapArt = db.getTreeMap("articolo");

		Article newArt;

		for (Map.Entry<Integer, Article> art : mapArt.entrySet()) {

			if(art.getValue().getTitle().equals(titleArt)) {

				newArt = art.getValue();

				newArt.addNumTotComm();
				mapArt.replace(art.getKey(), newArt);
				db.commit();

			} 
		}

		db.commit();
		db.close();
	}


	/*
	 * Metodo per aumentare il numero di like di un articolo
	 */
	public void incrementNumLike(String titleArt) {
		DB db = getDB();

		ConcurrentNavigableMap<Integer, Article> mapArt = db.getTreeMap("articolo");

		Article newArt;

		for (Map.Entry<Integer, Article> art : mapArt.entrySet()) {

			if(art.getValue().getTitle().equals(titleArt)) {

				newArt = art.getValue();

				newArt.addNumTotLike();
				mapArt.replace(art.getKey(), newArt);	

			}
		}

		db.commit();
		db.close();
	}
	
	/*
	 * Metodo per aumentare il numero di commenti di un articolo
	 */
	public void decrementNumComm(String titleArt) {
		DB db = getDB();

		ConcurrentNavigableMap<Integer, Article> mapArt = db.getTreeMap("articolo");

		Article newArt;

		for (Map.Entry<Integer, Article> art : mapArt.entrySet()) {

			if(art.getValue().getTitle().equals(titleArt)) {

				newArt = art.getValue();

				newArt.lessNumTotComm();
				mapArt.replace(art.getKey(), newArt);
				db.commit();

			} 
		}

		db.commit();
		db.close();
	}


	/*
	 * Metodo per stampare la lista di tutti gli utenti
	 */
	public String printAllUsers() {

		DB db = getDB();

		String s = "";

		ConcurrentNavigableMap<Integer,User> map = db.getTreeMap("utenti");

		for (Map.Entry<Integer, User> u : map.entrySet()) {

			s += u.getValue().toString();
			s += "\n";

		}

		return s;

	}

	/*
	 * Metodo per recuperare le informazioni di fatturazione di un utente
	 */
	public Subscription getSubscriptionInfo(User utenteLoggato) {	

		DB db = getDB();

		ConcurrentNavigableMap<Integer, Subscription> map = db.getTreeMap("abbonamenti");

		for (Map.Entry<Integer, Subscription> sub : map.entrySet()) {	

			if(sub.getValue().getIdUser().equals(utenteLoggato.getUsername())) {

				Subscription subReturn = sub.getValue();

				return subReturn;
			}

		}

		db.commit();
		db.close();

		return null;
	}

	/*
	 * Metodo che, data una determinata categoria o sottocategoria, restituisce la lista di tutti e solo gli articoli associati a codesta categoria/sottocategoria
	 */
	public ArrayList<Article> getArticles(String cat) {	

		DB db = getDB();

		ConcurrentNavigableMap<Integer, Article> map = db.getTreeMap("articolo");

		ArrayList<Article> result = new ArrayList<Article>();

		for (Map.Entry<Integer, Article> articoli : map.entrySet()) {

			if(cat.equals("Tutti gli articoli")) {

				result.add(articoli.getValue());

			}

			if(articoli.getValue().getCategory().equals(cat) || articoli.getValue().getSubCategory().equals(cat)) {

				result.add(articoli.getValue());

			}

		}

		db.commit();
		db.close();

		return result;
	}


	/*
	 * Metodo che, data una determinata sottocategoria, restituisce la lista di tutti e solo gli articoli associati a codesta sottocategoria
	 */
	public ArrayList<Article> getArticlesSubCat(String subCat) {	

		DB db = getDB();

		ConcurrentNavigableMap<Integer, Article> map = db.getTreeMap("articolo");

		ArrayList<Article> result = new ArrayList<Article>();

		for (Map.Entry<Integer, Article> articoli : map.entrySet()) {

			//stampo solo gli articoli che corrispondono alla sottocategoria
			if(articoli.getValue().getSubCategory().equals(subCat)) {

				result.add(articoli.getValue());

			}
		}

		db.commit();
		db.close();

		return result;
	}


	/*
	 * Metodo che, dato il titolo di un articolo, restituisce l'articolo stesso se questo � presente nel DB
	 */
	public Article printArticle(String articolo) {

		DB db = getDB();

		ConcurrentNavigableMap<Integer, Article> map = db.getTreeMap("articolo");

		for (Map.Entry<Integer, Article> articoli : map.entrySet()) {

			if (articoli.getValue().getTitle().equals(articolo)) {

				Article result = articoli.getValue();

				db.commit();
				db.close();

				return result;			
			}
		}

		db.commit();
		db.close();

		return null;
	}

	/*
	 * Metodo che, dato il titolo di un articolo, restituisce tutti i commenti ad esso associato
	 */
	public ArrayList<Comment> getArticleComments(String titoloArticolo) {

		DB db = getDB();

		ArrayList<Comment> listComments = new ArrayList<Comment>();
		ConcurrentNavigableMap<Integer, Comment> mapCom = db.getTreeMap("commento");

		for (Map.Entry<Integer, Comment> commenti : mapCom.entrySet()) {

			if (commenti.getValue().getArticleTitle().equals(titoloArticolo) && commenti.getValue().isApproved()) {

				listComments.add(commenti.getValue());

			}
		}

		db.commit();
		db.close();

		return listComments;
	}

	/*
	 * Metodo che aggiunge un nuovo commento al DB
	 */
	public Boolean newComment(Comment newComm) throws IllegalArgumentException {
		DB db = getDB();

		ConcurrentNavigableMap<Integer,Comment> map = db.getTreeMap("commento");

		int k;

		if (map.isEmpty()) {
			k = 0;
		} else {
			k = map.lastKey() + 1;
		}

		map.put(k, newComm);

		//controllo che il commento sia stato effettivamente aggiunto al DB
		if(map.size() > k) {

			db.commit();
			db.close();
			return true;

		} else {

			db.commit();
			db.close();
			return false;

		}
	}


	/*
	 * Metodo che aggiunge un nuovo like al DB e verifica che non sia gi� stato inserito dal medesimo utente al medesimo articolo
	 */	
	public Boolean newLike(Like newLi) throws IllegalArgumentException {
		DB db = getDB();

		ConcurrentNavigableMap<Integer,Like> map = db.getTreeMap("like");

		int k;

		if (map.isEmpty()) {

			k = 0;

		} else {

			k = map.lastKey() + 1;

		}

		//devo controllare che l'utente NON abbia gi� messo mi piace al medesimo articolo
		for (Map.Entry<Integer, Like> like : map.entrySet()) {

			if (like.getValue().getTitleArticle().equals(newLi.getTitleArticle()) && like.getValue().getUser().equals(newLi.getUser())) {

				return false;

			}
		}

		map.put(k, newLi);

		//verifico che il like sia stato correttamente aggiunto
		if(map.size() > k) {

			db.commit();

			//incremento il numero di like dell'articolo
			incrementNumLike(newLi.getTitleArticle());

			db.commit();
			db.close();
			return true;

		} else {

			db.commit();
			db.close();
			return false;

		}
	}

	/*
	 * Metodo che, dato il titolo di un articolo, restituisce l'autore che l'ha scritto
	 */	
	public Author getArticleAuthor(String titoloArticolo) {

		DB db = getDB();
		String nAut = null;

		ConcurrentNavigableMap<Integer, Article> mapArt = db.getTreeMap("articolo");
		ConcurrentNavigableMap<Integer, Author> mapAut = db.getTreeMap("autore");

		for (Map.Entry<Integer, Article> articolo : mapArt.entrySet()) {

			if (articolo.getValue().getTitle().equals(titoloArticolo)) {

				nAut = articolo.getValue().getAuthor();
				break;

			}
		}

		for (Map.Entry<Integer, Author> autore : mapAut.entrySet()) {

			if (autore.getValue().getUsername().equalsIgnoreCase(nAut)) {

				return  autore.getValue();

			}
		}

		return null;
	}

	/*
	 * Metodo che aggiunge un nuovo like al DB e verifica che non sia gi� stato inserito dal medesimo utente al medesimo articolo
	 */	
	public ArrayList<Article> getListArticles() {
		DB db = getDB();

		ConcurrentNavigableMap<Integer, Article> mapArt = db.getTreeMap("articolo");

		ArrayList<Article> listArticles = new ArrayList<Article>();

		for (Map.Entry<Integer, Article> articoli : mapArt.entrySet()) {

			listArticles.add(articoli.getValue());	

		}

		Collections.sort(listArticles, new Comparator<Article>() {
			public int compare(Article a1, Article a2) {
				if (a2.getDate().after(a1.getDate())) return +1;
				else if (a1.getDate().after(a2.getDate())) return -1;
				else return 0;
			}	
		});	


		db.commit();
		db.close();

		return listArticles;
	}

	/*
	 * Metodo che verifica se le credenziali inserite sono contenute del DB e, in caso positivo, restituisce l'utente loggato
	 */	
	public User login(String username, String password) throws IllegalArgumentException {
		DB db = getDB();

		ConcurrentNavigableMap<Integer,User> map = db.getTreeMap("utenti");

		for (Entry<Integer, User> utenti : map.entrySet()) {

			if (utenti.getValue().getUsername().equals(username)) {

				if (utenti.getValue().getPassword().equals(password)) {

					User utente = utenti.getValue();

					db.commit();
					db.close();

					return utente;
				}
			}
		}

		db.commit();
		db.close();

		return null;
	}

	/*
	 * Metodo che, dato l'username, restituisce le informazioni di un utente
	 */	
	public User getUserInfo(String username) throws IllegalArgumentException {

		DB db = getDB();
		ConcurrentNavigableMap<Integer, User> mapUser = db.getTreeMap("utenti");

		for (Entry<Integer, User> utenti : mapUser.entrySet()) {

			if (utenti.getValue().getUsername().equals(username)) {

				User utente = utenti.getValue();

				db.commit();
				db.close();

				return utente;

			}
		}

		db.commit();
		db.close();

		return null;

	}

	/*
	 * Metodo che verifica che la password inserita per effettuare l'accesso admin sia corretta o meno. In caso positivo, restituisce l'admin.
	 */	
	public Admin adminLogin(String password) throws IllegalArgumentException {

		Admin admin = new Admin();

		if (admin.getPassword().equals(password)) {

			return admin;

		} else {

			return null;

		}
	}


	/*
	 * Metodo che aggiunge un nuovo utente al DB
	 */	
	public User registration(String username, String email, String password, String name, String surname, Date birthDate, String birthCity, Gender genre, String address, ArrayList<String> listFavCat) {

		Date today = new Date();

		if (!today.after(birthDate)) {

			return null;			

		} 

		DB db = getDB();

		ConcurrentNavigableMap<Integer, User> mapUtenti = db.getTreeMap("utenti");

		User nuovoUtente = new User(username, email, password, name, surname, birthDate, birthCity, genre, address, listFavCat);

		int lastId = mapUtenti.size();

		mapUtenti.put(lastId, nuovoUtente);

		if(mapUtenti.size()>lastId) {

			db.commit();
			db.close();
			return nuovoUtente;

		} else {

			db.commit();
			db.close();
			return null;

		}

	}

	/*
	 * Metodo che aggiunge un nuovo autore al DB
	 */	
	public Author authorRegistration(String username, String email, String pw, String name, String surname, Date dateReg) {
		DB db = getDB();
		ConcurrentNavigableMap<Integer, Author> mapAut = db.getTreeMap("autore");

		Author newAuthor = new Author(username, email, pw, name, surname, dateReg);

		int lastId = mapAut.size();

		mapAut.put(lastId, newAuthor);

		if(mapAut.size()>lastId) {

			db.commit();
			db.close();
			return newAuthor;

		} else {

			db.commit();
			db.close();
			return null;

		}

	}

	/*
	 * Metodo che verifica che le credenziali inserite per effettuare l'accesso di un autore siano corrette e siano presenti nel DB
	 */	
	public Author loginAuthor(String username, String password) throws IllegalArgumentException {
		DB db = getDB();

		ConcurrentNavigableMap<Integer,Author> map = db.getTreeMap("autore");

		for (Entry<Integer, Author> autori : map.entrySet()) {

			if (autori.getValue().getUsername().equals(username)) {

				if (autori.getValue().getPassword().equals(password)) {

					Author autore = autori.getValue();

					db.commit();
					db.close();

					return autore;
				}
			}
		}

		db.commit();
		db.close();

		return null;
	}

	/*
	 * Metodo che aggiunge un nuovo abbonamento al DB
	 */	
	public User subscribe(String nomeUtente, String cardNumber, String name, String surname, String dataScadenza, String CVV, String nazione, String cap) throws IllegalArgumentException {

		Subscription newSubscription = new Subscription(cardNumber, name, surname, dataScadenza, CVV, nazione, cap, nomeUtente);

		DB db = getDB();

		ConcurrentNavigableMap<Integer, Subscription> map = db.getTreeMap("abbonamenti");

		int lastId = map.size();

		map.put(lastId, newSubscription);	

		db.commit();

		ConcurrentNavigableMap<Integer, User> mapUser = db.getTreeMap("utenti");

		for (Entry<Integer, User> utenti : mapUser.entrySet()) {

			if (utenti.getValue().getUsername().equals(nomeUtente)) {

				User temp = utenti.getValue(); // temp prende il valore dell'oggetto contenuto dentro utenti
				temp.setSubscribed(true); // setta la variabile a true
				int indice = utenti.getKey();   //recupero l'indice dell'utente  (BISOGNA VERIFICARE CHE SIA CORRETTO)

				mapUser.replace(indice, utenti.getValue(), temp);

				db.commit();
				db.close();
				return temp;

			}
		}

		db.commit();
		db.close();

		return null;
	}

	/*
	 * Metodo che restiuisce la lista delle categorie	 
	 */
	public ArrayList<String> getCategories() {

		DB db = getDB();
		ConcurrentNavigableMap<Integer, Category> mapCat = db.getTreeMap("categoria");

		ArrayList<String> result = new ArrayList<String>();

		for (Entry<Integer, Category> cat : mapCat.entrySet()) {

			result.add(cat.getValue().getName());

		}

		return result;

	}

	/*
	 * Metodo che restituisce la lista delle sottocategorie di una determinata categoria
	 */
	public ArrayList<String> getSubCategories(String selcat) {

		DB db = getDB();

		ConcurrentNavigableMap<Integer, Category> mapCat = db.getTreeMap("categoria");

		ArrayList<String> result = new ArrayList<String>();

		for (Entry<Integer, Category> cat : mapCat.entrySet()) {

			if (cat.getValue().getName().equals(selcat)) {

				result = cat.getValue().getSubCat();

			}
		}

		return result;
	}

	/*
	 * Metodo che restituisce i titoli di tutti gli articoli
	 */
	public ArrayList<String> getAllTitles() {
		DB db = getDB();
		ConcurrentNavigableMap<Integer, Article> mapArticoli = db.getTreeMap("articolo");

		ArrayList<String> result = new ArrayList<String>();

		for (Entry<Integer, Article> art : mapArticoli.entrySet()) {

			result.add(art.getValue().getTitle());

		}

		return result;
	}

	/*
	 * Metodo che restituisce l'oggetto articolo relativo a un determinato titolo
	 */
	public Article getArticle(String tit) {
		DB db = getDB();
		ConcurrentNavigableMap<Integer, Article> mapArticoli = db.getTreeMap("articolo");

		for (Entry<Integer, Article> art : mapArticoli.entrySet()) {

			if (art.getValue().getTitle().equals(tit)) {

				return art.getValue();

			}
		}

		return null;

	}

	/*
	 * Metodo che restituisce tutti i nomi degli autori
	 */
	public ArrayList<String> getAllAuthors() {
		DB db = getDB();

		ConcurrentNavigableMap<Integer, Author> mapAutore = db.getTreeMap("autore");

		ArrayList<String> result = new ArrayList<String>();

		for (Entry<Integer, Author> a : mapAutore.entrySet()) {

			result.add(a.getValue().getUsername());

		}

		return result;
	}

	/*
	 * Metodo che inserisce un nuovo articolo nel DB
	 */
	public Boolean addArticle(Article art) {

		DB db = getDB();

		ConcurrentNavigableMap<Integer, Article> mapArticoli = db.getTreeMap("articolo");

		int k;

		if (mapArticoli.isEmpty()) {
			k = 0;
		} else {

			for (Map.Entry<Integer, Article> articolo : mapArticoli.entrySet()) {

				if(articolo.getValue().getTitle().equals(art.getTitle())) {

					db.commit();
					db.close();
					return false;

				}
			}

			k = mapArticoli.lastKey() + 1;
		}

		mapArticoli.put(k, art);

		//verifico che l'articolo sia stato correttamente aggiunto al DB
		if(mapArticoli.size()>k) {

			db.commit();
			db.close();
			return true;

		} else {

			db.commit();
			db.close();
			return false;

		}
	}

	/*
	 * Metodo che modifica un articolo esistente dato il suo titolo
	 */
	public Boolean modifyArticle(String t, Article a) {

		DB db = getDB();

		ConcurrentNavigableMap<Integer, Article> mapArticoli = db.getTreeMap("articolo");

		for (Map.Entry<Integer, Article> articolo : mapArticoli.entrySet()) {

			if(articolo.getValue().getTitle().equals(t)) {

				int k = articolo.getKey();
				mapArticoli.replace(k, articolo.getValue(), a);

				db.commit();
				db.close();
				return true;

			}
		}

		db.commit();
		db.close();
		return false;
	}

	/*
	 * Metodo che, dato il titolo di un articolo, elimina quest'ultimo dal DB
	 */
	public Boolean deleteArticle(String t) {
		DB db = getDB();
		ConcurrentNavigableMap<Integer, Article> mapArticoli = db.getTreeMap("articolo");

		for (Map.Entry<Integer, Article> articolo : mapArticoli.entrySet()) {

			if(articolo.getValue().getTitle().equals(t)) {

				int k = articolo.getKey();

				mapArticoli.remove(k);

				db.commit();
				db.close();
				return true;

			}
		}

		db.commit();
		db.close();
		return false;
	}

	/*
	 * Metodo che aggiunge una nuova categoria al DB
	 */
	public Boolean addCategory(Category c) {
		DB db = getDB();
		ConcurrentNavigableMap<Integer, Category> mapCat = db.getTreeMap("categoria");

		int k;

		if (mapCat.isEmpty()) {
			k = 0;
		} else {

			for (Map.Entry<Integer, Category> categoria : mapCat.entrySet()) {

				if(categoria.getValue().getName().equals(c.getName())) {

					db.commit();
					db.close();
					return false;

				}

			}

			k = mapCat.lastKey() + 1;

			mapCat.put(k, c);
		}

		//Controllo che la categoria sia stata effettivamente aggiunta al DB
		if(mapCat.size()>k) {

			db.commit();
			db.close();
			return true;

		} else {

			db.commit();
			db.close();
			return false;

		}
	}

	/*
	 * Metodo che aggiunge una nuova sottocategoria associata a una determinata categoria
	 */
	public Boolean addSubCategory(String cat, String subcat) {

		DB db = getDB();

		ConcurrentNavigableMap<Integer, Category> mapCat = db.getTreeMap("categoria");

		for (Map.Entry<Integer, Category> categoria : mapCat.entrySet()) {

			if (categoria.getValue().getName().equals(cat)) {

				for (String s: categoria.getValue().getSubCat()) {

					if (s.equals(subcat)) {

						db.commit();
						db.close();
						return false;

					}

				}

				int k = categoria.getKey();

				Category c = categoria.getValue();

				c.setSubCat(subcat);

				mapCat.replace(k, categoria.getValue(), c);

				db.commit();
				db.close();
				return true;

			}
		}

		db.commit();
		db.close();
		return false;
	}

	/*
	 * Metodo che elimina una categoria
	 */
	public Boolean deleteCategory(String cat) {

		DB db = getDB();

		ConcurrentNavigableMap<Integer, Category> mapCat = db.getTreeMap("categoria");

		for (Map.Entry<Integer, Category> categoria : mapCat.entrySet()) {

			if(categoria.getValue().getName().equals(cat)) {

				int k = categoria.getKey();

				mapCat.remove(k);

				db.commit();
				db.close();
				return true;

			}
		}

		db.commit();
		db.close();
		return false;
	}

	/*
	 * Metodo che elimina una sottocategoria
	 */
	public Boolean deleteSubCategory(String cat, String subcat) {

		DB db = getDB();

		ConcurrentNavigableMap<Integer, Category> mapCat = db.getTreeMap("categoria");

		for (Map.Entry<Integer, Category> categoria : mapCat.entrySet()) {

			if(categoria.getValue().getName().equals(cat)) {

				int k = categoria.getKey();

				Category c = categoria.getValue();

				boolean res = c.removeSubCat(subcat);

				if (res) {	

					mapCat.replace(k, categoria.getValue(), c);

				}

				db.commit();
				db.close();
				return res;
			}
		}

		db.commit();
		db.close();
		return false;
	}

	/*
	 * Metodo che restituisce tutti i commenti in attesa di moderazione
	 */
	public ArrayList<Comment> getUnmoderatedComments() {

		DB db = getDB();

		ArrayList<Comment> listComments = new ArrayList<Comment>();

		ConcurrentNavigableMap<Integer, Comment> map = db.getTreeMap("commento");	

		for (Map.Entry<Integer, Comment> commento : map.entrySet()) {

			if (!commento.getValue().isApproved()) {

				listComments.add(commento.getValue());

			}

		}

		db.commit();
		db.close();

		return listComments;

	}

	/*
	 * Metodo che restituisce tutti i commenti in attesa di moderazione di un determinato articolo dato il suo titolo
	 */
	public ArrayList<Comment> getUnmoderatedComments(String title) {

		DB db = getDB();

		ArrayList<Comment> listComments = new ArrayList<Comment>();

		ConcurrentNavigableMap<Integer, Comment> map = db.getTreeMap("commento");	

		for (Map.Entry<Integer, Comment> commento : map.entrySet()) {

			if (!commento.getValue().isApproved() && commento.getValue().getArticleTitle().equals(title)) {	

				listComments.add(commento.getValue());

			}
		}

		db.commit();
		db.close();

		return listComments;

	}

	/*
	 * Metodo che restituisce tutti i commenti gi� moderati
	 */
	public ArrayList<Comment> getModeratedComments() {

		DB db = getDB();

		ArrayList<Comment> listComments = new ArrayList<Comment>();

		ConcurrentNavigableMap<Integer, Comment> map = db.getTreeMap("commento");	

		for (Map.Entry<Integer, Comment> commento : map.entrySet()) {

			if (commento.getValue().isApproved()) {	

				listComments.add(commento.getValue());

			}
		}

		db.commit();
		db.close();

		return listComments;

	}	

	/*
	 * Metodo che restituisce tutti i commenti gi� moderati di un determinato articolo dato il suo titolo
	 */
	public ArrayList<Comment> getModeratedComments(String title) {

		DB db = getDB();

		ArrayList<Comment> listComments = new ArrayList<Comment>();

		ConcurrentNavigableMap<Integer, Comment> map = db.getTreeMap("commento");	

		for (Map.Entry<Integer, Comment> commento : map.entrySet()) {

			if (commento.getValue().isApproved() && commento.getValue().getArticleTitle().equals(title)) {		

				listComments.add(commento.getValue());

			}
		}

		db.commit();
		db.close();

		return listComments;

	}

	/*
	 * Metodo che approva un commento in attesa di moderazione dato il suo contenuto
	 */
	public Boolean approveComment(String text) {

		DB db = getDB();

		ConcurrentNavigableMap<Integer, Comment> map = db.getTreeMap("commento");	

		for (Map.Entry<Integer, Comment> comment : map.entrySet()) {

			if (comment.getValue().getTextComment().equals(text) && !comment.getValue().isApproved()) {

				Comment c = comment.getValue();
				c.setState(true);
				map.replace(comment.getKey(),comment.getValue(),c);

				db.commit();
				//incremento il numero di commenti dell'articolo
				incrementNumComm(c.getArticleTitle() );
				db.commit();
				db.close();

				return true;
			}
		}

		db.commit();
		db.close();

		return false;

	}

	/*
	 * Metodo che elimina un commento dato il suo contenuto
	 */
	public Boolean deleteComment(String text) {

		DB db = getDB();

		ConcurrentNavigableMap<Integer, Comment> map = db.getTreeMap("commento");	

		for (Map.Entry<Integer, Comment> comment : map.entrySet()) {

			if (comment.getValue().getTextComment().equals(text)) {

				String artTitle = comment.getValue().getArticleTitle();
				// diminuisco il numero di commenti dell'articolo
				decrementNumComm(artTitle);				
				map.remove(comment.getKey());
				

				db.commit();
				db.close();

				return true;

			}
		}

		db.commit();
		db.close();

		return false;
	}

	/*
	 * Metodo che restituisce tutti i commenti effettuati da un utente dato il suo nome
	 */
	public ArrayList<String> getActivityComments(String name) {

		DB db = getDB();
		ConcurrentNavigableMap<Integer, Comment> mapCom = db.getTreeMap("commento");

		ArrayList<String> listArt = new ArrayList<String>();


		for (Map.Entry<Integer, Comment> commenti : mapCom.entrySet()) {

			if (commenti.getValue().getUser().equals(name) && commenti.getValue().isApproved() ) {

				listArt.add(commenti.getValue().getArticleTitle());

			}
		}

		db.close();
		return listArt;
	}

	/*
	 * Metodo che restituisce tutti i like effettuati da un utente dato il suo nome
	 */
	public ArrayList<String> getActivityLikes(String name) {

		DB db = getDB();

		ConcurrentNavigableMap<Integer, Like> mapLike = db.getTreeMap("like");

		ArrayList<String> listArt = new ArrayList<String>();


		for (Map.Entry<Integer, Like> like : mapLike.entrySet()) {

			if (like.getValue().getUser().equals(name)) {

				listArt.add(like.getValue().getTitleArticle());

			}
		}

		db.close();

		return listArt;
	}

	/*
	 * Metodo che restituisce la lista di articoli scritti da un determinato autore
	 */
	public ArrayList<String> getListArticleAuthor(String name) {

		DB db = getDB();

		ConcurrentNavigableMap<Integer, Article> mapLike = db.getTreeMap("articolo");

		ArrayList<String> listArt = new ArrayList<String>();


		for (Map.Entry<Integer, Article> articolo : mapLike.entrySet()) {

			if (articolo.getValue().getAuthor().equals(name)) {

				listArt.add(articolo.getValue().getTitle());

			}
		}

		db.commit();
		db.close();

		return listArt;
	}

}