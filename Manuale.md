# **Manuale del progetto**

## Introduzione

Questo manuale contiene le informazioni generali e le regole di installazione del progetto per il corso Ingegneria del Software del corso di laurea in Informatica per il Management dell’Università di Bologna, anno accademico 2020/2021.  
Le specifiche complete del progetto sono disponibili a [questo link](https://virtuale.unibo.it/mod/url/view.php?id=462531).

Il progetto è stato realizzato come lavoro di gruppo dai segueti componenti:
| Nome             | Email                             | Matricola |
|------------------|-----------------------------------|-----------|
| Lorenzo Battisti | lorenzo.battisti3@studio.unibo.it | 874002    |
| Giorgia Castelli | giorgia.castelli2@studio.unibo.it | 873787    |
| Riccardo Nassisi | riccardo.nassisi@studio.unibo.it  | 889867    |

L’IDE utilizzato per la scrittura del codice è Eclipse, specificamente la versione [Enterprise Java and Web Developers 2019-12](https://www.eclipse.org/downloads/packages/release/2019-12/r/eclipse-ide-java-developers).  
Il linguaggio di stesura del progetto è [Java](https://www.java.com/en/) 1.8. La scelta della versione è stata guidata dall’utilizzo della tecnologia [GWT](http://www.gwtproject.org/), utilizzata come mezzo principale per la realizzazione dei contenuti.  
La versione di *GWT* più recente, la 2.9.0, è scaricabile autonomamente oppure tramite il plugin integrato di Eclipse, entrambi disponibili a [questo link](http://www.gwtproject.org/download.html).  
La tecnologia di gestione della persistenza è [MapDB](https://mapdb.org/) 0.9.7, mentre quella per la stesura dei test case è [JUnit](https://junit.org/junit5/) 3.  

Tutte le informazioni non contenute in questo documento possono essere trovate nel file `Relazione.md` completa oppure a [questo link](https://drive.google.com/drive/folders/1ChBKuRJXUYeJF7_rrwOrnJjAZKOF2g4D?usp=sharing).

## Installazione

> Tutti i link di download dei vari componenti sono definiti nella sezione precedente.

Per prima cosa è necessario clonare il progetto dalla repository di *BitBucket* sul proprio dispositivo:

> Tramite terminale raggiungere la cartella in cui salvare il progetto
>
> Eseguire questo comando:  
> `git clone https://richinassisi@bitbucket.org/progettosweng2021/reads.git`

Per riuscire a eseguire e testare il progetto si consiglia l’utilizzo dell’*IDE Eclipse*.
Si dà per scontato che la versione di Java installata e integrata su *Eclipse* sia la *jre 1.8*, versione sulla quale il team ha sviluppato e testato.  
Successivamente è necessario installare il plugin di *GWT*, impostando la versione 2.9.0.

Una volta installato e configurato, entrare dentro *Eclipse* e seguire le seguenti azioni:

> *File* -> *Import*... -> *Existing Projects into Workspace*
>
> Navigare nella cartella contenente il progetto clonato e selezionare *Reads*.
>
> Assicurarsi di deselezionare *Copy projects into workspace* se già selezionato.
>
> Cliccare *Finish*.

Ora è possibile ispezionare il progetto all’interno di Eclipse.

Per eseguire l’applicazione tramite GWT development mode:

> Selezionare il file *Reads.launch* nel Package Explorer di Eclipse
>
> Andare nel menù *Run* e selezionare la prima voce *Run*
>
> In alternativa premere `F11` o premere il tasto destro sul file selezionato, selezionare *Run As* e successivamente *Reads*

A seguito di questo passaggio verrà aperta una nuova finestra contenente la *GWT Development Mode*.

> Attendere che l'applicazione finisca di compilare
>
> Premere il pulsante *Launch Default Browser*

Seguiti questi passi, l’applicazione verrà lanciata automaticamente nel browser predefinito e sarà possibile l’utilizzo.

## Utilizzo

L’applicazione è utilizzabile fin da subito come utilizzatore non registrato, ma il team ha previsto la disposizione di due account di test per velocizzare e facilitare il processo; tutti i dati relativi agli account sono contenuti nella tabella seguente:
| Username    | Password | Account        |
|-------------|----------|----------------|
| testuser    | password | non abbonato   |
| testpremium | password | abbonato       |
| testauthor  | password | autore         |
| admin       | admin    | amministratore |
