package com.google.gwt.sweng.reads.client;

import com.google.gwt.sweng.reads.shared.*;

import java.time.Instant;
import java.util.*;

import com.google.gwt.core.client.GWT;
import com.google.gwt.junit.client.GWTTestCase;
import com.google.gwt.user.client.rpc.AsyncCallback;

/**
 * GWT JUnit tests must extend GWTTestCase.
 */
public class ReadsTest extends GWTTestCase {

	/**
	 * Must refer to a valid module that sources this class.
	 */
	public String getModuleName() {
		return "com.google.gwt.sweng.reads.ReadsJUnit";
	}	

	/*
	 * Test Login
	 * 
	 * Il test invoca il metodo login, che deve restituire un valore nullo.
	 */
	public void testLogin() {

		String un = "usernameInsestente";
		String pw = "passwordProva";

		GreetingServiceAsync greetingService = GWT.create(GreetingService.class);
		delayTestFinish(30000);

		greetingService.login(un, pw, new AsyncCallback<User>() {
			@Override
			public void onFailure(Throwable caught) {
				fail("Request failure: " + caught.getMessage());
			}
			@Override
			public void onSuccess(User result) {
				assertTrue(result.equals(null));
				finishTest();
			}
		});
	}

	/*
	 * Test Abbonamento
	 * 
	 * Il test invoca il metodo subscribe, che deve restituire l'oggetto User
	 * corrispondente all'abbonamento appena creato.
	 */
	public void testSubscription() {

		String un = "Riccardo Nassisi";
		String card = "1234123412341234";
		String name = "Riccardo";
		String surname = "Nassisi";
		String date = "2025-01-01";
		String cvv = "123";
		String nation = "Italy";
		String cap = "40120";

		GreetingServiceAsync greetingService = GWT.create(GreetingService.class);
		delayTestFinish(10000);

		greetingService.subscribe(un, card, name, surname, date, cvv, nation, cap, new AsyncCallback<User>() {
			@Override
			public void onFailure(Throwable caught) {
				fail("Request failure: " + caught.getMessage());
			}
			@Override
			public void onSuccess(User result) {
				assertTrue(result.isSubscribed() == true);
				finishTest();
			}
		});
	}

	/*
	 * Test Informazioni Utente
	 * 
	 * Il test invoca il metodo getUserInfo, che deve restituire l'oggetto User
	 * corrispondente all'username inserito.
	 */
	public void testUserInfo() {

		String un = "Riccardo Nassisi";

		GreetingServiceAsync greetingService = GWT.create(GreetingService.class);
		delayTestFinish(5000);

		greetingService.getUserInfo(un, new AsyncCallback<User>() {
			@Override
			public void onFailure(Throwable caught) {
				fail("Request failure: " + caught.getMessage());
			}
			@Override
			public void onSuccess(User result) {
				assertTrue(result.getUsername().equals(un));
				finishTest();
			}
		});
	}

	/*
	 * Test Inserimento articolo
	 * 
	 * Il test invoca il metodo addArticle, che deve restituire un valore 
	 * booleano uguale a false.
	 */
	public void testAddArticle() {

		Article art = new Article("Il mondo dei ghepardi","Il ghepardo, il felino pi� veloce della terra, riuscir� a sfuggire all'estinzione?"
				+ " Negli ultimi 18 anni il numero di questa specie si  ridotta del 30%, ad oggi si contano poco"
				+ " pi di 6.600 mila esemplari che sopravvivo in Africa e alcuni in Iran. \r\n"
				+ "le due sottospecie dell'Iran (Acinonyx jubatus venaticus) e dell'Africa nord "
				+ "occidentale (Acinonyx jubatus heckii) sono state classificate come \"Critically Endangered\""
				+ " nella lista rossa dell'IUCN.",new Date(),"Mondo","Luiginobirichino",false,0,0);

		GreetingServiceAsync greetingService = GWT.create(GreetingService.class);
		delayTestFinish(5000);

		greetingService.addArticle(art, new AsyncCallback<Boolean>() {
			@Override
			public void onFailure(Throwable caught) {
				fail("Request failure: " + caught.getMessage());				
			}
			@Override
			public void onSuccess(Boolean result) {
				assertTrue(result == false);
				finishTest();				
			}
		});
	}

	/*
	 * Test Inerimento categoria
	 * 
	 * Il test invoca il metodo addCategory, che deve restituire un valore 
	 * booleano uguale a true.
	 */
	public void testAddCategory() {
		//nuova 
		Category cat = new Category("Cibo e Cucina");

		GreetingServiceAsync greetingService = GWT.create(GreetingService.class);
		delayTestFinish(5000);

		greetingService.addCategory(cat, new AsyncCallback<Boolean>() {
			@Override
			public void onFailure(Throwable caught) {
				fail("Request failure: " + caught.getMessage());								
			}
			@Override
			public void onSuccess(Boolean result) {
				assertTrue(result == true);
				finishTest();	
			}
		});
	}

	/*
	 * Test Articolo
	 * 
	 * Il test invoca il metodo getArticle, che deve resitutire l'articolo relativo
	 * al titolo inserito.
	 */
	public void testGetArticle() {

		String tit = "Dove si vive meglio in Italia? Ecco la Classifica Ufficiale 2021!";

		GreetingServiceAsync greetingService = GWT.create(GreetingService.class);
		delayTestFinish(5000);

		greetingService.printArticle(tit, new AsyncCallback<Article>() {
			@Override
			public void onFailure(Throwable caught) {
				fail("Request failure: " + caught.getMessage());												
			}
			@Override
			public void onSuccess(Article result) {
				assertTrue(result.getTitle().equals(tit));
				finishTest();
			}
		});

	}

	/*
	 * Test Sessione
	 * 
	 * Il test richiama il servizio relativo alle sessioni, invocando il metodo GetSession.
	 * Il metodo deve restituire valore nullo.
	*/ 
	public void testGetSession() {

		SessionServiceAsync sessionService = GWT.create(SessionService.class);
		delayTestFinish(5000);

		sessionService.getSession(new AsyncCallback<User>() {	
			@Override
			public void onFailure(Throwable caught) {
				fail("Request failure: " + caught.getMessage());												
			}
			@Override
			public void onSuccess(User result) {
				assertTrue(result == null);
				finishTest();			
			}
		});
	}

}
