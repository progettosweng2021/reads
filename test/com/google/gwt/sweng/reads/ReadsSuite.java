package com.google.gwt.sweng.reads;

import com.google.gwt.sweng.reads.client.ReadsTest;
import com.google.gwt.junit.tools.GWTTestSuite;
import junit.framework.Test;
import junit.framework.TestSuite;

public class ReadsSuite extends GWTTestSuite {
  public static Test suite() {
    TestSuite suite = new TestSuite("Tests for Reads");
    suite.addTestSuite(ReadsTest.class);
    return suite;
  }
}
